﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MAIConfirm.Models;

namespace MAIConfirm.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("api/[controller]")]
    [ApiController]
    public class CheckShopSignedsController : ControllerBase
    {
        private readonly MAIConfirmAssetDbContext _context;

        public CheckShopSignedsController(MAIConfirmAssetDbContext context)
        {
            _context = context;
        }

        // GET: api/CheckShopSigneds
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CheckShopSigned>>> GetCheckShopSigneds()
        {
            return await _context.CheckShopSigneds.ToListAsync();
        }

        // GET: api/CheckShopSigneds/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CheckShopSigned>> GetCheckShopSigned(int id)
        {
            var checkShopSigned = await _context.CheckShopSigneds.FindAsync(id);

            if (checkShopSigned == null)
            {
                return NotFound();
            }

            return checkShopSigned;
        }

        // PUT: api/CheckShopSigneds/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCheckShopSigned(int id, CheckShopSigned checkShopSigned)
        {
            if (id != checkShopSigned.id)
            {
                return BadRequest();
            }

            _context.Entry(checkShopSigned).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CheckShopSignedExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CheckShopSigneds
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<CheckShopSigned>> PostCheckShopSigned(CheckShopSigned checkShopSigned)
        {
            _context.CheckShopSigneds.Add(checkShopSigned);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCheckShopSigned", new { id = checkShopSigned.id }, checkShopSigned);
        }

        // DELETE: api/CheckShopSigneds/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCheckShopSigned(int id)
        {
            var checkShopSigned = await _context.CheckShopSigneds.FindAsync(id);
            if (checkShopSigned == null)
            {
                return NotFound();
            }

            _context.CheckShopSigneds.Remove(checkShopSigned);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool CheckShopSignedExists(int id)
        {
            return _context.CheckShopSigneds.Any(e => e.id == id);
        }
    }
}
