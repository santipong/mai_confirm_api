﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MAIConfirm.Models
{
    public class Employee
    {
		[Key]
		public int empId { get; set; }
		public string empName { get; set; }
		public string email { get; set; }
		public string empPosition { get; set; }
		public string signature { get; set; }
		public string region { get; set; }
		public string compCode { get; set; }
		public int? departmentId { get; set; }

	}

	public class EmployeeResponse
	{
		public string status { get; set; }
		public string message { get; set; }
		public List<Employee> employees { get; set; }
	}

	public class Checker
	{
		public string empId { get; set; }
		public string empName { get; set; }
	}
}
