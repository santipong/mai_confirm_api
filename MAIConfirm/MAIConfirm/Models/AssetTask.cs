﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MAIConfirm.Models
{
	public class AssetTask
	{
		[Key]
		public int taskID { get; set; }
		public int periodId { get; set; }
		public int shopId { get; set; }
		public string taskName { get; set; }
		public DateTime planDate { get; set; }
		public DateTime checkDate { get; set; }
		public DateTime accountDate { get; set; }
	}

	public class TaskResponse
	{
		public string status { get; set; }
		public string message { get; set; }
		public List<TaskData> tasks { get; set; }
	}

	public class TaskData {
		[Key]
		public int taskID { get; set; }
		public int periodId { get; set; }
		public int shopId { get; set; }
		public string taskName { get; set; }
		public DateTime planDate { get; set; }
		public DateTime checkDate { get; set; }
		public CheckShop checkShop { get; set; }
		public Shop shop { get; set; }
		public List<ShopKeeperDetail> keeperDetail { get; set; }
		public List<CheckShopSigned> checkShopSigneds { get; set; }
		public List<ShopKeeperDetail> oldKeeperDetails { get; set; }
		public int? sumTask { get; set; }


	}

	public class TaskDetailResponse
	{
		public string status { get; set; }
		public string message { get; set; }
		public TaskData task { get; set; }
	}
}
