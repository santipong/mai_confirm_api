﻿using System;
using System.Collections.Generic;

#nullable disable

namespace MAIConfirm.EFModel.Entities
{
    public partial class Company
    {
        public int Id { get; set; }
        public string CompCode { get; set; }
        public string CompanyName { get; set; }
        public int Region { get; set; }
        public byte[] CreatedAt { get; set; }
        public string Address { get; set; }
    }
}
