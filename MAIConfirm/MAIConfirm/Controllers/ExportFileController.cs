﻿using MAIConfirm.Business.Interface;
using MAIConfirm.EFModel.BackEndModel;
using MAIConfirm.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using OfficeOpenXml;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Information;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Logical;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using OfficeOpenXml.FormulaParsing.Excel.Functions.RefAndLookup;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Text;
using OfficeOpenXml.Style;
using SelectPdf;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection.Emit;
using System.Threading.Tasks;
using static MAIConfirm.EFModel.BackEndModel.ModelReport;


namespace MAIConfirm.Controllers
{
    [Route("api/ExportFile")]
    [ApiController]
    public class ExportFileController : ControllerBase
    {
        private readonly ILogger<ExportFileController> _log;
        private readonly MAIConfirmAssetDbContext _context;

        readonly IConfiguration _configuration;
        IExportFile _exportFile;
        static IConfiguration conf = (new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build());
        public static string PathExportFile = conf["AppSettings:PATH_EXPORT_MAICONFIRM"].ToString();
        public static string PATHConSentformBody = conf["AppSettings:PATH_CONSENTFORM"].ToString();
        public static string PATHConSentMCMCformBody = conf["AppSettings:PATH_CONSENTFORM_MCMC"].ToString();
        public static string PathImg = conf["AppSettings:PATH_IMG"].ToString();
        public static string PathTempImg = conf["AppSettings:PATH_TEMPIMG"].ToString();


        public ExportFileController(IConfiguration configuration, ILogger<ExportFileController> log, IExportFile exportFile, MAIConfirmAssetDbContext context)
        {
            _configuration = configuration;
            _exportFile = exportFile;
            _log = log;
            _context = context;
        }

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="periodId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GenerateConsentFormToPdf")]
        public async Task<IActionResult> GenerateConsentFormToPdf(int? periodId)
        {
            GenerateConsentFormToPdfReq req = new GenerateConsentFormToPdfReq();
            var pathConsentFormByTask = Path.Combine(PathExportFile, "ConsentFormByTask\\periodId_" + periodId);

            try
            {
                if (periodId == null || (periodId ?? 0) == 0)
                    throw new Exception(ConstMsg.NOT_FOUND_MSG);

                if (!Directory.Exists(PathExportFile))
                    Directory.CreateDirectory(PathExportFile);

                DateTime setdate = DateTime.Now;
                req.periodId = periodId;

                List<GenerateConsentFormToPdfResp> generateConsentFormToPdfResps = _exportFile.generateConsentFormToPdfBC(req);
                List<string> ShopName = new List<string>();

                //int i = 1;
                foreach (var item in generateConsentFormToPdfResps)
                {
                    if (!Directory.Exists(pathConsentFormByTask))
                        Directory.CreateDirectory(pathConsentFormByTask);

                    if (item.compCod == "3200")
                    {
                        using (StreamReader reader = System.IO.File.OpenText(PATHConSentMCMCformBody))
                        {
                            HtmlToPdf converterCheckList = new HtmlToPdf();
                            // set converterCheckList options
                            converterCheckList.Options.PdfPageSize = PdfPageSize.A4;
                            converterCheckList.Options.PdfPageOrientation = PdfPageOrientation.Portrait;
                            converterCheckList.Options.WebPageWidth = 1024;
                            converterCheckList.Options.WebPageHeight = 0;
                            converterCheckList.Options.WebPageFixedSize = false;
                            converterCheckList.Options.MarginLeft = 60;
                            converterCheckList.Options.MarginRight = 60;
                            converterCheckList.Options.MarginTop = 60;
                            converterCheckList.Options.MarginBottom = 40;
                            converterCheckList.Options.AutoFitWidth = HtmlToPdfPageFitMode.ShrinkOnly;
                            converterCheckList.Options.AutoFitHeight = HtmlToPdfPageFitMode.NoAdjustment;

                            // footer settings
                            converterCheckList.Options.DisplayFooter = true;
                            converterCheckList.Footer.DisplayOnFirstPage = true;
                            converterCheckList.Footer.DisplayOnOddPages = true;
                            converterCheckList.Footer.DisplayOnEvenPages = true;
                            converterCheckList.Footer.Height = 9;
                            PdfTextSection textChecklist = new PdfTextSection(0, 0, "หนังสือยืนยันยอด - " + item.taskName + "                                                                            หน้า: {page_number} จาก {total_pages}", new System.Drawing.Font("TH SarabunPSK", 8));
                            textChecklist.HorizontalAlign = PdfTextHorizontalAlign.Left;

                            converterCheckList.Footer.Add(textChecklist);


                            List<GetCheckShopTransactionResp> getCheckShopTransactionResps = _exportFile.getCheckShopTransactionBC(item.checkShopId);
                            List<GetCheckShopDetailSignResp> getCheckShopDetailSignResps = _exportFile.GetCheckShopDetailSignBC(item.checkShopId);
                            GetSaleOnShopResp getSaleOnShopResp = _exportFile.GetSaleOnShopBC(item.checkShopId, item.periodId, item.taskID, item.compCod, item.typeId);
                            string tbody = @"";
                            string strOvertableOnPage = @"";
                            List<GetCheckShopTransactionResp> GetCheckShopTransactionRespBalance = new List<GetCheckShopTransactionResp>();
                            List<GetCheckShopTransactionResp> GetCheckShopTransactionByCompAndType = getCheckShopTransactionResps.Where(x => x.typeId == item.typeId && x.compCode == item.compCod).ToList();

                            string sNumPOS = @"", sAssetCode = @"", sSerialNumber = @"";
                            foreach (var itemTransaction in GetCheckShopTransactionByCompAndType.Select((value, index) => new { value, index }))
                            {
                                sNumPOS = itemTransaction.value.avaliable.ToString();
                                sAssetCode = itemTransaction.value.assetName;
                                sSerialNumber = itemTransaction.value.assetCode;
                            }

                            string strImgSignCustomer = $@"<text style='height: 70px;'></text>";

                            GetCheckShopDetailSignResp getCheckShopDetailSignResp = getCheckShopDetailSignResps.Where(x => x.assetTypeID == item.typeId && x.compCode == item.compCod).FirstOrDefault();

                            if (getCheckShopDetailSignResp.customerSigned != null && getCheckShopDetailSignResp.customerSigned != "null" && getCheckShopDetailSignResp.customerSigned != "")
                            {
                                var signImgCustomer = Path.Combine(getCheckShopDetailSignResp.customerSigned);
                                //var signImgCustomer = Path.Combine(PathImg, "A.png");
                                string signImgCustomerogo = new Uri(signImgCustomer).LocalPath;
                                strImgSignCustomer = $@"<img src='{signImgCustomerogo}' alt='imgCustomer' width='150' height='70'>";
                            }

                            Employee EmployeeMANAGER_MCMC = _context.Employees.Where(c => c.empId == int.Parse(conf["AppSettings:MANAGER_MCMC"].ToString())).First();
                            //EmployeeMCMC.empId = EmployeeMANAGER_MCMC.empId;
                            //EmployeeMCMC.empName = EmployeeMANAGER_MCMC.empName;
                            //EmployeeMCMC.empPosition = EmployeeMANAGER_MCMC.empPosition;
                            //EmployeeMCMC.signature = EmployeeMANAGER_MCMC.signature;

                            string strImgSignMANAGER_MCMC = $@"<text style='height: 70px;'></text>";
                            if (EmployeeMANAGER_MCMC.signature != "")
                            {
                                var signImgMANAGER_MCMC = Path.Combine(EmployeeMANAGER_MCMC.signature);
                                string signImgsignImgMANAGER_MCMC = new Uri(signImgMANAGER_MCMC).LocalPath;
                                strImgSignMANAGER_MCMC = $@"<img src='{signImgsignImgMANAGER_MCMC}' alt='imgMANAGER_MCMC' width='150' height='70'>";
                            }

                            var fileRender = reader.ReadToEnd()
                                .Replace("%companyName%", item.compName)
                                .Replace("%companyAdr%", item.compAdr)
                                .Replace("%laterTitle%", item.laterTitle)
                                .Replace("%laterAdr%", item.laterAdr)
                                .Replace("%checkingDate%", item.checkingDate)
                                .Replace("%sNumberPOS%", sNumPOS)
                                .Replace("%sAssetCode%", sAssetCode)
                                .Replace("%sSerialNumber%", sSerialNumber)
                               
                                .Replace("%SignManagerMCMCImg%", strImgSignMANAGER_MCMC)
                                .Replace("%SignManagerMCMCName%", EmployeeMANAGER_MCMC.empName)
                                .Replace("%SignManagerMCMCPosition%", EmployeeMANAGER_MCMC.empPosition)


                                .Replace("%checkedYes%", (item.confirmStatus == "ถูกต้อง" ? "checked.png" : "unchecked.png"))
                                .Replace("%checkedNo%", (item.confirmStatus == "ไม่ถูกต้อง" ? "checked.png" : "unchecked.png"))
                                .Replace("%textbodydeteil%", item.remark)
                                .Replace("%SignShopOwner%", strImgSignCustomer)
                                .Replace("%NameShopOwner%", getCheckShopDetailSignResp.customerName)
                                .Replace("%SignShopOwnerDate%",item.checkingDate)
                                
                                ;

                            // create a new pdf document converting an url
                            SelectPdf.PdfDocument doc = converterCheckList.ConvertHtmlString(fileRender);
                            PdfFont font1CheckList = doc.AddFont(PdfStandardFont.Helvetica);
                            font1CheckList.Size = 20;
                            string strTaskNameFile = $@"";
                            if (item.taskName.Contains("("))
                                item.taskName = item.taskName.Split('(')[1].Replace(")", "");

                            if (item.taskName.Length > 30)
                                strTaskNameFile = $@"{item.shopCode}-{item.taskName.Substring(0, 29)}";
                            else
                                strTaskNameFile = $@"{item.shopCode}-{item.taskName}";



                            var pathFiles = Path.Combine(pathConsentFormByTask, $"{strTaskNameFile}-{item.typeName}-{item.compCod.Substring(0, 2)}.pdf");
                            //var pathFiles = Path.Combine(pathShop, $"หนังสือยืนยันยอด-{item.taskName}.pdf");
                            // save pdf document
                            byte[] pdf = doc.Save();
                            System.IO.File.WriteAllBytes(pathFiles, pdf);
                            // close pdf document
                            doc.Close();
                        }
                    }
                    else {
                        using (StreamReader reader = System.IO.File.OpenText(PATHConSentformBody))
                        {
                            HtmlToPdf converterCheckList = new HtmlToPdf();
                            // set converterCheckList options
                            converterCheckList.Options.PdfPageSize = PdfPageSize.A4;
                            converterCheckList.Options.PdfPageOrientation = PdfPageOrientation.Portrait;
                            converterCheckList.Options.WebPageWidth = 1024;
                            converterCheckList.Options.WebPageHeight = 0;
                            converterCheckList.Options.WebPageFixedSize = false;
                            converterCheckList.Options.MarginLeft = 60;
                            converterCheckList.Options.MarginRight = 60;
                            converterCheckList.Options.MarginTop = 60;
                            converterCheckList.Options.MarginBottom = 40;
                            converterCheckList.Options.AutoFitWidth = HtmlToPdfPageFitMode.ShrinkOnly;
                            converterCheckList.Options.AutoFitHeight = HtmlToPdfPageFitMode.NoAdjustment;

                            // footer settings
                            converterCheckList.Options.DisplayFooter = true;
                            converterCheckList.Footer.DisplayOnFirstPage = true;
                            converterCheckList.Footer.DisplayOnOddPages = true;
                            converterCheckList.Footer.DisplayOnEvenPages = true;
                            converterCheckList.Footer.Height = 9;
                            PdfTextSection textChecklist = new PdfTextSection(0, 0, "หนังสือยืนยันยอด - " + item.taskName + "                                                                            หน้า: {page_number} จาก {total_pages}", new System.Drawing.Font("TH SarabunPSK", 8));
                            textChecklist.HorizontalAlign = PdfTextHorizontalAlign.Left;

                            converterCheckList.Footer.Add(textChecklist);


                            List<GetCheckShopTransactionResp> getCheckShopTransactionResps = _exportFile.getCheckShopTransactionBC(item.checkShopId);
                            List<GetCheckShopDetailSignResp> getCheckShopDetailSignResps = _exportFile.GetCheckShopDetailSignBC(item.checkShopId);
                            GetSaleOnShopResp getSaleOnShopResp = _exportFile.GetSaleOnShopBC(item.checkShopId, item.periodId, item.taskID, item.compCod, item.typeId);
                            string tbody = @"";
                            string strOvertableOnPage = @"";
                            List<GetCheckShopTransactionResp> GetCheckShopTransactionRespBalance = new List<GetCheckShopTransactionResp>();
                            List<GetCheckShopTransactionResp> GetCheckShopTransactionByCompAndType = getCheckShopTransactionResps.Where(x => x.typeId == item.typeId && x.compCode == item.compCod).ToList();
                            foreach (var itemTransaction in GetCheckShopTransactionByCompAndType.Select((value, index) => new { value, index }))
                            {
                                if (item.typeId == itemTransaction.value.typeId && item.compCod == itemTransaction.value.compCode)
                                {
                                    if (itemTransaction.index <= 14)
                                    {
                                        string sCode = (itemTransaction.value.assetCode == "null") ? "" : itemTransaction.value.assetCode;
                                        int sAvaliable = (itemTransaction.value.avaliable ?? 0) + (itemTransaction.value.damaged ?? 0);
                                        string sDiff = "";
                                        string colorText = "color:black;";
                                        if (itemTransaction.value.diffTotal > -1)
                                        {
                                            sDiff = itemTransaction.value.diffTotal.ToString();
                                        }
                                        else
                                        {
                                            int iDiff = -(itemTransaction.value.diffTotal ?? 0);
                                            sDiff = $"({iDiff})";
                                            colorText = "color:#FF0000;";
                                        }

                                        tbody += $@"
                                        <tr>
                                            <td style='text-align: center;'>{sCode}</td>
                                            <td style='text-align: left;'>{itemTransaction.value.assetName}</td>
                                            <td style='text-align: center;'>{itemTransaction.value.unitCount}</td>
                                            <td style='text-align: center;'>{itemTransaction.value.accountBalance}</td>
                                            <td style='text-align: center;'>{sAvaliable}</td>
                                            <td style='text-align: center;{colorText}'>{sDiff}</td>
                                        </tr>
                                    ";
                                    }
                                    else
                                    {
                                        GetCheckShopTransactionRespBalance.Add(itemTransaction.value);
                                    }
                                }

                            }

                            if (GetCheckShopTransactionRespBalance.Count > 0)
                            {
                                strOvertableOnPage = $@"
                                                    <div style='page-break-after: always;'></div>
                                                    <div class='container-fluid' style='display:flex;'>
	                                                    <div class='col-xs-12x'>
		                                                    <table class='table table-bordered col-xs-12x'>
			                                                    <thead>
				                                                    <tr>
					                                                    <th style='width: 50px;'>รหัสสินค้า</th>
					                                                    <th style='width: 320px;'>รายการ</th>
					                                                    <th style='width: 50px;'>หน่วยนับ</th>
					                                                    <th style='width: 70px;'>ยอดบัญชี</th>
					                                                    <th style='width: 70px;'>ยอดตรวจนับ</th>
					                                                    <th style='width: 70px;'>(ขาด)เกิน</th>
				                                                    </tr>
			                                                    </thead>
			                                                    <tbody>
                            ";
                                foreach (var itemTranOver in GetCheckShopTransactionRespBalance.Select((value, index) => new { value, index }))
                                {
                                    if (item.typeId == itemTranOver.value.typeId && item.compCod == itemTranOver.value.compCode)
                                    {
                                        string sCode = (itemTranOver.value.assetCode == "null") ? "" : itemTranOver.value.assetCode;
                                        int sAvaliable = (itemTranOver.value.avaliable ?? 0) + (itemTranOver.value.damaged ?? 0);
                                        string sDiff = "";
                                        string colorText = "color:black;";
                                        if (itemTranOver.value.diffTotal > -1)
                                        {
                                            sDiff = itemTranOver.value.diffTotal.ToString();
                                        }
                                        else
                                        {
                                            int iDiff = -(itemTranOver.value.diffTotal ?? 0);
                                            sDiff = $"({iDiff})";
                                            colorText = "color:#FF0000;";
                                        }

                                        strOvertableOnPage += $@"
                                        <tr>
                                            <td style='text-align: center;'>{sCode}</td>
                                            <td style='text-align: left;'>{itemTranOver.value.assetName}</td>
                                            <td style='text-align: center;'>{itemTranOver.value.unitCount}</td>
                                            <td style='text-align: center;'>{itemTranOver.value.accountBalance}</td>
                                            <td style='text-align: center;'>{sAvaliable}</td>
                                            <td style='text-align: center;{colorText}'>{sDiff}</td>
                                        </tr>
                                ";
                                    }
                                }
                                strOvertableOnPage += $@"
                                                                </tbody>
		                                                    </table>
	                                                    </div>
                                                    </div>
                            ";
                            }

                            string sConfirmStatus = @"", sShopSignRemark = @"";
                            if (item.confirmStatus.Equals(ConstMsg.CONFIRMCORRECT))
                                sConfirmStatus = $@"<span class='label label-success'>{ConstMsg.CONFIRMCORRECT}</span>";
                            else if (item.confirmStatus.Equals(ConstMsg.CONFIRMINCORRECT))
                                sConfirmStatus = $@"<span class='label label-danger'>{ConstMsg.CONFIRMINCORRECT}</span>";
                            else if (item.confirmStatus.Equals(ConstMsg.CONFIRMUNRECOMMEND))
                                sConfirmStatus = $@"<span class='label label-default'>{ConstMsg.CONFIRMUNRECOMMEND}</span>";
                            sShopSignRemark = (item.remark == "") ? "" : item.remark;

                            string strImgSignCustomer = $@"<text style='height: 70px;'></text>";
                            string strheightAutoTextCustomer = $@"";

                            GetCheckShopDetailSignResp getCheckShopDetailSignResp = getCheckShopDetailSignResps.Where(x => x.assetTypeID == item.typeId && x.compCode == item.compCod).FirstOrDefault();

                            if (getCheckShopDetailSignResp.customerSigned != null && getCheckShopDetailSignResp.customerSigned != "null" && getCheckShopDetailSignResp.customerSigned != "")
                            {
                                var signImgCustomer = Path.Combine(getCheckShopDetailSignResp.customerSigned);
                                //var signImgCustomer = Path.Combine(PathImg, "A.png");
                                string signImgCustomerogo = new Uri(signImgCustomer).LocalPath;
                                strImgSignCustomer = $@"<img src='{signImgCustomerogo}' alt='imgCustomer' width='150' height='70'>";
                            }
                            else
                            {
                                strheightAutoTextCustomer = $@"height: 80px;";
                            }

                            string strSignCustomerName = getCheckShopDetailSignResp.customerName;
                            string strSignCustomerPosition = getCheckShopDetailSignResp.customerRelation;

                            string strImgSignEmployee = $@"<text style='height: 70px;'></text>";
                            string strheightAutoTextloyee = $@"";
                            if (getCheckShopDetailSignResp.signature != null && getCheckShopDetailSignResp.signature != "null" && getCheckShopDetailSignResp.signature != "")
                            {
                                var signImgEmp = Path.Combine(getCheckShopDetailSignResp.signature);
                                //var signImgEmp = Path.Combine(PathImg, "B.png");
                                string signImgEmpLogo = new Uri(signImgEmp).LocalPath;
                                strImgSignEmployee = $@"<img src='{signImgEmpLogo}' alt='imgCustomer' width='150' height='70'>";
                            }
                            else
                            {
                                strheightAutoTextloyee = $@"height: 80px;";
                            }

                            string strSignEmployeeName = getCheckShopDetailSignResp.empName;
                            string strSignEmployeePosition = getCheckShopDetailSignResp.empPosition;

                            string strImgSignSale = $@"<text style='height: 70px;'></text>";
                            string strheightAutoTextSale = $@"";
                            if (getSaleOnShopResp.employeeSigned != null && getSaleOnShopResp.employeeSigned != "null" && getSaleOnShopResp.employeeSigned != "")
                            {
                                var signImgSale = Path.Combine(getSaleOnShopResp.employeeSigned);
                                //var signImgSale = Path.Combine(PathImg, "C.png");
                                string signImgSaleLogo = new Uri(signImgSale).LocalPath;
                                strImgSignSale = $@"<img src='{signImgSaleLogo}' alt='imgCustomer' width='150' height='70'>";
                            }
                            else
                            {
                                strheightAutoTextSale = $@"height: 80px;";
                            }

                            string strSignSaleName = getSaleOnShopResp.EmpName;
                            string strSignSalePosition = getSaleOnShopResp.EmpPosition;


                            var fileRender = reader.ReadToEnd()
                                .Replace("%companyName%", item.compName)
                                .Replace("%companyAdr%", item.compAdr)
                                .Replace("%laterTitle%", item.laterTitle)
                                .Replace("%laterAdr%", item.laterAdr)
                                .Replace("%laterDescripton%", item.laterDescripton)
                                .Replace("%laterDatetime%", item.laterDatetime)
                                .Replace("%checkingDate%", item.checkingDate)
                                .Replace("%tbodyReferenceDescription%", tbody)
                                .Replace("%confirmStatus%", sConfirmStatus)
                                .Replace("%ShopSignRemark%", sShopSignRemark)
                                .Replace("%SignCustomerImg%", strImgSignCustomer)
                                .Replace("%SignCustomerName%", strSignCustomerName)
                                .Replace("%SignCustomerPosition%", strSignCustomerPosition)
                                .Replace("%SignEmployeeImg%", strImgSignEmployee)
                                .Replace("%SignEmployeeName%", strSignEmployeeName)
                                .Replace("%SignEmployeePosition%", strSignEmployeePosition)
                                .Replace("%SignSaleImg%", strImgSignSale)
                                .Replace("%SignSaleName%", strSignSaleName)
                                .Replace("%SignSalePosition%", strSignSalePosition)
                                .Replace("%heightAutoTextA%", strheightAutoTextCustomer)
                                .Replace("%heightAutoTextB%", strheightAutoTextloyee)
                                .Replace("%heightAutoTextC%", strheightAutoTextSale)
                                .Replace("%overtableOnPage%", strOvertableOnPage)
                                ;

                            // create a new pdf document converting an url
                            SelectPdf.PdfDocument doc = converterCheckList.ConvertHtmlString(fileRender);
                            PdfFont font1CheckList = doc.AddFont(PdfStandardFont.Helvetica);
                            font1CheckList.Size = 20;
                            string strTaskNameFile = $@"";
                            if (item.taskName.Contains("("))
                                item.taskName = item.taskName.Split('(')[1].Replace(")", "");

                            if (item.taskName.Length > 30)
                                strTaskNameFile = $@"{item.shopCode}-{item.taskName.Substring(0, 29)}";
                            else
                                strTaskNameFile = $@"{item.shopCode}-{item.taskName}";



                            var pathFiles = Path.Combine(pathConsentFormByTask, $"{strTaskNameFile}-{item.typeName}-{item.compCod.Substring(0, 2)}.pdf");
                            //var pathFiles = Path.Combine(pathShop, $"หนังสือยืนยันยอด-{item.taskName}.pdf");
                            // save pdf document
                            byte[] pdf = doc.Save();
                            System.IO.File.WriteAllBytes(pathFiles, pdf);
                            // close pdf document
                            doc.Close();
                        }
                    }

                   
                }


                var pathConsentFormByTaskZip = Path.Combine(PathExportFile, "ConsentFormByTaskZip\\periodId_" + periodId);
                //var pathConsentFormByTaskTest = Path.Combine(PathExportFile, "ConsentFormByTask", "การบ้าน");

                if (!Directory.Exists(pathConsentFormByTaskZip))
                    Directory.CreateDirectory(pathConsentFormByTaskZip);

                var pathFile = Path.Combine(PathExportFile + "\\ConsentFormByTaskZip", "periodId_" + periodId + "_" + setdate.Year + setdate.Month + setdate.Day + "_" + setdate.Millisecond + ".zip");

                ZipFile.CreateFromDirectory(pathConsentFormByTask, pathFile);
                ZipFile.ExtractToDirectory(pathFile, pathConsentFormByTaskZip);

                var fileStream = new FileStream(pathFile, FileMode.Open, FileAccess.Read);
                var response = new FileHttpResponseMessage(pathFile);
                response.StatusCode = HttpStatusCode.OK;
                response.Content = new StreamContent(fileStream);
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = Path.GetFileName(pathFile)
                };

                return File(fileStream, "application/octet-stream", Path.GetFileName(pathFile));
            }
            catch (Exception ex)
            {
                _log.LogInformation("Exception Message : " + ex.Message);
                _log.LogInformation("Exception StackTrace : " + ex.StackTrace);

                if (ex.InnerException != null)
                {
                    _log.LogInformation("InnerException Message : " + ex.InnerException.Message);
                    _log.LogInformation("InnerException StackTrace : " + ex.InnerException.StackTrace);
                }

                ObjectResult messageCode = null;
                if (ex.Message == ConstMsg.FIND_REPORT_AUDIT_SHOP_NOT_FOUND)
                    messageCode = StatusCode((int)HttpStatusCode.NotFound, ex.Message);
                else
                    messageCode = StatusCode((int)HttpStatusCode.BadRequest, ex.Message);

                return messageCode;
            }
            finally
            {
                if (Directory.Exists(pathConsentFormByTask))
                {
                    Directory.Delete(pathConsentFormByTask, true);
                }
                if (Directory.Exists(PathExportFile + "\\ConsentFormByTaskZip\\periodId_" + periodId))
                {
                    Directory.Delete(PathExportFile + "\\ConsentFormByTaskZip\\periodId_" + periodId, true);
                }
            }
        }

        //[HttpPost]
        //[Route("GenerateConsentFormToExcel")]
      
        [HttpGet("GenerateConsentFormToExcel/{periodId}/{compCode}/{center}")]
        //public async Task<IActionResult> GenerateConsentFormToExcel(Req_ReportExcelConfirmImage req)
        public async Task<IActionResult> GenerateConsentFormToExcel(int? periodId, int? compCode, string center)
        {

            Req_ReportExcelConfirmImage req = new Req_ReportExcelConfirmImage();
            req.periodId = periodId;
            req.compCode = compCode;
            req.center = center;
            //call sql 

            try
            {


                using (ExcelPackage excel = new ExcelPackage())
                {
                    Color colorWhile = ColorTranslator.FromHtml("#FFFFFF");
                    Color colorBlack = ColorTranslator.FromHtml("#000000");
                    Color colorRed = ColorTranslator.FromHtml("#FF0000");
                    Color colorBlue = ColorTranslator.FromHtml("#2552a4");
                    Color backgroundcolorYellow = ColorTranslator.FromHtml("#FFE699");
                    Color backgroundcolorYellowGold = ColorTranslator.FromHtml("#FFFF00");
                    Color backgroundcolorOrange = ColorTranslator.FromHtml("#F4B084");
                    Color backgroundColorBlue = ColorTranslator.FromHtml("#99bdf2");
                    Color backgroundHeaderTopColor = ColorTranslator.FromHtml("#17375D");
                    Color backgroundColorWhile = ColorTranslator.FromHtml("#FFFFFF");
                    int iFontHeaderSize = 10;

                    #region [set name sheets]
                    excel.Workbook.Worksheets.Add("รายงานรูปภาพ");
                    var worksheet = excel.Workbook.Worksheets["รายงานรูปภาพ"];
                    #endregion

                    if (!Directory.Exists(PathExportFile))
                        Directory.CreateDirectory(PathExportFile);

                    #region [set static header]
                    var Rowss = 1;
                    worksheet.Cells[Rowss, 2, Rowss, 29].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[Rowss, 2, Rowss, 29].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[Rowss, 2, Rowss, 29].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[Rowss, 2, Rowss, 29].Style.Font.Bold = true;
                    worksheet.Cells[Rowss, 2, Rowss, 29].Merge = true;
                    worksheet.Cells[Rowss, 2, Rowss, 29].Value = "รายงานสรุปผลการตรวจยืนยันยอดลูกหนี้(รูปภาพ)";
                    worksheet.Cells[Rowss, 2, Rowss, 29].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    worksheet.Cells[Rowss, 2, Rowss, 29].Style.Fill.BackgroundColor.SetColor(backgroundHeaderTopColor);
                    worksheet.Cells[Rowss, 2, Rowss, 29].Style.Font.Color.SetColor(colorWhile);

                    List<Res_HeaderReportExcelConfirmImage> resHead = _exportFile.GetHeaderImageReportTaskOnSummary(req);

                    Rowss++; Rowss++; worksheet.Cells[Rowss, 2, Rowss, 2].Value = "สรุปผลการตรวจยืนยันยอดลูกหนี้(รูปภาพ)";

                    Rowss++; Rowss++; worksheet.Cells[Rowss, 2, Rowss, 2].Value = "ระหว่างวันที่ " + resHead[0].startDate + " ถึง วันที่ " + resHead[0].endDate + "";
                    Rowss++; worksheet.Cells[Rowss, 2, Rowss, 2].Value = "บริษัท " + resHead[0].companyName + " จำกัด";
                    //Rowss++; worksheet.Cells[Rowss, 2, Rowss, 2].Value = "หน่วยขาย/โกดัง : " + resHead[0].departmentName + "";
                    Rowss++; worksheet.Cells[Rowss, 2, Rowss, 2].Value = "ศูนย์: " + resHead[0].departmentCenter + "";
                    //Rowss++; worksheet.Cells[Rowss, 2, Rowss, 2].Value = "รายงานรูปภาพ : ทั้งหมด";
                    //worksheet.Cells[Rowss, 2, Rowss, 2].Style.Font.Color.SetColor(colorRed);

                    string[] arrColumnsHeaderName = { "หน่วยขาย/โกดัง", "รหัสพนักงาน", "พนักงานผู้ดูแล", "รหัสลูกค้า", "ชื่อลูกค้า", "สถานะร้านค้า", "รูปถ่ายหน้าร้าน", "หมายเหตุ", "วันที่ตรวจนับ", "ประเภทรายการ", "รหัสทรัพย์สิน", "ชื่อPOP/POS/อุปกรณ์เบียร์สด" };
                    for (int indexTbName = 1; indexTbName < arrColumnsHeaderName.Length + 1; indexTbName++)
                    {
                        int columnsHeader = 0;
                        columnsHeader = indexTbName + 1;



                        worksheet.Cells[10, columnsHeader, 11, columnsHeader].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Cells[10, columnsHeader, 11, columnsHeader].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        worksheet.Cells[10, columnsHeader, 11, columnsHeader].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[10, columnsHeader, 11, columnsHeader].Style.Font.Bold = true;
                        worksheet.Cells[10, columnsHeader, 11, columnsHeader].Merge = true;
                        worksheet.Cells[10, columnsHeader, 11, columnsHeader].Value = arrColumnsHeaderName[indexTbName - 1];
                        worksheet.Cells[10, columnsHeader, 11, columnsHeader].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        // worksheet.Cells[10, columnsHeader, 11, columnsHeader].Style.Fill.BackgroundColor.SetColor(backgroundColorBlue);
                        worksheet.Cells[10, columnsHeader, 11, columnsHeader].Style.Font.Color.SetColor(colorBlack);
                        worksheet.Cells[10, columnsHeader, 11, columnsHeader].Style.Font.Size = iFontHeaderSize;


                        #region[set color header]
                        if (columnsHeader <= 6 || (columnsHeader > 10 && columnsHeader <= 13))
                            worksheet.Cells[10, columnsHeader, 11, columnsHeader].Style.Fill.BackgroundColor.SetColor(backgroundColorBlue);
                        if ((columnsHeader >= 7 && columnsHeader <= 9))
                            worksheet.Cells[10, columnsHeader, 11, columnsHeader].Style.Fill.BackgroundColor.SetColor(backgroundcolorYellow);
                        if (columnsHeader == 10 || columnsHeader == 14 || columnsHeader == 15 || columnsHeader == 16 || columnsHeader == 17)
                            worksheet.Cells[10, columnsHeader, 11, columnsHeader].Style.Fill.BackgroundColor.SetColor(backgroundcolorOrange);
                        #endregion

                        #region[set width header]
                        if (columnsHeader == 2 || columnsHeader == 4 || columnsHeader == 6 || columnsHeader == 13)
                        {
                            //worksheet.Column(columnsHeader).Width = 20;
                            switch (columnsHeader)
                            {
                                case 2:
                                    worksheet.Column(columnsHeader).Width = 20;
                                    break;
                                case 4:
                                    worksheet.Column(columnsHeader).Width = 20;
                                    break;
                                case 6:
                                    worksheet.Column(columnsHeader).Width = 30;
                                    break;
                                case 13:
                                    worksheet.Column(columnsHeader).Width = 30;
                                    break;
                            }
                        }
                        else
                        {
                            worksheet.Column(columnsHeader).Width = 15;
                        }

                        #endregion


                    }
                    worksheet.Cells[11, 2, 11, 12].AutoFilter = true;

                    worksheet.Column(14).Width = 20;
                    worksheet.Column(17).Width = 20;


                    #endregion                 
                    worksheet.Cells[10, 14, 10, 17].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[10, 14, 10, 17].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[10, 14, 10, 17].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[10, 14, 10, 17].Style.Font.Bold = true;
                    worksheet.Cells[10, 14, 10, 17].Merge = true;
                    worksheet.Cells[10, 14, 10, 17].Value = "จำนวนตรวจนับ";
                    worksheet.Cells[10, 14, 10, 17].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    worksheet.Cells[10, 14, 10, 17].Style.Fill.BackgroundColor.SetColor(backgroundcolorOrange);
                    worksheet.Cells[10, 14, 10, 17].Style.Font.Color.SetColor(colorBlack);
                    worksheet.Cells[10, 14, 10, 17].Style.Font.Size = iFontHeaderSize;

                    worksheet.Cells[11, 14, 11, 14].Value = "ยอดตามบัญชี";
                    worksheet.Cells[11, 15, 11, 15].Value = "สภาพดี";
                    worksheet.Cells[11, 16, 11, 16].Value = "ชำรุด";
                    worksheet.Cells[11, 17, 11, 17].Value = "ผลต่าง(ขาด)เกิน";
                    worksheet.Cells[11, 14, 11, 14].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    worksheet.Cells[11, 15, 11, 15].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    worksheet.Cells[11, 16, 11, 16].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    worksheet.Cells[11, 17, 11, 17].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    worksheet.Cells[11, 14, 11, 17].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[11, 14, 11, 17].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[11, 14, 11, 17].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[11, 14, 11, 17].Style.Font.Bold = true;
                    worksheet.Cells[11, 14, 11, 17].Style.Font.Color.SetColor(colorBlack);
                    worksheet.Cells[11, 14, 11, 17].Style.Font.Size = iFontHeaderSize;
                    worksheet.Cells[11, 14, 11, 17].Style.Fill.BackgroundColor.SetColor(backgroundcolorOrange);


                    int goodcolums = 18;
                    int goodcolums2 = 22;
                    int goodcolums3 = 23;
                    if (resHead[0].CountImageGood < 5)
                    {
                        goodcolums = 18;
                        goodcolums2 = 22;
                        goodcolums3 = 23;
                    }
                    else
                    {
                        goodcolums = 18;
                        goodcolums2 = 17 + ((int)resHead[0].CountImageGood);
                        goodcolums3 = 18 + ((int)resHead[0].CountImageGood);
                    }
                    worksheet.Cells[10, goodcolums, 10, goodcolums2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[10, goodcolums, 10, goodcolums2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[10, goodcolums, 10, goodcolums2].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[10, goodcolums, 10, goodcolums2].Style.Font.Bold = true;
                    worksheet.Cells[10, goodcolums, 10, goodcolums2].Merge = true;
                    worksheet.Cells[10, goodcolums, 10, goodcolums2].Value = "รูปถ่ายประกอบการตรวจนับ";
                    worksheet.Cells[10, goodcolums, 10, goodcolums2].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    worksheet.Cells[10, goodcolums, 10, goodcolums2].Style.Fill.BackgroundColor.SetColor(backgroundcolorYellow);
                    worksheet.Cells[10, goodcolums, 10, goodcolums2].Style.Font.Color.SetColor(colorBlack);
                    worksheet.Cells[10, goodcolums, 10, goodcolums2].Style.Font.Size = iFontHeaderSize;

                    worksheet.Cells[11, goodcolums, 11, goodcolums2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[11, goodcolums, 11, goodcolums2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[11, goodcolums, 11, goodcolums2].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[11, goodcolums, 11, goodcolums2].Style.Font.Bold = true;
                    worksheet.Cells[11, goodcolums, 11, goodcolums2].Merge = true;
                    worksheet.Cells[11, goodcolums, 11, goodcolums2].Value = "สภาพดี";
                    worksheet.Cells[11, goodcolums, 11, goodcolums2].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    worksheet.Cells[11, goodcolums, 11, goodcolums2].Style.Fill.BackgroundColor.SetColor(backgroundcolorYellow);
                    worksheet.Cells[11, goodcolums, 11, goodcolums2].Style.Font.Color.SetColor(colorBlack);
                    worksheet.Cells[11, goodcolums, 11, goodcolums2].Style.Font.Size = iFontHeaderSize;

                    worksheet.Cells[10, goodcolums3, 11, goodcolums3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[10, goodcolums3, 11, goodcolums3].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[10, goodcolums3, 11, goodcolums3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[10, goodcolums3, 11, goodcolums3].Style.Font.Bold = true;
                    worksheet.Cells[10, goodcolums3, 11, goodcolums3].Merge = true;
                    worksheet.Cells[10, goodcolums3, 11, goodcolums3].Value = "หมายเหตุ (สภาพดี)";
                    worksheet.Cells[10, goodcolums3, 11, goodcolums3].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    worksheet.Cells[10, goodcolums3, 11, goodcolums3].Style.Fill.BackgroundColor.SetColor(backgroundcolorYellowGold);
                    worksheet.Cells[10, goodcolums3, 11, goodcolums3].Style.Font.Color.SetColor(colorBlack);
                    worksheet.Cells[10, goodcolums3, 11, goodcolums3].Style.Font.Size = iFontHeaderSize;
                    worksheet.Column(goodcolums3).Width = 30;
                    int badcolums = 24;
                    int badcolums2 = 28;
                    int badcolums3 = 29;
                    if (resHead[0].CountImageBad < 5)
                    {
                        if (resHead[0].CountImageGood > 5)
                        {
                            badcolums = goodcolums3 + 1;
                            badcolums2 = ((goodcolums3) + 5);
                            badcolums3 = ((goodcolums3 + 1) + 5);
                        }
                        else
                        {
                            badcolums = 24;
                            badcolums2 = 28;
                            badcolums3 = 29;
                        }
                    }
                    else
                    {
                        badcolums = (goodcolums3 + 1);
                        badcolums2 = (goodcolums3) + ((int)resHead[0].CountImageBad);
                        badcolums3 = (goodcolums3 + 1) + ((int)resHead[0].CountImageBad);
                    }
                    worksheet.Cells[10, badcolums, 10, badcolums2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[10, badcolums, 10, badcolums2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[10, badcolums, 10, badcolums2].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[10, badcolums, 10, badcolums2].Style.Font.Bold = true;
                    worksheet.Cells[10, badcolums, 10, badcolums2].Merge = true;
                    worksheet.Cells[10, badcolums, 10, badcolums2].Value = "รูปถ่ายประกอบการตรวจนับ";
                    worksheet.Cells[10, badcolums, 10, badcolums2].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    worksheet.Cells[10, badcolums, 10, badcolums2].Style.Fill.BackgroundColor.SetColor(backgroundcolorYellow);
                    worksheet.Cells[10, badcolums, 10, badcolums2].Style.Font.Color.SetColor(colorBlack);
                    worksheet.Cells[10, badcolums, 10, badcolums2].Style.Font.Size = iFontHeaderSize;

                    worksheet.Cells[11, badcolums, 11, badcolums2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[11, badcolums, 11, badcolums2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[11, badcolums, 11, badcolums2].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[11, badcolums, 11, badcolums2].Style.Font.Bold = true;
                    worksheet.Cells[11, badcolums, 11, badcolums2].Merge = true;
                    worksheet.Cells[11, badcolums, 11, badcolums2].Value = "ชำรุด";
                    worksheet.Cells[11, badcolums, 11, badcolums2].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    worksheet.Cells[11, badcolums, 11, badcolums2].Style.Fill.BackgroundColor.SetColor(backgroundcolorYellow);
                    worksheet.Cells[11, badcolums, 11, badcolums2].Style.Font.Color.SetColor(colorBlack);
                    worksheet.Cells[11, badcolums, 11, badcolums2].Style.Font.Size = iFontHeaderSize;

                    worksheet.Cells[10, badcolums3, 11, badcolums3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[10, badcolums3, 11, badcolums3].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[10, badcolums3, 11, badcolums3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[10, badcolums3, 11, badcolums3].Style.Font.Bold = true;
                    worksheet.Cells[10, badcolums3, 11, badcolums3].Merge = true;
                    worksheet.Cells[10, badcolums3, 11, badcolums3].Value = "หมายเหตุ (ชำรุด)";
                    worksheet.Cells[10, badcolums3, 11, badcolums3].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    worksheet.Cells[10, badcolums3, 11, badcolums3].Style.Fill.BackgroundColor.SetColor(backgroundcolorYellowGold);
                    worksheet.Cells[10, badcolums3, 11, badcolums3].Style.Font.Color.SetColor(colorBlack);
                    worksheet.Cells[10, badcolums3, 11, badcolums3].Style.Font.Size = iFontHeaderSize;
                    worksheet.Column(badcolums3).Width = 30;

                    Rowss = 12;
                    int Columss = 2;
                    List<Res_ReportExcelConfirmImage> generateConsentFormToExcelResps = _exportFile.GetImageReportTaskOnSummary(req);

                    List<Res_DeteilReportExcelConfirmImage> generateDeteilConsentFormToExcelResps = _exportFile.GetDeteilImageReportTaskOnSummary(req);

                    int countImage = 1;
                    int? oldempId = null;
                    string oldshopCode = null;
                    string olddepartmentName = null;

                    int StartRowdepartmentName = 0;
                    int EndRowdepartmentName = 0;
                    string FirstRowdepartmentName = null;


                    int StartRowempId = 0;
                    int EndRowempId = 0;
                    int? FirstRowempId = null;

                    int StartRowshopCode = 0;
                    int EndRowshopCode = 0;
                    string FirstRowshopCode = null;

                    foreach (var item in generateConsentFormToExcelResps)
                    {
                        worksheet.Cells[Rowss, Columss, Rowss, Columss].Value = item.departmentName;
                        Columss++; worksheet.Cells[Rowss, Columss, Rowss, Columss].Value = item.empId;
                        Columss++; worksheet.Cells[Rowss, Columss, Rowss, Columss].Value = item.empName;
                        Columss++; worksheet.Cells[Rowss, Columss, Rowss, Columss].Value = item.shopCode;
                        Columss++; worksheet.Cells[Rowss, Columss, Rowss, Columss].Value = item.shopName; worksheet.Cells[Rowss, Columss, Rowss, Columss].Style.WrapText = true;
                        Columss++; worksheet.Cells[Rowss, Columss, Rowss, Columss].Value = item.shopStatus;
                        if (item.shopImage != null)
                        {
                            //using (System.Drawing.Image image = System.Drawing.Image.FromFile("C:\\Users\\70059245\\Desktop\\2.jpg"))
                            //var PathImageDemo = "C:\\Users\\70059245\\Desktop\\ei_1722423138878149914282260398748T1.jpg";
                            var PathImageDemo = item.shopImage;
                            //string[] PathImageDemo2 = item.shopImage.Split('\\');
                            //PathImageDemo = "D:\\WorkSpaceWeb\\MaiConfirm\\shopImages\\" + PathImageDemo2[PathImageDemo2.Length-1];
                            //PathImageDemo = "D:\\WorkSpaceWeb\\MaiConfirm\\assetImages\\" + PathImageDemo2[PathImageDemo2.Length - 1];
                            string[] FileNameArray = PathImageDemo.Split('\\');
                            var FileName = FileNameArray[FileNameArray.Length - 1];
                            string[] FileNameArrayChild = FileName.Split('.');
                            var filePathSave = FileNameArrayChild[0] + "Copy" + periodId + "." + FileNameArrayChild[1];
                            System.Drawing.Image imageReQuality = System.Drawing.Image.FromFile(PathImageDemo);
                            SaveJpeg(PathTempImg + "\\" +filePathSave, imageReQuality, 40);
                            System.Drawing.Image image = System.Drawing.Image.FromFile(PathTempImg + "\\" + filePathSave);
                            using (image)
                            //using (System.Drawing.Image image = System.Drawing.Image.FromFile(item.shopImage))
                            {
                                var excelImage = worksheet.Drawings.AddPicture(countImage.ToString(), image);
                                excelImage.SetSize(50, 50);
                                excelImage.SetPosition(Rowss - 1, 3, Columss, 3);
                                worksheet.Row(Rowss).Height = 60;
                            }
                        }
                        Columss++;
                        Columss++;
                        worksheet.Cells[Rowss, Columss, Rowss, Columss].Value = item.remark;

                        Columss++; worksheet.Cells[Rowss, Columss, Rowss, Columss].Value = (item.shopStatus == "ไม่ได้เข้าร้าน" ? "" : item.checkDate);
                        Columss++; worksheet.Cells[Rowss, Columss, Rowss, Columss].Value = item.assetType;
                        Columss++; worksheet.Cells[Rowss, Columss, Rowss, Columss].Value = item.assetCode;
                        Columss++; worksheet.Cells[Rowss, Columss, Rowss, Columss].Value = item.assetName;
                        worksheet.Cells[Rowss, Columss, Rowss, Columss].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Cells[Rowss, Columss, Rowss, Columss].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        worksheet.Cells[Rowss, Columss, Rowss, Columss].Style.WrapText = true;

                        Columss++; worksheet.Cells[Rowss, Columss, Rowss, Columss].Value = item.SumAccountBalance;
                        Columss++; worksheet.Cells[Rowss, Columss, Rowss, Columss].Value = item.SumGood;
                        Columss++; worksheet.Cells[Rowss, Columss, Rowss, Columss].Value = item.SumBad;
                        Columss++; worksheet.Cells[Rowss, Columss, Rowss, Columss].Value = (item.DiffAccountBalance < 0 ? "(" + item.DiffAccountBalance * (-1) + ")" : item.DiffAccountBalance);

                        Random rnd = new Random();

                        worksheet.Cells[Rowss, 2, Rowss, Columss].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Cells[Rowss, 2, Rowss, Columss].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        worksheet.Cells[Rowss, 2, Rowss, Columss].Style.Font.Size = iFontHeaderSize;

                        int checkh1 = 1;
                        int checkh2 = 1;
                        if (generateDeteilConsentFormToExcelResps != null)
                        {
                            List<Res_DeteilReportExcelConfirmImage> dataRes = generateDeteilConsentFormToExcelResps.Where(a => a.empId == item.empId && a.assetName == item.assetName && a.shopCode == item.shopCode && a.assetStatus == "available").ToList();
                            if (dataRes.Count > 0)
                            {
                                int iColums = 0;
                                var textremark = "";
                                foreach (var itemDeteil in dataRes)
                                {
                                    //using (System.Drawing.Image imageD = System.Drawing.Image.FromFile("C:\\Users\\70059245\\Desktop\\2.jpg"))
                                    //var PathImageDemo = "C:\\Users\\70059245\\Desktop\\ei_1722423138878149914282260398748T1.jpg";
                                    var PathImageDemo = itemDeteil.attachImage;
                                    //string[] PathImageDemo2 = itemDeteil.attachImage.Split('\\');
                                    //PathImageDemo = "D:\\WorkSpaceWeb\\MaiConfirm\\shopImages\\" + PathImageDemo2[PathImageDemo2.Length-1];
                                    //PathImageDemo = "D:\\WorkSpaceWeb\\MaiConfirm\\assetImages\\" + PathImageDemo2[PathImageDemo2.Length - 1];
                                    string[] FileNameArray = PathImageDemo.Split('\\');
                                    var FileName = FileNameArray[FileNameArray.Length - 1];
                                    string[] FileNameArrayChild = FileName.Split('.');
                                    var filePathSave = FileNameArrayChild[0] + "Copy" + periodId + "." + FileNameArrayChild[1];
                                    System.Drawing.Image imageReQuality = System.Drawing.Image.FromFile(PathImageDemo);
                                    SaveJpeg(PathTempImg + "\\"+ filePathSave, imageReQuality, 40);
                                    System.Drawing.Image imageD = System.Drawing.Image.FromFile(PathTempImg + "\\" + filePathSave);
                                    using (imageD)
                                    //using (System.Drawing.Image imageD = System.Drawing.Image.FromFile(itemDeteil.attachImage))
                                    {
                                        var excelImageD = worksheet.Drawings.AddPicture(item.shopCode + "" + item.assetCode.ToString() + "" + iColums + rnd.Next(), imageD);
                                        excelImageD.SetSize(50, 50);
                                        excelImageD.SetPosition(Rowss - 1, 3, Columss + iColums, 3);
                                        worksheet.Row(Rowss).Height = 60;
                                    }
                                    if (itemDeteil.remarkimage != "")
                                    {
                                        textremark += (iColums + 1) + ". " + itemDeteil.remarkimage + "\n";
                                        checkh1++;
                                        //if (iColums != 0)
                                        //{
                                        //    textremark += "\n";
                                        //}
                                    }

                                    iColums++;
                                }
                                Columss = goodcolums3;
                                worksheet.Cells[Rowss, Columss, Rowss, Columss].Value = textremark;
                                worksheet.Cells[Rowss, Columss, Rowss, Columss].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                worksheet.Cells[Rowss, Columss, Rowss, Columss].Style.WrapText = true;
                                //worksheet.Column(Columss).AutoFit();

                            }

                            //damaged
                            List<Res_DeteilReportExcelConfirmImage> dataResdamaged = generateDeteilConsentFormToExcelResps.Where(a => a.empId == item.empId && a.assetName == item.assetName && a.shopCode == item.shopCode && a.assetStatus == "damaged").ToList();
                            if (dataResdamaged.Count > 0)
                            {
                                Columss = badcolums - 1;
                                int iColums = 0;

                                var textremark = "";
                                foreach (var itemDeteil in dataResdamaged)
                                {
                                    //using (System.Drawing.Image imageD = System.Drawing.Image.FromFile("C:\\Users\\70059245\\Desktop\\2.jpg"))
                                    //var PathImageDemo = "C:\\Users\\70059245\\Desktop\\ei_1722423138878149914282260398748T1.jpg";
                                    var PathImageDemo = itemDeteil.attachImage;
                                    //string[] PathImageDemo2 = itemDeteil.attachImage.Split('\\');
                                    //PathImageDemo = "D:\\WorkSpaceWeb\\MaiConfirm\\shopImages\\" + PathImageDemo2[PathImageDemo2.Length-1];
                                    //PathImageDemo = "D:\\WorkSpaceWeb\\MaiConfirm\\assetImages\\" + PathImageDemo2[PathImageDemo2.Length-1];
                                    string[] FileNameArray = PathImageDemo.Split('\\');
                                    var FileName = FileNameArray[FileNameArray.Length - 1];
                                    string[] FileNameArrayChild = FileName.Split('.');
                                    var filePathSave = FileNameArrayChild[0] + "Copy" + periodId + "." + FileNameArrayChild[1];
                                    System.Drawing.Image imageReQuality = System.Drawing.Image.FromFile(PathImageDemo);
                                    SaveJpeg(PathTempImg + "\\"+ filePathSave, imageReQuality, 40);
                                    System.Drawing.Image imageD = System.Drawing.Image.FromFile(PathTempImg + "\\" + filePathSave);
                                    using (imageD)
                                    //using (System.Drawing.Image imageD = System.Drawing.Image.FromFile(itemDeteil.attachImage))
                                    {
                                        var excelImageB = worksheet.Drawings.AddPicture(item.shopCode + "bad" + item.assetCode.ToString() + "" + iColums + rnd.Next(), imageD);
                                        excelImageB.SetSize(50, 50);
                                        excelImageB.SetPosition(Rowss - 1, 3, Columss + iColums, 3);
                                        worksheet.Row(Rowss).Height = 60;
                                    }
                                    if (itemDeteil.remarkimage != "")
                                    {
                                        textremark += (iColums + 1) + ". " + itemDeteil.remarkimage + "\n";
                                        checkh2++;
                                        //if (iColums != 0)
                                        //{
                                        //    textremark += "\n";
                                        //}
                                    }

                                    iColums++;
                                }

                                Columss = badcolums3;
                                worksheet.Cells[Rowss, Columss, Rowss, Columss].Value = textremark;
                                worksheet.Cells[Rowss, Columss, Rowss, Columss].Style.WrapText = true;
                                worksheet.Cells[Rowss, Columss, Rowss, Columss].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                //worksheet.Column(Columss).AutoFit();

                            }
                            if (checkh1 < checkh2)
                            {
                                if (checkh2 < 4)
                                {
                                    worksheet.Row(Rowss).Height = 50;
                                }
                                else
                                {
                                    worksheet.Row(Rowss).Height = 15 * checkh2;
                                }

                            }
                            else
                            {
                                if (checkh1 < 4)
                                {
                                    worksheet.Row(Rowss).Height = 50;
                                }
                                else
                                {
                                    worksheet.Row(Rowss).Height = 15 * checkh1;
                                }
                            }


                        }

                        for (int i = 2; i <= badcolums3; i++)
                        {
                            worksheet.Cells[Rowss, 2, Rowss, i].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        }

                        countImage++;
                        Rowss++;
                        Columss = 2;
                    }

                    #region [create directory]
                    string excelFilePath = Path.Combine(PathExportFile, "รายงานสรุปผลการตรวจยืนยันยอดลูกหนี้(รูปภาพ)" + DateTime.Now.ToString("ddMMyyyy") + ".xlsx");

                    if (System.IO.File.Exists(excelFilePath))
                        System.IO.File.Delete(excelFilePath);

                    FileInfo excelFile = new FileInfo(excelFilePath);
                    excel.SaveAs(excelFile);

                    var stream = new FileStream(excelFilePath, FileMode.Open, FileAccess.Read);
                    return File(stream, "application/octet-stream", Path.GetFileName(excelFilePath));
                    #endregion
                }
            }
            catch (Exception ex)
            {
                ObjectResult messageCode = null;
                if (ex.Message == ConstMsg.FIND_REPORT_AUDIT_SHOP_NOT_FOUND)
                    messageCode = StatusCode((int)HttpStatusCode.NotFound, ex.Message);
                else
                    messageCode = StatusCode((int)HttpStatusCode.BadRequest, ex.Message);

                return messageCode;
            }
            finally {
                //System.IO.DirectoryInfo di = new DirectoryInfo(PathTempImg);

                //foreach (FileInfo file in di.GetFiles())
                //{
                //    file.Delete();
                //}

                //string[] fileNames = Directory.GetFiles(PathTempImg).Where(filename => filename.Contains("Copy" + periodId));
                var fileNames = Directory.GetFiles(PathTempImg).Where(filename => filename.Contains("Copy" + periodId));
                foreach (string fileName in fileNames)
                    System.IO.File.Delete(fileName);
            }
            return null;



        }

        /// <summary>
        /// POST parameter taskID: int?
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("BeforeCommitShopOnStatusShop")]
        public BeforeCommitShopOnStatusShopModel BeforeCommitShopOnStatusShop(BeforeCommitShopOnStatusShopReq req)
        {
            BeforeCommitShopOnStatusShopModel model = new BeforeCommitShopOnStatusShopModel();
            try
            {
                if (req.taskID == null)
                    throw new Exception(ConstMsg.INVALID_PARAMETER + " taskID");

                model = _exportFile.BeforeCommitShopOnStatusShopBC(req);
            }
            catch (Exception ex)
            {
                model.status = ConstMsg.STATUS_ERROR;
                model.message = ex.Message;
                _log.LogInformation("Exception Message : " + ex.Message);
                _log.LogInformation("Exception StackTrace : " + ex.StackTrace);

                if (ex.InnerException != null)
                {
                    _log.LogInformation("InnerException Message : " + ex.InnerException.Message);
                    _log.LogInformation("InnerException StackTrace : " + ex.InnerException.StackTrace);
                }
            }

            return model;
        }

        [HttpGet("TestIamgequality")]
        public async Task<IActionResult> TestIamgequality()
        {
            try
            {
                System.Drawing.Image imageD = System.Drawing.Image.FromFile("C:\\Users\\70059245\\Desktop\\ei_1722423138878149914282260398748T1.jpg");

                System.Drawing.Image imageD2 = System.Drawing.Image.FromFile("C:\\Users\\70059245\\Desktop\\2.jpg");

                SaveJpeg("C:\\Users\\70059245\\Desktop\\ei_1722423138878149914282260398748T1_COPY.jpg", imageD, 40);
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public static void SaveJpeg(string path, Image img, int quality)
        {
            if (quality < 0 || quality > 100)
                throw new ArgumentOutOfRangeException("quality must be between 0 and 100.");

            // Encoder parameter for image quality 
            EncoderParameter qualityParam = new EncoderParameter(Encoder.Quality, quality);
            // JPEG image codec 
            ImageCodecInfo jpegCodec = GetEncoderInfo("image/jpeg");
            EncoderParameters encoderParams = new EncoderParameters(1);
            encoderParams.Param[0] = qualityParam;
            img.Save(path, jpegCodec, encoderParams);
        }
        private static ImageCodecInfo GetEncoderInfo(string mimeType)
        {
            // Get image codecs for all image formats 
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();

            // Find the correct image codec 
            for (int i = 0; i < codecs.Length; i++)
                if (codecs[i].MimeType == mimeType)
                    return codecs[i];

            return null;
        }

    }
    public class FileHttpResponseMessage : HttpResponseMessage
    {
        private string filePath;

        public FileHttpResponseMessage(string filePath)
        {
            this.filePath = filePath;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            Content.Dispose();

            File.Delete(filePath);
        }
    }

 
}
