﻿using System;
using System.Collections.Generic;

#nullable disable

namespace MAIConfirm.EFModel.Entities
{
    public partial class User
    {
        public int Id { get; set; }
        public int EmpId { get; set; }
        public string Password { get; set; }
        public string AvaliableRegion { get; set; }
        public byte[] CreatedAt { get; set; }
    }
}
