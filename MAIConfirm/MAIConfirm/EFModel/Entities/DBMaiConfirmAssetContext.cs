﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace MAIConfirm.EFModel.Entities
{
    public partial class DBMaiConfirmAssetContext : DbContext
    {
        public DBMaiConfirmAssetContext()
        {
        }

        public DBMaiConfirmAssetContext(DbContextOptions<DBMaiConfirmAssetContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Asset> Assets { get; set; }
        public virtual DbSet<AssetType> AssetTypes { get; set; }
        public virtual DbSet<CheckShop> CheckShops { get; set; }
        public virtual DbSet<CheckShopDetail> CheckShopDetails { get; set; }
        public virtual DbSet<CheckShopKeeper> CheckShopKeepers { get; set; }
        public virtual DbSet<CheckShopSigned> CheckShopSigneds { get; set; }
        public virtual DbSet<CheckShopTransaction> CheckShopTransactions { get; set; }
        public virtual DbSet<CheckShopTransactionDetail> CheckShopTransactionDetails { get; set; }
        public virtual DbSet<Checker> Checkers { get; set; }
        public virtual DbSet<Company> Companies { get; set; }
        public virtual DbSet<Department> Departments { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Period> Periods { get; set; }
        public virtual DbSet<Shop> Shops { get; set; }
        public virtual DbSet<Shopkeeper> Shopkeepers { get; set; }
        public virtual DbSet<Task> Tasks { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=10.5.56.35;Database=DBMai_Confirm_Asset_PRD;User ID=sa;Password=P@ssw0rd");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Thai_CS_AS");

            modelBuilder.Entity<Asset>(entity =>
            {
                entity.ToTable("Asset");

                entity.Property(e => e.AssetId).HasColumnName("assetId");

                entity.Property(e => e.AccountType)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("accountType");

                entity.Property(e => e.Amount).HasColumnName("amount");

                entity.Property(e => e.AssetCode)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("assetCode");

                entity.Property(e => e.AssetName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("assetName");

                entity.Property(e => e.AssetType)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("assetType");

                entity.Property(e => e.CompCode)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("compCode");

                entity.Property(e => e.CreatedAt)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken()
                    .HasColumnName("created_at");

                entity.Property(e => e.DepartmentId).HasColumnName("departmentId");

                entity.Property(e => e.IsNew).HasColumnName("isNew");

                entity.Property(e => e.Price).HasColumnName("price");

                entity.Property(e => e.Region)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("region");

                entity.Property(e => e.Status)
                    .HasMaxLength(255)
                    .HasColumnName("status");

                entity.Property(e => e.UnitCount)
                    .HasMaxLength(255)
                    .HasColumnName("unitCount");
            });

            modelBuilder.Entity<AssetType>(entity =>
            {
                entity.ToTable("AssetType");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreatedAt)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken()
                    .HasColumnName("created_at");

                entity.Property(e => e.TypeId).HasColumnName("typeId");

                entity.Property(e => e.TypeName)
                    .HasMaxLength(50)
                    .HasColumnName("typeName");
            });

            modelBuilder.Entity<CheckShop>(entity =>
            {
                entity.ToTable("CheckShop");

                entity.Property(e => e.CheckShopId).HasColumnName("checkShopId");

                entity.Property(e => e.CheckLocation)
                    .HasMaxLength(255)
                    .HasColumnName("checkLocation");

                entity.Property(e => e.CreatedAt)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken()
                    .HasColumnName("created_at");

                entity.Property(e => e.Remark)
                    .HasMaxLength(255)
                    .HasColumnName("remark");

                entity.Property(e => e.ShopImage)
                    .HasMaxLength(255)
                    .HasColumnName("shopImage");

                entity.Property(e => e.ShopStatus)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("shopStatus");

                entity.Property(e => e.TaskId).HasColumnName("taskID");
            });

            modelBuilder.Entity<CheckShopDetail>(entity =>
            {
                entity.ToTable("CheckShopDetail");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CheckShopId).HasColumnName("checkShopId");

                entity.Property(e => e.CreatedAt)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken()
                    .HasColumnName("created_at");

                entity.Property(e => e.CustomerName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("customerName");

                entity.Property(e => e.PhoneNumber)
                    .HasMaxLength(255)
                    .HasColumnName("phoneNumber");

                entity.Property(e => e.Relation)
                    .HasMaxLength(255)
                    .HasColumnName("relation");
            });

            modelBuilder.Entity<CheckShopKeeper>(entity =>
            {
                entity.ToTable("CheckShopKeeper");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CheckShopId).HasColumnName("checkShopId");

                entity.Property(e => e.CompCode)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("compCode");

                entity.Property(e => e.CreatedAt)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken()
                    .HasColumnName("created_at");

                entity.Property(e => e.EmpId)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("empId");

                entity.Property(e => e.EmpName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("empName");

                entity.Property(e => e.IsActive).HasColumnName("isActive");
            });

            modelBuilder.Entity<CheckShopSigned>(entity =>
            {
                entity.ToTable("CheckShopSigned");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AssetTypeId).HasColumnName("assetTypeID");

                entity.Property(e => e.CheckShopId).HasColumnName("checkShopId");

                entity.Property(e => e.CheckerId)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("checkerId");

                entity.Property(e => e.CompCode)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("compCode");

                entity.Property(e => e.ConfirmStatus)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("confirmStatus");

                entity.Property(e => e.CreatedAt)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken()
                    .HasColumnName("created_at");

                entity.Property(e => e.CustomerSigned)
                    .HasMaxLength(255)
                    .HasColumnName("customerSigned");

                entity.Property(e => e.EmployeeSigned)
                    .HasMaxLength(255)
                    .HasColumnName("employeeSigned");

                entity.Property(e => e.Remark)
                    .HasMaxLength(255)
                    .HasColumnName("remark");
            });

            modelBuilder.Entity<CheckShopTransaction>(entity =>
            {
                entity.ToTable("CheckShopTransaction");

                entity.Property(e => e.CheckShopTransactionId).HasColumnName("checkShopTransactionId");

                entity.Property(e => e.AccountBalance).HasColumnName("accountBalance");

                entity.Property(e => e.AssetId).HasColumnName("assetId");

                entity.Property(e => e.AssetTypeId).HasColumnName("assetTypeID");

                entity.Property(e => e.Avaliable).HasColumnName("avaliable");

                entity.Property(e => e.CheckShopId).HasColumnName("checkShopId");

                entity.Property(e => e.CompCode)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("compCode");

                entity.Property(e => e.CreatedAt)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken()
                    .HasColumnName("created_at");

                entity.Property(e => e.Damaged).HasColumnName("damaged");
            });

            modelBuilder.Entity<CheckShopTransactionDetail>(entity =>
            {
                entity.ToTable("CheckShopTransactionDetail");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Amount).HasColumnName("amount");

                entity.Property(e => e.AssetStatus)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("assetStatus");

                entity.Property(e => e.AttachImage)
                    .HasMaxLength(255)
                    .HasColumnName("attachImage");

                entity.Property(e => e.CheckShopTransactionId).HasColumnName("checkShopTransactionId");

                entity.Property(e => e.CreatedAt)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken()
                    .HasColumnName("created_at");

                entity.Property(e => e.Remark)
                    .HasMaxLength(255)
                    .HasColumnName("remark");
            });

            modelBuilder.Entity<Checker>(entity =>
            {
                entity.ToTable("Checker");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CheckShopId).HasColumnName("checkShopId");

                entity.Property(e => e.CompCode)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("compCode");

                entity.Property(e => e.CreatedAt)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken()
                    .HasColumnName("created_at");

                entity.Property(e => e.EmpId)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("empId");

                entity.Property(e => e.EmpName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("empName");

                entity.Property(e => e.IsActive).HasColumnName("isActive");
            });

            modelBuilder.Entity<Company>(entity =>
            {
                entity.ToTable("Company");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.Address)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("address");

                entity.Property(e => e.CompCode)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("compCode");

                entity.Property(e => e.CompanyName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("companyName");

                entity.Property(e => e.CreatedAt)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken()
                    .HasColumnName("created_at");

                entity.Property(e => e.Region).HasColumnName("region");
            });

            modelBuilder.Entity<Department>(entity =>
            {
                entity.ToTable("Department");

                entity.Property(e => e.DepartmentId).HasColumnName("departmentId");

                entity.Property(e => e.Area)
                    .HasMaxLength(255)
                    .HasColumnName("area");

                entity.Property(e => e.Center)
                    .HasMaxLength(255)
                    .HasColumnName("center");

                entity.Property(e => e.Channel)
                    .HasMaxLength(255)
                    .HasColumnName("channel");

                entity.Property(e => e.CompCode)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("compCode");

                entity.Property(e => e.CreatedAt)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken()
                    .HasColumnName("created_at");

                entity.Property(e => e.DepartmentName)
                    .HasMaxLength(255)
                    .HasColumnName("departmentName");

                entity.Property(e => e.Province)
                    .HasMaxLength(255)
                    .HasColumnName("province");

                entity.Property(e => e.Region)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("region");
            });

            modelBuilder.Entity<Employee>(entity =>
            {
                entity.HasKey(e => e.EmpId);

                entity.ToTable("Employee");

                entity.Property(e => e.EmpId)
                    .ValueGeneratedNever()
                    .HasColumnName("empId");

                entity.Property(e => e.CompCode)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("compCode");

                entity.Property(e => e.CreatedAt)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken()
                    .HasColumnName("created_at");

                entity.Property(e => e.DepartmentId).HasColumnName("departmentId");

                entity.Property(e => e.Email)
                    .HasMaxLength(255)
                    .HasColumnName("email");

                entity.Property(e => e.EmpName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("empName");

                entity.Property(e => e.EmpPosition)
                    .HasMaxLength(255)
                    .HasColumnName("empPosition");

                entity.Property(e => e.Region)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("region");

                entity.Property(e => e.Signature)
                    .HasMaxLength(255)
                    .HasColumnName("signature");
            });

            modelBuilder.Entity<Period>(entity =>
            {
                entity.ToTable("Period");

                entity.Property(e => e.PeriodId).HasColumnName("periodId");

                entity.Property(e => e.CreatedAt)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken()
                    .HasColumnName("created_at");

                entity.Property(e => e.EndDate)
                    .HasColumnType("date")
                    .HasColumnName("endDate");

                entity.Property(e => e.PeriodNname)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("periodNName");

                entity.Property(e => e.RegionId)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("regionId");

                entity.Property(e => e.StartDate)
                    .HasColumnType("date")
                    .HasColumnName("startDate");
            });

            modelBuilder.Entity<Shop>(entity =>
            {
                entity.ToTable("Shop");

                entity.Property(e => e.ShopId).HasColumnName("shopId");

                entity.Property(e => e.Address)
                    .HasMaxLength(255)
                    .HasColumnName("address");

                entity.Property(e => e.CreatedAt)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken()
                    .HasColumnName("created_at");

                entity.Property(e => e.Location)
                    .HasMaxLength(255)
                    .HasColumnName("location");

                entity.Property(e => e.ShopCode)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("shopCode");

                entity.Property(e => e.ShopName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("shopName");
            });

            modelBuilder.Entity<Shopkeeper>(entity =>
            {
                entity.ToTable("Shopkeeper");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreatedAt)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken()
                    .HasColumnName("created_at");

                entity.Property(e => e.EmpId).HasColumnName("empId");

                entity.Property(e => e.IsNew).HasColumnName("isNew");

                entity.Property(e => e.ShopId).HasColumnName("shopId");
            });

            modelBuilder.Entity<Task>(entity =>
            {
                entity.ToTable("Task");

                entity.Property(e => e.TaskId).HasColumnName("taskID");

                entity.Property(e => e.AccountDate)
                    .HasColumnType("datetime")
                    .HasColumnName("accountDate");

                entity.Property(e => e.CheckDate)
                    .HasColumnType("date")
                    .HasColumnName("checkDate");

                entity.Property(e => e.CreatedAt)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken()
                    .HasColumnName("created_at");

                entity.Property(e => e.PeriodId).HasColumnName("periodId");

                entity.Property(e => e.PlanDate)
                    .HasColumnType("date")
                    .HasColumnName("planDate");

                entity.Property(e => e.ShopId).HasColumnName("shopId");

                entity.Property(e => e.TaskName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("taskName");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("User");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AvaliableRegion)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("avaliableRegion");

                entity.Property(e => e.CreatedAt)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken()
                    .HasColumnName("created_at");

                entity.Property(e => e.EmpId).HasColumnName("empId");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("password");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
