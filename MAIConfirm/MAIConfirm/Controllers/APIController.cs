﻿using MAIConfirm.Business;
using MAIConfirm.Business.Interface;
using MAIConfirm.EFModel.BackEndModel;
using MAIConfirm.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Text.Json;
using System.Threading.Tasks;
using System.Web;

namespace MAIConfirm.Controllers
{
    [Route("api/")]
    [ApiController]
    public class APIController : ControllerBase
    {
        static HttpClient client = new HttpClient();
        const String url = "https://ss.thaibev.com/mai-confirm-asset/";
        const String folderShopImage = "shopImages";
        const String folderSignature = "signatureImages";
        const String folderCheckAsset = "assetImages";
        const String folderAssetTemplate = "assetTemplate";
        readonly String shopImagePath = Path.Combine(Directory.GetCurrentDirectory(), folderShopImage);
        readonly String signaturePath = Path.Combine(Directory.GetCurrentDirectory(), folderSignature);
        readonly String checkAssetParh = Path.Combine(Directory.GetCurrentDirectory(), folderCheckAsset);
        readonly String assetTemplatePath = Path.Combine(Directory.GetCurrentDirectory(), folderAssetTemplate);

        readonly IConfiguration _configuration;
        IMaiConfirm _maiConfirm;

        private readonly MAIConfirmAssetDbContext _context;

        static IConfiguration conf = (new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build());
        public static string MANAGER_MCMC = conf["AppSettings:MANAGER_MCMC"].ToString();
        public static string urlConnectToken = conf["AppSettings:URL_CONNECT_TOKEN"].ToString();
        public static string urlEmpInfo = conf["AppSettings:URL_EMP_INFO"].ToString();

        public APIController(MAIConfirmAssetDbContext context, IConfiguration configuration, IMaiConfirm maiConfirm)
        {
            _context = context;
            _configuration = configuration;
            _maiConfirm = maiConfirm;

            if (!Directory.Exists(shopImagePath))
            {
                Directory.CreateDirectory(shopImagePath);
            }

            if (!Directory.Exists(signaturePath))
            {
                Directory.CreateDirectory(signaturePath);
            }

            if (!Directory.Exists(checkAssetParh))
            {
                Directory.CreateDirectory(checkAssetParh);
            }
            if (!Directory.Exists(assetTemplatePath))
            {
                Directory.CreateDirectory(assetTemplatePath);
            }
        }

        [HttpPost("login")]
        public async Task<ActionResult<LoginResponse>> Login(Login login)
        {
            try
            {
                GetConnectTokenModel getConnectTokenModel = null;

                var checkLogin = login.userId.Split("@");
                var checkLogi33n = checkLogin.Count();
                if (checkLogin.Count() == 1)
                {
                    using (var multiPartStream = new MultipartFormDataContent())
                    {

                        multiPartStream.Add(new StringContent("password"), "grant_type");
                        multiPartStream.Add(new StringContent("emppoint_client"), "client_id");
                        multiPartStream.Add(new StringContent("thaibev.privilege.readonly user.readonly"), "scope");
                        multiPartStream.Add(new StringContent("Th@ib3v"), "client_secret");
                        multiPartStream.Add(new StringContent(login.userId), "username");
                        multiPartStream.Add(new StringContent(login.password), "password");

                        var res = await client.PostAsync(urlConnectToken, multiPartStream);
                        var strinResp = await res.Content.ReadAsStringAsync();

                        getConnectTokenModel = new GetConnectTokenModel();
                        getConnectTokenModel = DeserializeObject<GetConnectTokenModel>(strinResp);

                    }
                }
                else {
                    getConnectTokenModel = new GetConnectTokenModel();
                }

                if (string.IsNullOrEmpty(getConnectTokenModel.error))
                {
                    var user  = _context.Users.Where(c => (c.empId == int.Parse(login.userId.Replace("@","")))).FirstOrDefault();

                    var employee = _context.Employees.Where(c => c.empId == user.empId).First();

                    var companies = _context.Companies.Where(c => c.region.ToString() == employee.region).ToList();

                    return await System.Threading.Tasks.Task.FromResult(new LoginResponse
                    {
                        status = "success",
                        message = "เข้าสู่ระบบสำเร็จ",
                        avaliableRegion = user.avaliableRegion,
                        user = employee,
                        companies = companies,
                    });
                }
                else
                    throw new Exception(getConnectTokenModel.error_description.Replace("invalid_username_or_password", "รหัสพนักงานหรือรหัสผ่านผิด"));

              
            }
            catch (Exception)
            {
                return await Task.FromResult(new LoginResponse
                {
                    status = "error",
                    message = "รหัสพนักงาน หรือรหัสผ่านไม่ถูกต้อง",
                });
            }
        }

        [HttpPost("loginOnTablet")]
        public async Task<ActionResult<LoginResponse>> loginOnTablet(LoginV2 login)
        {
            try
            {
                var user = _context.Users.Where(c => (c.empId == int.Parse(login.userId))).FirstOrDefault();

                var employee = _context.Employees.Where(c => c.empId == user.empId).First();

                var companies = _context.Companies.Where(c => c.region.ToString() == employee.region).ToList();

                return await System.Threading.Tasks.Task.FromResult(new LoginResponse
                {
                    status = "success",
                    message = "เข้าสู่ระบบสำเร็จ",
                    avaliableRegion = user.avaliableRegion,
                    user = employee,
                    companies = companies,
                });
            }
            catch (Exception)
            {
                return await Task.FromResult(new LoginResponse
                {
                    status = "error",
                    message = "รหัสพนักงาน หรือรหัสผ่านไม่ถูกต้อง",
                });
            }
        }

        [HttpGet("getPlan/{region}")]
        public async Task<ActionResult<PeriodResponse>> getPlan(String region)
        {
            try
            {
                var period = _context.Periods.Where(c => c.regionId == region).Select(e => new Period
                {
                    periodId = e.periodId,
                    periodNName = string.Concat(e.periodNName, " : แผนงานเลขที่ "+ e.periodId),
                    regionId = e.regionId,
                    startDate = e.startDate,
                    endDate = e.endDate
                }).ToList();
                if (period.Count > 0)
                {
                    period.Reverse();
                }
                return await Task.FromResult(new PeriodResponse
                {
                    status = "success",
                    message = "",
                    periods = period
                });
            }
            catch (Exception e)
            {
                return await Task.FromResult(new PeriodResponse
                {
                    status = "error",
                    message = e.Message,
                });
            }
        }

        [HttpPost("createPeriod")]
        public async Task<ActionResult<Response>> CreatePeriod(CreateTemplateRequest request)
        {
            _context.Database.BeginTransaction();
            try
            {
                var period = new Period
                {
                    periodNName = request.periodName,
                    regionId = request.region,
                    startDate = request.startDate,
                    endDate = request.endDate
                };

                _context.Periods.Add(period);
                await _context.SaveChangesAsync();
                var periodData = await _context.Periods.FindAsync(period.periodId);

                foreach (var data in request.rawData)
                {

                    //add shop
                    var shop = new Shop
                    {
                        shopName = data.rawTemplates[0].customerName,
                        address = data.rawTemplates[0].address,
                        location = data.rawTemplates[0].location,
                        shopCode = data.rawTemplates[0].customerCode,
                    };
                    _context.Shops.Add(shop);
                    await _context.SaveChangesAsync();
                    var shopData = await _context.Shops.FindAsync(shop.shopId);

                    //create task
                    var task = new AssetTask
                    {
                        shopId = shopData.shopId,
                        periodId = periodData.periodId,
                        taskName = shopData.shopName,
                        planDate = Convert.ToDateTime(data.rawTemplates[0].planDate),
                        accountDate = Convert.ToDateTime(data.rawTemplates[0].accountDate),
                    };
                    _context.Tasks.Add(task);
                    await _context.SaveChangesAsync();
                    var taskData = await _context.Tasks.FindAsync(task.taskID);

                    //craete check shop
                    var checkShop = new CheckShop
                    {
                        taskID = taskData.taskID,
                        shopStatus = "unchecked",
                    };
                    _context.CheckShops.Add(checkShop);
                    await _context.SaveChangesAsync();
                    var checkShopData = await _context.CheckShops.FindAsync(checkShop.checkShopId);

                    foreach (var raw in data.rawTemplates)
                    {
                        if (taskData.planDate != Convert.ToDateTime(raw.planDate))
                        {
                            var taskList = _context.Tasks.Where(c => c.shopId == shopData.shopId).ToList();
                            if (taskList.Count > 0)
                            {
                                var isInList = false;
                                foreach (var addedTask in taskList)
                                {
                                    if (addedTask.planDate == Convert.ToDateTime(raw.planDate))
                                    {
                                        isInList = true;
                                        taskData = addedTask;
                                    }
                                }
                                if (!isInList)
                                {
                                    var newTask = new AssetTask
                                    {
                                        shopId = shopData.shopId,
                                        periodId = periodData.periodId,
                                        taskName = shopData.shopName,
                                        planDate = Convert.ToDateTime(raw.planDate),
                                        accountDate = Convert.ToDateTime(raw.accountDate),
                                    };
                                    _context.Tasks.Add(newTask);
                                    await _context.SaveChangesAsync();
                                    taskData = await _context.Tasks.FindAsync(newTask.taskID);

                                    var newCheckShop = new CheckShop
                                    {
                                        taskID = taskData.taskID,
                                        shopStatus = "unchecked",
                                    };
                                    _context.CheckShops.Add(newCheckShop);
                                    await _context.SaveChangesAsync();
                                    checkShopData = await _context.CheckShops.FindAsync(newCheckShop.checkShopId);
                                }
                                else
                                {
                                    checkShopData = await _context.CheckShops.FindAsync(taskData.taskID);
                                    if (checkShopData == null)
                                    {
                                        var newCheckShop = new CheckShop
                                        {
                                            taskID = taskData.taskID,
                                            shopStatus = "unchecked",
                                        };
                                        _context.CheckShops.Add(newCheckShop);
                                        await _context.SaveChangesAsync();
                                        checkShopData = await _context.CheckShops.FindAsync(newCheckShop.checkShopId);
                                    }
                                }
                            }
                            else
                            {
                                var newTask = new AssetTask
                                {
                                    shopId = shopData.shopId,
                                    periodId = periodData.periodId,
                                    taskName = shopData.shopName,
                                    planDate = Convert.ToDateTime(raw.planDate),
                                    accountDate = Convert.ToDateTime(raw.accountDate),

                                };
                                _context.Tasks.Add(newTask);
                                await _context.SaveChangesAsync();
                                taskData = await _context.Tasks.FindAsync(newTask.taskID);

                                var newCheckShop = new CheckShop
                                {
                                    taskID = taskData.taskID,
                                    shopStatus = "unchecked",
                                };
                                _context.CheckShops.Add(newCheckShop);
                                await _context.SaveChangesAsync();
                                checkShopData = await _context.CheckShops.FindAsync(newCheckShop.checkShopId);
                            }
                        }

                        var company = _context.Companies.Where(c => raw.companyName.Contains(c.companyName)).FirstOrDefault();

                        if (company == null)
                        {
                            return await Task.FromResult(new Response
                            {
                                status = "error",
                                message = "ไม่พบข้อมูล " + raw.companyName
                            });
                        }

                        //add department
                        var depratment = new Department
                        {
                            departmentName = raw.department,
                            region = raw.region,
                            compCode = company.compCode,
                            area = raw.area,
                            center = raw.center,
                            channel = raw.channel,
                            province = raw.province,
                        };

                        var addedDepartment = _context.Departments.Where(c => c.departmentName == depratment.departmentName && c.channel == depratment.channel).ToList();
                        var departmentData = new Department();
                        if (addedDepartment.Count > 0)
                        {
                            departmentData = addedDepartment[0];
                        }
                        else
                        {
                            _context.Departments.Add(depratment);
                            await _context.SaveChangesAsync();
                            departmentData = await _context.Departments.FindAsync(depratment.departmentId);
                        }

                        //get employee
                        var employee = GetEmployeeData(raw);
                        if (employee == null)
                        {
                            if (raw.employeeId == null || raw.employeeId == "" || raw.employeeId == "null")
                            {
                                return await Task.FromResult(new Response
                                {
                                    status = "error",
                                    message = "ไม่พบรหัสพนักงานของ " + raw.employeeName + " ในไฟล์ Template",
                                });
                            }
                            var empData = new Employee
                            {
                                empId = int.Parse(raw.employeeId),
                                empName = raw.employeeName,
                                region = raw.region,
                                compCode = company.compCode,
                                departmentId = departmentData.departmentId
                            };
                            _context.Employees.Add(empData);

                            if (!CheckShopKeeper(raw, shopData.shopId))
                            {
                                var shopKeeper = new Shopkeeper
                                {
                                    empId = empData.empId,
                                    shopId = shopData.shopId,
                                };
                                _context.Shopkeepers.Add(shopKeeper);
                            }
                        }
                        else
                        {
                            if (!CheckShopKeeper(raw, shopData.shopId))
                            {
                                var shopKeeper = new Shopkeeper
                                {
                                    empId = employee.empId,
                                    shopId = shopData.shopId,
                                };
                                _context.Shopkeepers.Add(shopKeeper);
                            }
                        }
                        await _context.SaveChangesAsync();

                        double price = 0;
                        if (raw.unitPrice == null || raw.unitPrice.Trim() == "-" || raw.unitPrice.Trim() == "" || raw.unitPrice.Trim() == "null")
                        {
                            price = 0;
                        }
                        else
                        {
                            price = double.Parse(raw.unitPrice);
                        }
                        //add asset
                        var asset = new Asset
                        {
                            assetCode = raw.assetCode,
                            assetName = raw.assetName,
                            assetType = raw.assetType,
                            accountType = raw.assetStatus,
                            region = raw.region,
                            compCode = departmentData.compCode,
                            departmentId = departmentData.departmentId,
                            unitCount = raw.unitCount,
                            price = price,
                            amount = int.Parse(raw.accountAmount),
                        };
                        _context.Assets.Add(asset);
                        await _context.SaveChangesAsync();
                        var assetData = await _context.Assets.FindAsync(asset.assetId);
                        var type = _context.AssetTypes.Where(c => c.typeName == assetData.assetType).First();

                        //create asset transaction
                        var transaction = new CheckShopTransaction
                        {
                            checkShopId = checkShopData.checkShopId,
                            assetId = assetData.assetId,
                            compCode = assetData.compCode,
                            assetTypeID = type.typeId,
                            accountBalance = int.Parse(raw.accountAmount),
                        };
                        _context.CheckShopTransactions.Add(transaction);
                        await _context.SaveChangesAsync();
                    }

                }
            }
            catch (Exception e)
            {
                _context.Database.RollbackTransaction();
                return await Task.FromResult(new Response
                {
                    status = "error",
                    message = e.Message
                });
            }

            _context.Database.CommitTransaction();
            return await Task.FromResult(new Response
            {
                status = "success",
                message = "สร้างแผนงานสำเร็จ"
            });
        }

        [HttpPut("editShop/{taskId}")]
        public async Task<ActionResult<Response>> editShop(CreateTemplateRequest request, int taskId)
        {
            _context.Database.BeginTransaction();

            //delete old task
            var task = _context.Tasks.Where(c => c.taskID == taskId).First();
            if (task != null)
            {
                try
                {
                    var checkShop = _context.CheckShops.Where(c => c.taskID == task.taskID).First();
                    var checkShopTrans = _context.CheckShopTransactions.Where(c => c.checkShopId == checkShop.checkShopId).ToList();

                    var checkSigned = _context.CheckShopSigneds.Where(c => c.checkShopId == checkShop.checkShopId).ToList();
                    if (checkSigned.Count > 0)
                    {
                        return await Task.FromResult(new Response
                        {
                            status = "error",
                            message = "ไม่สามารถแก้ไขได้ เนื่องจากมีการเซ็นชื่อแล้ว"
                        });
                    }
                    if (checkShopTrans.Count > 0)
                    {
                        List<Asset> assets = new();
                        foreach (var tran in checkShopTrans)
                        {
                            var transDetail = _context.CheckShopTransactionDetails.Where(c => c.checkShopTransactionId == tran.checkShopTransactionId).ToList();
                            if (transDetail.Count > 0)
                            {
                                _context.CheckShopTransactionDetails.RemoveRange(transDetail);
                            }

                            var asset = _context.Assets.Where(c => c.assetId == tran.assetId).First();
                            assets.Add(asset);
                        }

                        await _context.SaveChangesAsync();

                        _context.CheckShopTransactions.RemoveRange(checkShopTrans);
                        await _context.SaveChangesAsync();

                        _context.Assets.RemoveRange(assets);
                        await _context.SaveChangesAsync();
                    }
                    var checkDetail = _context.CheckShopDetails.Where(c => c.checkShopId == checkShop.checkShopId).ToList();
                    if (checkDetail.Count > 0)
                    {
                        _context.CheckShopDetails.RemoveRange(checkDetail);
                        await _context.SaveChangesAsync();
                    }
                    _context.CheckShops.Remove(checkShop);
                    await _context.SaveChangesAsync();

                    var shop = _context.Shops.Where(c => c.shopId == task.shopId).First();
                    var shopKeeper = _context.Shopkeepers.Where(c => c.shopId == shop.shopId).ToList();
                    if (shopKeeper.Count > 0)
                    {
                        _context.Shopkeepers.RemoveRange(shopKeeper);
                        await _context.SaveChangesAsync();
                    }
                }
                catch (Exception e)
                {
                    _context.Database.RollbackTransaction();
                    return await Task.FromResult(new Response
                    {
                        status = "error",
                        message = e.Message
                    });
                }
            }
            //end delete

            //update period
            try
            {
                var taskData = await _context.Tasks.FindAsync(taskId);
                var shopData = await _context.Shops.FindAsync(taskData.shopId);
                String planDate = "";

                foreach (var data in request.rawData)
                {
                    if (data.shopId == shopData.shopCode)
                    {
                        var checkShop = new CheckShop
                        {
                            taskID = taskData.taskID,
                            shopStatus = "unchecked",
                        };
                        _context.CheckShops.Add(checkShop);
                        await _context.SaveChangesAsync();
                        var checkShopData = await _context.CheckShops.FindAsync(checkShop.checkShopId);

                        foreach (var raw in data.rawTemplates)
                        {
                            planDate = raw.planDate;
                            var company = _context.Companies.Where(c => raw.companyName.Contains(c.companyName)).FirstOrDefault();
                            if (company == null)
                            {
                                return await Task.FromResult(new Response
                                {
                                    status = "error",
                                    message = "ไม่พบข้อมูล " + raw.companyName
                                });
                            }
                            //add department
                            var depratment = new Department
                            {
                                departmentName = raw.department,
                                region = raw.region,
                                compCode = company.compCode,
                                area = raw.area,
                                center = raw.center,
                                channel = raw.channel,
                                province = raw.province,
                            };
                            var addedDepartment = _context.Departments.Where(c => c.departmentName == depratment.departmentName && c.channel == depratment.channel).ToList();
                            var departmentData = new Department();
                            if (addedDepartment.Count > 0)
                            {
                                departmentData = addedDepartment[0];
                            }
                            else
                            {
                                _context.Departments.Add(depratment);
                                await _context.SaveChangesAsync();
                                departmentData = await _context.Departments.FindAsync(depratment.departmentId);
                            }
                            //get employee
                            var employee = GetEmployeeData(raw);
                            if (employee == null)
                            {
                                if (raw.employeeId == null || raw.employeeId == "" || raw.employeeId == "null")
                                {
                                    return await Task.FromResult(new Response
                                    {
                                        status = "error",
                                        message = "ไม่พบรหัสพนักงานของคุณ" + raw.employeeName + " ในไฟล์ Template",
                                    });
                                }
                                var empData = new Employee
                                {
                                    empId = int.Parse(raw.employeeId),
                                    empName = raw.employeeName,
                                    region = raw.region,
                                    compCode = company.compCode,
                                    departmentId = departmentData.departmentId
                                };
                                _context.Employees.Add(empData);

                                if (!CheckShopKeeper(raw, shopData.shopId))
                                {
                                    var shopKeeper = new Shopkeeper
                                    {
                                        empId = empData.empId,
                                        shopId = shopData.shopId,
                                    };
                                    _context.Shopkeepers.Add(shopKeeper);
                                }
                            }
                            else
                            {
                                if (!CheckShopKeeper(raw, shopData.shopId))
                                {
                                    var shopKeeper = new Shopkeeper
                                    {
                                        empId = employee.empId,
                                        shopId = shopData.shopId,
                                    };
                                    _context.Shopkeepers.Add(shopKeeper);
                                }
                            }
                            await _context.SaveChangesAsync();
                            //add asset
                            var asset = new Asset
                            {
                                assetCode = raw.assetCode,
                                assetName = raw.assetName,
                                assetType = raw.assetType,
                                accountType = raw.assetStatus,
                                region = raw.region,
                                compCode = departmentData.compCode,
                                departmentId = departmentData.departmentId,
                                unitCount = raw.unitCount,
                                price = double.Parse(raw.unitPrice),
                                amount = int.Parse(raw.accountAmount),
                            };
                            _context.Assets.Add(asset);
                            await _context.SaveChangesAsync();
                            var assetData = await _context.Assets.FindAsync(asset.assetId);
                            var type = _context.AssetTypes.Where(c => c.typeName == assetData.assetType).First();
                            //create asset transaction
                            var transaction = new CheckShopTransaction
                            {
                                checkShopId = checkShopData.checkShopId,
                                assetId = assetData.assetId,
                                compCode = assetData.compCode,
                                assetTypeID = type.typeId,
                                accountBalance = int.Parse(raw.accountAmount),
                            };
                            _context.CheckShopTransactions.Add(transaction);
                            await _context.SaveChangesAsync();
                        }
                    }
                    else
                    {
                        return await Task.FromResult(new Response
                        {
                            status = "error",
                            message = "รหัสร้านค้า " + data.shopId.ToString() + " ไม่ตรงกับร้านที่ต้องการแก้ไข",
                        });
                    }
                }

                task.planDate = Convert.ToDateTime(planDate);
                _context.Tasks.Update(task);
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                _context.Database.RollbackTransaction();
                return await Task.FromResult(new Response
                {
                    status = "error",
                    message = e.Message
                });
            }
            _context.Database.CommitTransaction();
            return await Task.FromResult(new Response
            {
                status = "success",
                message = "สร้างแผนงานสำเร็จ"
            });
        }

        private Employee GetEmployeeData(RawTemplate raw)
        {
            try
            {
                return _context.Employees.Where(c => c.empId == int.Parse(raw.employeeId)).First();
            }
            catch (Exception e)
            {
                return null;
            }
        }

        private bool CheckShopKeeper(RawTemplate raw, int shopId)
        {
            try
            {
                var keeperList = _context.Shopkeepers.Where(c => c.shopId == shopId).ToList();

                foreach (var data in keeperList)
                {
                    if (data.empId == int.Parse(raw.employeeId))
                    {
                        return true;
                    }
                }

                return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        [HttpPut("editPeriod/{periodId}")]
        public async Task<ActionResult<Response>> editPeriod(CreateTemplateRequest request, int periodId)
        {
            _context.Database.BeginTransaction();

            //delete old task
            var oldTasks = _context.Tasks.Where(c => c.periodId == periodId).ToList();
            if (oldTasks.Count > 0)
            {
                try
                {
                    foreach (var task in oldTasks)
                    {
                        var checkShops = _context.CheckShops.Where(c => c.taskID == task.taskID).ToList();
                        foreach (var checkShop in checkShops)
                        {
                            var checkShopTrans = _context.CheckShopTransactions.Where(c => c.checkShopId == checkShop.checkShopId).ToList();
                            var checkSigned = _context.CheckShopSigneds.Where(c => c.checkShopId == checkShop.checkShopId).ToList();
                            var checkShopKeeper = _context.CheckShopKeepers.Where(c => c.checkShopId == checkShop.checkShopId).ToList();

                            if (checkSigned.Count > 0)
                            {
                                return await Task.FromResult(new Response
                                {
                                    status = "error",
                                    message = "ไม่สามารถแก้ไขได้ เนื่องจากมีการเซ็นชื่อแล้ว"
                                });
                            }

                            if (checkShopKeeper.Count > 0)
                            {
                                return await Task.FromResult(new Response
                                {
                                    status = "error",
                                    message = "ไม่สามารถแก้ไขได้ เนื่องจากมีการตรวจยืนยันบางรายการแล้ว"
                                });
                            }

                            if (checkShopTrans.Count > 0)
                            {
                                List<Asset> assets = new();
                                foreach (var tran in checkShopTrans)
                                {
                                    var transDetail = _context.CheckShopTransactionDetails.Where(c => c.checkShopTransactionId == tran.checkShopTransactionId).ToList();
                                    if (transDetail.Count > 0)
                                    {
                                        _context.CheckShopTransactionDetails.RemoveRange(transDetail);
                                    }

                                    var asset = _context.Assets.Where(c => c.assetId == tran.assetId).First();
                                    assets.Add(asset);
                                }

                                await _context.SaveChangesAsync();

                                _context.CheckShopTransactions.RemoveRange(checkShopTrans);
                                await _context.SaveChangesAsync();

                                _context.Assets.RemoveRange(assets);
                                await _context.SaveChangesAsync();
                            }

                            var checkDetail = _context.CheckShopDetails.Where(c => c.checkShopId == checkShop.checkShopId).ToList();
                            if (checkDetail.Count > 0)
                            {
                                _context.CheckShopDetails.RemoveRange(checkDetail);
                                await _context.SaveChangesAsync();
                            }

                            _context.CheckShops.Remove(checkShop);
                            await _context.SaveChangesAsync();

                            var shop = _context.Shops.Where(c => c.shopId == task.shopId).First();
                            var shopKeeper = _context.Shopkeepers.Where(c => c.shopId == shop.shopId).ToList();
                            if (shopKeeper.Count > 0)
                            {
                                _context.Shopkeepers.RemoveRange(shopKeeper);
                                await _context.SaveChangesAsync();
                            }
                        }
                        _context.Tasks.Remove(task);
                        await _context.SaveChangesAsync();
                    }
                }
                catch (Exception e)
                {
                    _context.Database.RollbackTransaction();
                    return await Task.FromResult(new Response
                    {
                        status = "error",
                        message = e.Message
                    });
                }
            }
            //end delete

            //update period
            try
            {
                var period = new Period
                {
                    periodId = periodId,
                    periodNName = request.periodName,
                    regionId = request.region,
                    startDate = request.startDate,
                    endDate = request.endDate
                };

                _context.Periods.Update(period);
                await _context.SaveChangesAsync();

                var periodData = await _context.Periods.FindAsync(period.periodId);

                foreach (var data in request.rawData)
                {
                    //add shop
                    var shop = new Shop
                    {
                        shopName = data.rawTemplates[0].customerName,
                        address = data.rawTemplates[0].address,
                        location = data.rawTemplates[0].location,
                        shopCode = data.rawTemplates[0].customerCode,
                    };
                    _context.Shops.Add(shop);
                    await _context.SaveChangesAsync();
                    var shopData = await _context.Shops.FindAsync(shop.shopId);

                    //create task
                    var task = new AssetTask
                    {
                        shopId = shopData.shopId,
                        periodId = periodData.periodId,
                        taskName = shopData.shopName,
                        planDate = Convert.ToDateTime(data.rawTemplates[0].planDate),
                        accountDate = Convert.ToDateTime(data.rawTemplates[0].accountDate),
                    };
                    _context.Tasks.Add(task);
                    await _context.SaveChangesAsync();
                    var taskData = await _context.Tasks.FindAsync(task.taskID);

                    //craete check shop
                    var checkShop = new CheckShop
                    {
                        taskID = taskData.taskID,
                        shopStatus = "unchecked",
                    };
                    _context.CheckShops.Add(checkShop);
                    await _context.SaveChangesAsync();
                    var checkShopData = await _context.CheckShops.FindAsync(checkShop.checkShopId);

                    foreach (var raw in data.rawTemplates)
                    {
                        if (taskData.planDate != Convert.ToDateTime(raw.planDate))
                        {
                            var taskList = _context.Tasks.Where(c => c.shopId == shopData.shopId).ToList();
                            if (taskList.Count > 0)
                            {
                                var isInList = false;
                                foreach (var addedTask in taskList)
                                {
                                    if (addedTask.planDate == Convert.ToDateTime(raw.planDate))
                                    {
                                        isInList = true;
                                        taskData = addedTask;
                                    }
                                }
                                if (!isInList)
                                {
                                    var newTask = new AssetTask
                                    {
                                        shopId = shopData.shopId,
                                        periodId = periodData.periodId,
                                        taskName = shopData.shopName,
                                        planDate = Convert.ToDateTime(raw.planDate),
                                        accountDate = Convert.ToDateTime(raw.accountDate),
                                    };
                                    _context.Tasks.Add(newTask);
                                    await _context.SaveChangesAsync();
                                    taskData = await _context.Tasks.FindAsync(newTask.taskID);

                                    var newCheckShop = new CheckShop
                                    {
                                        taskID = taskData.taskID,
                                        shopStatus = "unchecked",
                                    };
                                    _context.CheckShops.Add(newCheckShop);
                                    await _context.SaveChangesAsync();
                                    checkShopData = await _context.CheckShops.FindAsync(newCheckShop.checkShopId);
                                }
                                else
                                {
                                    checkShopData = await _context.CheckShops.FindAsync(taskData.taskID);
                                    if (checkShopData == null)
                                    {
                                        var newCheckShop = new CheckShop
                                        {
                                            taskID = taskData.taskID,
                                            shopStatus = "unchecked",
                                        };
                                        _context.CheckShops.Add(newCheckShop);
                                        await _context.SaveChangesAsync();
                                        checkShopData = await _context.CheckShops.FindAsync(newCheckShop.checkShopId);
                                    }
                                }
                            }
                            else
                            {
                                var newTask = new AssetTask
                                {
                                    shopId = shopData.shopId,
                                    periodId = periodData.periodId,
                                    taskName = shopData.shopName,
                                    planDate = Convert.ToDateTime(raw.planDate),
                                    accountDate = Convert.ToDateTime(raw.accountDate),
                                };
                                _context.Tasks.Add(newTask);
                                await _context.SaveChangesAsync();
                                taskData = await _context.Tasks.FindAsync(newTask.taskID);

                                var newCheckShop = new CheckShop
                                {
                                    taskID = taskData.taskID,
                                    shopStatus = "unchecked",
                                };
                                _context.CheckShops.Add(newCheckShop);
                                await _context.SaveChangesAsync();
                                checkShopData = await _context.CheckShops.FindAsync(newCheckShop.checkShopId);
                            }
                        }

                        var company = _context.Companies.Where(c => raw.companyName.Contains(c.companyName)).FirstOrDefault();

                        if (company == null)
                        {
                            return await Task.FromResult(new Response
                            {
                                status = "error",
                                message = "ไม่พบข้อมูล " + raw.companyName
                            });
                        }

                        //add department
                        var depratment = new Department
                        {
                            departmentName = raw.department,
                            region = raw.region,
                            compCode = company.compCode,
                            area = raw.area,
                            center = raw.center,
                            channel = raw.channel,
                            province = raw.province,
                        };

                        var addedDepartment = _context.Departments.Where(c => c.departmentName == depratment.departmentName && c.channel == depratment.channel).ToList();
                        var departmentData = new Department();
                        if (addedDepartment.Count > 0)
                        {
                            departmentData = addedDepartment[0];
                        }
                        else
                        {
                            _context.Departments.Add(depratment);
                            await _context.SaveChangesAsync();
                            departmentData = await _context.Departments.FindAsync(depratment.departmentId);
                        }

                        //get employee
                        var employee = GetEmployeeData(raw);
                        if (employee == null)
                        {
                            if (raw.employeeId == null || raw.employeeId == "" || raw.employeeId == "null")
                            {
                                return await Task.FromResult(new Response
                                {
                                    status = "error",
                                    message = "ไม่พบรหัสพนักงานของคุณ" + raw.employeeName + " ในไฟล์ Template",
                                });
                            }
                            var empData = new Employee
                            {
                                empId = int.Parse(raw.employeeId),
                                empName = raw.employeeName,
                                region = raw.region,
                                compCode = company.compCode,
                                departmentId = departmentData.departmentId
                            };
                            _context.Employees.Add(empData);

                            if (!CheckShopKeeper(raw, shopData.shopId))
                            {
                                var shopKeeper = new Shopkeeper
                                {
                                    empId = empData.empId,
                                    shopId = shopData.shopId,
                                };
                                _context.Shopkeepers.Add(shopKeeper);
                            }
                        }
                        else
                        {
                            if (!CheckShopKeeper(raw, shopData.shopId))
                            {
                                var shopKeeper = new Shopkeeper
                                {
                                    empId = employee.empId,
                                    shopId = shopData.shopId,
                                };
                                _context.Shopkeepers.Add(shopKeeper);
                            }
                        }
                        await _context.SaveChangesAsync();

                        double price = 0;
                        if (raw.unitPrice == null || raw.unitPrice.Trim() == "-" || raw.unitPrice.Trim() == "" || raw.unitPrice.Trim() == "null")
                        {
                            price = 0;
                        }
                        else
                        {
                            price = double.Parse(raw.unitPrice);
                        }
                        //add asset
                        var asset = new Asset
                        {
                            assetCode = raw.assetCode,
                            assetName = raw.assetName,
                            assetType = raw.assetType,
                            accountType = raw.assetStatus,
                            region = raw.region,
                            compCode = departmentData.compCode,
                            departmentId = departmentData.departmentId,
                            unitCount = raw.unitCount,
                            price = price,
                            amount = int.Parse(raw.accountAmount),
                        };
                        _context.Assets.Add(asset);
                        await _context.SaveChangesAsync();
                        var assetData = await _context.Assets.FindAsync(asset.assetId);
                        var type = _context.AssetTypes.Where(c => c.typeName == assetData.assetType).First();

                        //create asset transaction
                        var transaction = new CheckShopTransaction
                        {
                            checkShopId = checkShopData.checkShopId,
                            assetId = assetData.assetId,
                            compCode = assetData.compCode,
                            assetTypeID = type.typeId,
                            accountBalance = int.Parse(raw.accountAmount),
                        };
                        _context.CheckShopTransactions.Add(transaction);
                        await _context.SaveChangesAsync();
                    }
                }
            }
            catch (Exception e)
            {
                _context.Database.RollbackTransaction();
                return await Task.FromResult(new Response
                {
                    status = "error",
                    message = e.Message
                });
            }

            _context.Database.CommitTransaction();
            return await Task.FromResult(new Response
            {
                status = "success",
                message = "สร้างแผนงานสำเร็จ"
            });
        }

        [HttpDelete("deleteTask/{taskId}")]
        public async Task<Response> deleteTask(int taskId)
        {
            try
            {
                var task = await _context.Tasks.FindAsync(taskId);
                var checkShop = _context.CheckShops.Where(c => c.taskID == task.taskID).First();
                var checkShopTrans = _context.CheckShopTransactions.Where(c => c.checkShopId == checkShop.checkShopId).ToList();

                var checkSigned = _context.CheckShopSigneds.Where(c => c.checkShopId == checkShop.checkShopId).ToList();
                if (checkSigned.Count > 0)
                {
                    return await Task.FromResult(new Response
                    {
                        status = "error",
                        message = "ไม่สามารถลบได้ เนื่องจากมีการเซ็นชื่อแล้ว"
                    });
                }

                _context.Database.BeginTransaction();

                if (checkShopTrans.Count > 0)
                {
                    List<Asset> assets = new();
                    foreach (var tran in checkShopTrans)
                    {
                        var transDetail = _context.CheckShopTransactionDetails.Where(c => c.checkShopTransactionId == tran.checkShopTransactionId).ToList();
                        if (transDetail.Count > 0)
                        {
                            _context.CheckShopTransactionDetails.RemoveRange(transDetail);
                        }

                        var asset = _context.Assets.Where(c => c.assetId == tran.assetId).First();
                        assets.Add(asset);
                    }

                    await _context.SaveChangesAsync();

                    _context.CheckShopTransactions.RemoveRange(checkShopTrans);
                    await _context.SaveChangesAsync();

                    _context.Assets.RemoveRange(assets);
                    await _context.SaveChangesAsync();
                }

                var checkDetail = _context.CheckShopDetails.Where(c => c.checkShopId == checkShop.checkShopId).ToList();
                if (checkDetail.Count > 0)
                {
                    _context.CheckShopDetails.RemoveRange(checkDetail);
                    await _context.SaveChangesAsync();
                }

                _context.CheckShops.Remove(checkShop);
                await _context.SaveChangesAsync();

                var shop = _context.Shops.Where(c => c.shopId == task.shopId).First();
                var shopKeeper = _context.Shopkeepers.Where(c => c.shopId == shop.shopId).ToList();
                if (shopKeeper.Count > 0)
                {
                    _context.Shopkeepers.RemoveRange(shopKeeper);
                    await _context.SaveChangesAsync();
                }

                _context.Tasks.Remove(task);
                await _context.SaveChangesAsync();

                //_context.Shops.Remove(shop);
                //await _context.SaveChangesAsync();

                _context.Database.CommitTransaction();
                return await Task.FromResult(new Response
                {
                    status = "success",
                    message = "ลบสำเร็จ"
                });
            }
            catch (Exception e)
            {
                _context.Database.RollbackTransaction();
                return await Task.FromResult(new Response
                {
                    status = "error",
                    message = e.Message
                });
            }
        }

        [HttpGet("getTaskTEST2/{periodId}")]
        public async Task<ActionResult<TaskResponse>> getTaskTEST2(int periodId)
        {
            try
            {
                var tasks = _context.Tasks.Where(c => c.periodId == periodId).ToList();
                List<TaskData> datas = new();

                foreach (var task in tasks)
                {
                    GetStatusReportTaskOnSummaryReq getStatusReportTaskOnSummaryReq = new GetStatusReportTaskOnSummaryReq();
                    getStatusReportTaskOnSummaryReq.taskID = task.taskID;
                    getStatusReportTaskOnSummaryReq.periodId = task.periodId;
                    GetStatusReportTaskOnSummaryResp getStatusReportTaskOnSummaryResp = _maiConfirm.GetStatusReportTaskOnSummary(getStatusReportTaskOnSummaryReq);
                    TaskData taskData = new();
                    taskData.taskID = task.taskID;
                    taskData.taskName = task.taskName;
                    taskData.shopId = task.shopId;
                    taskData.periodId = task.periodId;
                    taskData.planDate = task.planDate;
                    taskData.checkDate = task.checkDate;

                    List<ShopKeeperDetail> shopKeeperDetails = new();
                    var keepers = _context.Shopkeepers.Where(c => c.shopId == task.shopId).ToList();
                    foreach (var keeper in keepers)
                    {
                        var employee = _context.Employees.Where(c => c.empId == keeper.empId).First();
                        var department = _context.Departments.Where(c => c.departmentId == employee.departmentId).First();
                        var company = _context.Companies.Where(c => c.compCode == department.compCode).First();
                        var keeperDetail = new ShopKeeperDetail
                        {
                            empId = employee.empId,
                            empName = employee.empName,
                            department = department,
                            company = company,
                        };
                        shopKeeperDetails.Add(keeperDetail);
                    }

                    var checkShop = _context.CheckShops.Where(c => c.taskID == task.taskID).First();
                    var checkShopSigned = _context.CheckShopSigneds.Where(c => c.checkShopId == checkShop.checkShopId).ToList();

                    var shop = _context.Shops.Where(c => c.shopId == task.shopId).First();
                    taskData.checkShop = checkShop;
                    taskData.keeperDetail = shopKeeperDetails;
                    taskData.shop = shop;
                    taskData.checkShopSigneds = checkShopSigned;
                    taskData.sumTask = getStatusReportTaskOnSummaryResp.sumTask;
                    datas.Add(taskData);
                }

                return await Task.FromResult(new TaskResponse
                {
                    status = "success",
                    message = "",
                    tasks = datas,
                });
            }
            catch (Exception e)
            {
                return await Task.FromResult(new TaskResponse
                {
                    status = "error",
                    message = e.Message,
                });
            }
        }

        [HttpGet("getTask/{periodId}")]
        public async Task<ActionResult<TaskResponse>> getTask(int periodId)
        {
            try
            {
                var tasks = _context.Tasks.Where(c => c.periodId == periodId).ToList();
                List<TaskData> datas = new();

                GetStatusReportTaskOnSummaryReq getStatusReportTaskOnSummaryReq = new GetStatusReportTaskOnSummaryReq();
                //getStatusReportTaskOnSummaryReq.taskID = task.taskID;
                getStatusReportTaskOnSummaryReq.periodId = periodId;
                List<GetStatusReportTaskOnSummaryResp> getStatusReportTaskOnSummaryResp = _maiConfirm.GetStatusReportTaskOnSummaryALL(getStatusReportTaskOnSummaryReq);

                foreach (var task in tasks)
                {
                   
                    TaskData taskData = new();
                    taskData.taskID = task.taskID;
                    taskData.taskName = task.taskName;
                    taskData.shopId = task.shopId;
                    taskData.periodId = task.periodId;
                    taskData.planDate = task.planDate;
                    taskData.checkDate = task.checkDate;

                    List<ShopKeeperDetail> shopKeeperDetails = new();
                    var keepers = _context.Shopkeepers.Where(c => c.shopId == task.shopId).ToList();
                    foreach (var keeper in keepers)
                    {
                        var employee = _context.Employees.Where(c => c.empId == keeper.empId).First();
                        var department = _context.Departments.Where(c => c.departmentId == employee.departmentId).First();
                        var company = _context.Companies.Where(c => c.compCode == department.compCode).First();
                        var keeperDetail = new ShopKeeperDetail
                        {
                            empId = employee.empId,
                            empName = employee.empName,
                            department = department,
                            company = company,
                        };
                        shopKeeperDetails.Add(keeperDetail);
                    }

                    var checkShop = _context.CheckShops.Where(c => c.taskID == task.taskID).First();
                    var checkShopSigned = _context.CheckShopSigneds.Where(c => c.checkShopId == checkShop.checkShopId).ToList();

                    var shop = _context.Shops.Where(c => c.shopId == task.shopId).First();
                    taskData.checkShop = checkShop;
                    taskData.keeperDetail = shopKeeperDetails;
                    taskData.shop = shop;
                    taskData.checkShopSigneds = checkShopSigned;
                    taskData.sumTask = getStatusReportTaskOnSummaryResp.Where(w => w.taskID == task.taskID).Select(s => s.sumTask).FirstOrDefault();
                    datas.Add(taskData);
                }

                return await Task.FromResult(new TaskResponse
                {
                    status = "success",
                    message = "",
                    tasks = datas,
                });
            }
            catch (Exception e)
            {
                return await Task.FromResult(new TaskResponse
                {
                    status = "error",
                    message = e.Message,
                });
            }
        }

        [HttpGet("getTaskTEST3/{periodId}")]
        public async Task<ActionResult<TaskResponse>> getTaskTEST3(int periodId)
        {
            try
            {
                var tasks = _context.Tasks.Where(c => c.periodId == periodId).ToList();

                List<Shopkeeper> shopkeeperList = _context.Shopkeepers.ToList();
                List<Employee> employeeList = _context.Employees.ToList();
                List<Department> departmentList = _context.Departments.ToList();
                List<Company> companyList = _context.Companies.ToList();

                List<CheckShop> checkShopsList = _context.CheckShops.ToList();
                List<CheckShopSigned> checkShopSignedsList = _context.CheckShopSigneds.ToList();
                List<Shop> shopsList = _context.Shops.ToList();

                List<TaskData> datas = new();

                foreach (var task in tasks)
                {
                    GetStatusReportTaskOnSummaryReq getStatusReportTaskOnSummaryReq = new GetStatusReportTaskOnSummaryReq();
                    getStatusReportTaskOnSummaryReq.taskID = task.taskID;
                    getStatusReportTaskOnSummaryReq.periodId = task.periodId;
                    GetStatusReportTaskOnSummaryResp getStatusReportTaskOnSummaryResp = _maiConfirm.GetStatusReportTaskOnSummary(getStatusReportTaskOnSummaryReq);
                    TaskData taskData = new();
                    taskData.taskID = task.taskID;
                    taskData.taskName = task.taskName;
                    taskData.shopId = task.shopId;
                    taskData.periodId = task.periodId;
                    taskData.planDate = task.planDate;
                    taskData.checkDate = task.checkDate;

                    List<ShopKeeperDetail> shopKeeperDetails = new();
                    var keepers = shopkeeperList.Where(c => c.shopId == task.shopId).ToList();
                    foreach (var keeper in keepers)
                    {
                        var employee = employeeList.Where(c => c.empId == keeper.empId).First();
                        var department = departmentList.Where(c => c.departmentId == employee.departmentId).First();
                        var company = companyList.Where(c => c.compCode == department.compCode).First();
                        var keeperDetail = new ShopKeeperDetail
                        {
                            empId = employee.empId,
                            empName = employee.empName,
                            department = department,
                            company = company,
                        };
                        shopKeeperDetails.Add(keeperDetail);
                    }

                    var checkShop = checkShopsList.Where(c => c.taskID == task.taskID).First();
                    var checkShopSigned = checkShopSignedsList.Where(c => c.checkShopId == checkShop.checkShopId).ToList();

                    var shop = shopsList.Where(c => c.shopId == task.shopId).First();
                    taskData.checkShop = checkShop;
                    taskData.keeperDetail = shopKeeperDetails;
                    taskData.shop = shop;
                    taskData.checkShopSigneds = checkShopSigned;
                    taskData.sumTask = getStatusReportTaskOnSummaryResp.sumTask;
                    datas.Add(taskData);
                }

                return await Task.FromResult(new TaskResponse
                {
                    status = "success",
                    message = "",
                    tasks = datas,
                });
            }
            catch (Exception e)
            {
                return await Task.FromResult(new TaskResponse
                {
                    status = "error",
                    message = e.Message,
                });
            }
        }

        [HttpGet("getTaskDetail/{taskId}")]
        public async Task<ActionResult<TaskDetailResponse>> getTaskDetail(int taskId)
        {
            try
            {
                var task = _context.Tasks.Where(c => c.taskID == taskId).First();
                List<TaskData> datas = new();
                TaskData taskData = new();
                taskData.taskID = task.taskID;
                taskData.taskName = task.taskName;
                taskData.shopId = task.shopId;
                taskData.periodId = task.periodId;
                taskData.planDate = task.planDate;
                taskData.checkDate = task.checkDate;

                var checkShop = _context.CheckShops.Where(c => c.taskID == task.taskID).First();
                var shop = _context.Shops.Where(c => c.shopId == task.shopId).First();
                var checkTransaction = _context.CheckShopTransactions.Where(c => c.checkShopId == checkShop.checkShopId).ToList();

                List<String> compCodes = new();
                foreach (var trans in checkTransaction)
                {
                    if (!compCodes.Contains(trans.compCode))
                    {
                        compCodes.Add(trans.compCode);
                    }
                }

                List<ShopKeeperDetail> shopKeeperDetails = new();
                List<ShopKeeperDetail> newKeeperDetails = new();
                List<ShopKeeperDetail> oldKeeperDetails = new();

                var checkShopKeepers = _context.CheckShopKeepers.Where(c => c.checkShopId == checkShop.checkShopId && c.isActive == true).ToList();
                foreach (var checkShopKeeper in checkShopKeepers)
                {
                    var employee = _context.Employees.Where(c => c.empId == int.Parse(checkShopKeeper.empId)).First();
                    var department = _context.Departments.Where(c => c.departmentId == employee.departmentId).First();
                    var company = _context.Companies.Where(c => c.compCode == department.compCode).First();
                    var keeperDetail = new ShopKeeperDetail
                    {
                        empId = employee.empId,
                        empName = employee.empName,
                        department = department,
                        company = company,
                        isNew = true,
                    };
                    newKeeperDetails.Add(keeperDetail);
                }

                var keepers = _context.Shopkeepers.Where(c => c.shopId == task.shopId).Distinct().ToList();
                foreach (var keeper in keepers)
                {
                    var employee = _context.Employees.Where(c => c.empId == keeper.empId).First();
                    var department = _context.Departments.Where(c => c.departmentId == employee.departmentId).First();
                    var company = _context.Companies.Where(c => c.compCode == department.compCode).First();
                    var shopKeeper = _context.Shopkeepers.Where(c => c.shopId == task.shopId && c.empId == employee.empId).First();

                    if (compCodes.Contains(company.compCode))
                    {
                        if (!isKeeperInList(newKeeperDetails, company.compCode) && !isKeeperInList(shopKeeperDetails, company.compCode))
                        {
                            var keeperDetail = new ShopKeeperDetail
                            {
                                empId = employee.empId,
                                empName = employee.empName,
                                department = department,
                                company = company,
                                isNew = shopKeeper.isNew,
                            };
                            shopKeeperDetails.Add(keeperDetail);
                        }
                    }

                    if (!shopKeeper.isNew)
                    {
                        var keeperDetail = new ShopKeeperDetail
                        {
                            empId = employee.empId,
                            empName = employee.empName,
                            department = department,
                            company = company,
                            isNew = shopKeeper.isNew,
                        };
                        oldKeeperDetails.Add(keeperDetail);
                    }
                }


                shopKeeperDetails.AddRange(newKeeperDetails);
                taskData.checkShop = checkShop;
                taskData.keeperDetail = shopKeeperDetails;
                taskData.shop = shop;
                taskData.oldKeeperDetails = oldKeeperDetails;

                return await Task.FromResult(new TaskDetailResponse
                {
                    status = "success",
                    message = "",
                    task = taskData,
                });
            }
            catch (Exception e)
            {
                return await Task.FromResult(new TaskDetailResponse
                {
                    status = "error",
                    message = e.Message,
                });
            }
        }

        private bool isKeeperInList(List<ShopKeeperDetail> newKeeperDetails, String compCode)
        {
            foreach (var keeper in newKeeperDetails)
            {
                if (keeper.company.compCode == compCode)
                {
                    return true;
                }
            }
            return false;
        }

        [HttpPost("uploadShopImage/{checkShopId}")]
        public async Task<Response> uploadShopImage(int checkShopId, [FromForm(Name = "imageFile")] IFormFile imageFile)
        {
            try
            {
                string[] TypeExtension = { ".jpg"};

                if (!(TypeExtension.Contains(System.IO.Path.GetExtension(imageFile.FileName))))
                    throw new Exception("Only jpg files can be uploaded.");

                using (var fileContentStream = new MemoryStream())
                {
                    await imageFile.CopyToAsync(fileContentStream);
                    await System.IO.File.WriteAllBytesAsync(Path.Combine(shopImagePath, imageFile.FileName), fileContentStream.ToArray());
                }

                var checkShop = _context.CheckShops.Where(c => c.checkShopId == checkShopId).First();
                checkShop.shopImage = shopImagePath + "\\" + imageFile.FileName;

                _context.CheckShops.Update(checkShop);
                await _context.SaveChangesAsync();

                return await Task.FromResult(new Response
                {
                    status = "success",
                    message = "บันทึกสำเร็จ",
                });
            }
            catch (Exception e)
            {
                return await Task.FromResult(new Response
                {
                    status = "error",
                    message = e.Message,
                });
            }
        }

        [HttpGet("getShopImage/{checkShopId}")]
        public IActionResult getShopImage(int checkShopId)
        {
            try
            {
                var checkShop = _context.CheckShops.Where(c => c.checkShopId == checkShopId).First();
                var image = System.IO.File.OpenRead(checkShop.shopImage);
                return File(image, "image/jpeg");
            }
            catch
            {
                return NotFound();
            }
        }

        [HttpPost("checkShop")]
        public async Task<Response> checkShop(CheckShop checkShop)
        {
            try
            {
                var task = _context.Tasks.Where(c => c.taskID == checkShop.taskID).First();
                task.checkDate = DateTime.Now;

                _context.CheckShops.Update(checkShop);
                _context.Tasks.Update(task);

                await _context.SaveChangesAsync();

                return await Task.FromResult(new Response
                {
                    status = "success",
                    message = "บันทึกข้อมูลสำเร็จ",
                });
            }
            catch (Exception e)
            {
                return await Task.FromResult(new Response
                {
                    status = "error",
                    message = e.Message,
                });
            }
        }

        [HttpGet("getCheckShopKeepers/{checkShopId}")]
        public async Task<CheckShopKeepersResponse> getCheckShopKeepers(int checkShopId)
        {
            try
            {
                var checkShop = _context.CheckShopKeepers.Where(c => c.checkShopId == checkShopId && c.isActive == true).ToList();
                return await Task.FromResult(new CheckShopKeepersResponse
                {
                    status = "success",
                    message = "",
                    checkShopKeeper = checkShop,
                });
            }
            catch (Exception e)
            {
                return await Task.FromResult(new CheckShopKeepersResponse
                {
                    status = "error",
                    message = e.Message,

                });
            }
        }

        [HttpPost("addShopKeepers/")]
        public async Task<Response> addShopkeepers(CheckShopKeeperRequest keeperRequest)
        {
            try
            {
                var checkShop = _context.CheckShopKeepers.Where(c => c.checkShopId == keeperRequest.checkShopId).ToList();
                foreach (var KeeperDetail in keeperRequest.keeperDetails)
                {
                    if (KeeperDetail.isNew)
                    {
                        var chekShopData = _context.CheckShops.Where(c => c.checkShopId == keeperRequest.checkShopId).First();
                        var task = _context.Tasks.Where(c => c.taskID == chekShopData.taskID).First();
                        var shopKeeperData = _context.Shopkeepers.Where(c => c.shopId == task.shopId && c.empId == int.Parse(KeeperDetail.empId)).ToList();

                        if (KeeperDetail.isChange)
                        {
                            var oldKeeper = _context.Shopkeepers.Where(c => c.shopId == task.shopId && c.empId == int.Parse(KeeperDetail.oldEmpId)).First();
                            oldKeeper.empId = int.Parse(KeeperDetail.empId);
                            _context.Shopkeepers.Update(oldKeeper);
                        }
                        else
                        {
                            if (shopKeeperData.Count == 0)
                            {
                                var shopKeeper = new Shopkeeper
                                {
                                    empId = int.Parse(KeeperDetail.empId),
                                    shopId = task.shopId,
                                    isNew = true
                                };
                                _context.Shopkeepers.Add(shopKeeper);
                            }
                        }
                    }

                    var checkShopKeeper = new CheckShopKeeper
                    {
                        checkShopId = keeperRequest.checkShopId,
                        compCode = KeeperDetail.compCode,
                        empId = KeeperDetail.empId,
                        empName = KeeperDetail.empName,
                        isActive = true,
                    };

                    if (KeeperDetail.checkShopKeeperId != 0)
                    {
                        var keepers = _context.CheckShopKeepers.Where(c => c.id == KeeperDetail.checkShopKeeperId).ToList();
                        var oldKeeper = keepers[0];
                        oldKeeper.isActive = false;
                        _context.CheckShopKeepers.Update(oldKeeper);
                        _context.CheckShopKeepers.Add(checkShopKeeper);
                    }
                    else
                    {
                        _context.CheckShopKeepers.Add(checkShopKeeper);
                    }
                }

                await _context.SaveChangesAsync();

                return await Task.FromResult(new Response
                {
                    status = "success",
                    message = "บันทึกสำเร็จ",
                });
            }
            catch (Exception e)
            {
                return await Task.FromResult(new Response
                {
                    status = "error",
                    message = e.Message,
                });
            }
        }

        [HttpGet("getCompanies/{region}")]
        public async Task<CompaniesResponse> getCompanies(int region)
        {
            try
            {
                var companyList = _context.Companies.Where(c => c.region == region).ToList();
                return await Task.FromResult(new CompaniesResponse
                {
                    status = "success",
                    message = "",
                    companies = companyList,
                });
            }
            catch (Exception e)
            {
                return await Task.FromResult(new CompaniesResponse
                {
                    status = "error",
                    message = e.Message,

                });
            }
        }

        [HttpPost("uploadSignature/{empID}")]
        public async Task<Response> uploadSignature(int empID, [FromForm(Name = "imageFile")] IFormFile imageFile)
        {
            try
            {
                string[] TypeExtension =  { ".jpg",".png"};

                if (!(TypeExtension.Contains(System.IO.Path.GetExtension(imageFile.FileName))))
                    throw new Exception("Only jpg or png files can be uploaded.");

                using (var fileContentStream = new MemoryStream())
                {
                    await imageFile.CopyToAsync(fileContentStream);
                    await System.IO.File.WriteAllBytesAsync(Path.Combine(signaturePath, imageFile.FileName), fileContentStream.ToArray());
                }

                var employee = _context.Employees.Where(c => c.empId == empID).First();
                employee.signature = signaturePath + "\\" + imageFile.FileName;

                _context.Employees.Update(employee);
                await _context.SaveChangesAsync();

                return await Task.FromResult(new Response
                {
                    status = "success",
                    message = "บันทึกสำเร็จ",
                });
            }
            catch (Exception e)
            {
                return await Task.FromResult(new Response
                {
                    status = "error",
                    message = e.Message,
                });
            }
        }

        [HttpGet("getSignature/{empID}")]
        public IActionResult getSignature(int empID)
        {
            try
            {
                var employee = _context.Employees.Where(c => c.empId == empID).First();
                var image = System.IO.File.OpenRead(employee.signature);
                return File(image, "image/jpeg");
            }
            catch
            {
                return NotFound();
            }
        }


        [HttpGet("getEmployee/{compCode}/{region}")]
        public async Task<EmployeeResponse> getEmployee(String compCode, String region)
        {
            try
            {
                var employees = _context.Employees.Where(c => c.compCode == compCode && c.departmentId != null && c.region == region).ToList();
                return await Task.FromResult(new EmployeeResponse
                {
                    status = "success",
                    message = "",
                    employees = employees,
                });
            }
            catch (Exception e)
            {
                return await Task.FromResult(new EmployeeResponse
                {
                    status = "error",
                    message = e.Message,

                });
            }
        }

        [HttpPost("addEmployee/")]
        public async Task<Response> addEmployee(Employee employee)
        {
            try
            {
                var employees = _context.Employees.Where(c => c.empId == employee.empId).ToList();

                if (employees.Count > 0)
                {
                    return await Task.FromResult(new Response
                    {
                        status = "error",
                        message = "รหัสพนักงานนี้มีในระบบอยู่แล้ว",
                    });
                }
                else
                {
                    _context.Employees.Add(employee);
                    await _context.SaveChangesAsync();

                    return await Task.FromResult(new Response
                    {
                        status = "success",
                        message = "บันทึกสำเร็จ",
                    });
                }
            }
            catch (Exception e)
            {
                return await Task.FromResult(new Response
                {
                    status = "error",
                    message = e.Message,
                });
            }
        }

        [HttpGet("getCurrentShopKeeper/{checkShopId}")]
        public async Task<CurrentShopKeepersResponse> getCurrentShopKeeper(int checkShopId)
        {
            try
            {
                var checkShopKeepers = _context.CheckShopKeepers.Where(c => c.checkShopId == checkShopId).ToList();
                return await Task.FromResult(new CurrentShopKeepersResponse
                {
                    status = "success",
                    message = "",
                    keeperDetails = checkShopKeepers,
                });
            }
            catch (Exception e)
            {
                return await Task.FromResult(new CurrentShopKeepersResponse
                {
                    status = "error",
                    message = e.Message,

                });
            }
        }

        [HttpGet("getCheckAsset/{checkShopId}/{compCode}/{type}")]
        public async Task<CheckAssetResponse> getCheckAsset(int checkShopId, String compCode, String type)
        {
            try
            {
                List<CheckShopTransaction> checkTransaction;
                if (type == "beer")
                {
                    checkTransaction = _context.CheckShopTransactions.Where(c => c.checkShopId == checkShopId && c.compCode == compCode && c.assetTypeID == 3).ToList();
                }
                else if (type == "pop-pos")
                {
                    checkTransaction = _context.CheckShopTransactions.Where(c => c.checkShopId == checkShopId && c.compCode == compCode && (c.assetTypeID == 2 || c.assetTypeID == 1)).ToList();
                }
                else if (type == "mac-pos")
                {
                    checkTransaction = _context.CheckShopTransactions.Where(c => c.checkShopId == checkShopId && c.compCode == compCode && c.assetTypeID == 4).ToList();
                }
                else
                {
                    checkTransaction = _context.CheckShopTransactions.Where(c => c.checkShopId == checkShopId && c.compCode == compCode).ToList();
                }
                var checkAssetList = new List<CheckAsset>();
                foreach (var transaction in checkTransaction)
                {
                    var assets = _context.Assets.Where(c => c.assetId == transaction.assetId).ToList();
                    if (assets.Count > 0)
                    {
                        var asset = assets[0];
                        checkAssetList.Add(new CheckAsset
                        {
                            checkShopTransactionId = transaction.checkShopTransactionId,
                            checkShopId = transaction.checkShopId,
                            assetId = transaction.assetId,
                            assetTypeID = transaction.assetTypeID,
                            assetType = asset.assetType,
                            assetCode = asset.assetCode,
                            assetName = asset.assetName,
                            accountType = asset.accountType,
                            status = asset.status,
                            region = asset.region,
                            compCode = asset.compCode,
                            departmentId = asset.departmentId,
                            unitCount = asset.unitCount,
                            price = asset.price,
                            amount = asset.amount,
                            accountBalance = transaction.accountBalance,
                            avaliable = transaction.avaliable,
                            damaged = transaction.damaged,
                            isNew = asset.isNew
                        });
                    }
                }

                return await Task.FromResult(new CheckAssetResponse
                {
                    status = "success",
                    message = "",
                    checkAssets = checkAssetList,
                });
            }
            catch (Exception e)
            {
                return await Task.FromResult(new CheckAssetResponse
                {
                    status = "error",
                    message = e.Message,

                });
            }
        }

        [HttpPost("uploadCheckAssetImage/{transactionDetailId}")]
        public async Task<Response> uploadCheckAssetImage(int transactionDetailId, [FromForm(Name = "imageFile")] IFormFile imageFile)
        {
            try
            {
                string[] TypeExtension = { ".jpg", ".png" };

                if (!(TypeExtension.Contains(System.IO.Path.GetExtension(imageFile.FileName))))
                    throw new Exception("Only jpg or png files can be uploaded.");

                using (var fileContentStream = new MemoryStream())
                {
                    await imageFile.CopyToAsync(fileContentStream);
                    await System.IO.File.WriteAllBytesAsync(Path.Combine(checkAssetParh, imageFile.FileName), fileContentStream.ToArray());
                }

                var transactionDetail = _context.CheckShopTransactionDetails.Where(c => c.id == transactionDetailId).First();
                transactionDetail.attachImage = checkAssetParh + "\\" + imageFile.FileName;

                _context.CheckShopTransactionDetails.Update(transactionDetail);
                await _context.SaveChangesAsync();

                return await Task.FromResult(new Response
                {
                    status = "success",
                    message = "บันทึกสำเร็จ",
                });
            }
            catch (Exception e)
            {
                return await Task.FromResult(new Response
                {
                    status = "error",
                    message = e.Message,
                });
            }
        }

        [HttpGet("getCheckAssetImage/{transactionDetailId}")]
        public IActionResult getCheckAssetImage(int transactionDetailId)
        {
            try
            {
                var transactionDetail = _context.CheckShopTransactionDetails.Where(c => c.id == transactionDetailId).First();
                var image = System.IO.File.OpenRead(transactionDetail.attachImage);
                return File(image, "image/jpeg");
            }
            catch
            {
                return NotFound();
            }
        }

        [HttpGet("getCheckTransactionDetail/{transactionId}")]
        public async Task<CheckTransactionResponse> getCheckTransactionDetail(int transactionId)
        {
            try
            {
                var transactioDetail = _context.CheckShopTransactionDetails.Where(c => c.checkShopTransactionId == transactionId).ToList();
                return await Task.FromResult(new CheckTransactionResponse
                {
                    status = "success",
                    message = "",
                    checkShopTransactionDetails = transactioDetail,
                });
            }
            catch (Exception e)
            {
                return await Task.FromResult(new CheckTransactionResponse
                {
                    status = "error",
                    message = e.Message,

                });
            }
        }

        [HttpPost("addTransactionDetail/")]
        public async Task<Response> addTransactionDetail(CheckShopTransactionDetail transactionDetail)
        {
            try
            {
                var checkTransaction = _context.CheckShopTransactions.Where(c => c.checkShopTransactionId == transactionDetail.checkShopTransactionId).First();
                if (transactionDetail.assetStatus == "available")
                {
                    checkTransaction.avaliable += transactionDetail.amount;
                }
                else
                {
                    checkTransaction.damaged += transactionDetail.amount;
                }

                _context.CheckShopTransactions.Update(checkTransaction);
                _context.CheckShopTransactionDetails.Add(transactionDetail);

                await _context.SaveChangesAsync();

                return await Task.FromResult(new Response
                {
                    status = "success",
                    message = "บันทึกสำเร็จ",
                });
            }
            catch (Exception e)
            {
                return await Task.FromResult(new Response
                {
                    status = "error",
                    message = e.Message,
                });
            }
        }

        [HttpDelete("deleteTransactionDetail/{detailId}")]
        public async Task<Response> deleteTransactionDetail(int detailId)
        {
            var transactionDetail = await _context.CheckShopTransactionDetails.FindAsync(detailId);
            if (transactionDetail == null)
            {
                return await Task.FromResult(new Response
                {
                    status = "error",
                    message = "ไม่พบข้อมูลที่ต้องการลบ",
                });
            }

            var checkTransaction = _context.CheckShopTransactions.Where(c => c.checkShopTransactionId == transactionDetail.checkShopTransactionId).FirstOrDefault();
            if (transactionDetail.assetStatus == "available")
            {
                checkTransaction.avaliable -= transactionDetail.amount;
            }
            else
            {
                checkTransaction.damaged -= transactionDetail.amount;
            }

            _context.CheckShopTransactions.Update(checkTransaction);
            _context.CheckShopTransactionDetails.Remove(transactionDetail);
            await _context.SaveChangesAsync();

            return await Task.FromResult(new Response
            {
                status = "success",
                message = "ลบสำเร็จ",
            });
        }

        [HttpDelete("removeShopKeeper/{checkShopId}/{empId}")]
        public async Task<Response> removeShopKeeper(int checkShopId, int empId)
        {
            var checkShopKeeper = _context.CheckShopKeepers.Where(c => c.checkShopId == checkShopId && c.empId == empId.ToString()).ToList();
            if (checkShopKeeper.Count > 0)
            {
                _context.CheckShopKeepers.RemoveRange(checkShopKeeper);
            }
            await _context.SaveChangesAsync();

            var checkShop = await _context.CheckShops.FindAsync(checkShopId);
            var task = await _context.Tasks.FindAsync(checkShop.taskID);
            var shop = await _context.Shops.FindAsync(task.shopId);
            var shopKeeper = _context.Shopkeepers.Where(c => c.shopId == shop.shopId && c.empId == empId && c.isNew == true).ToList();
            if (shopKeeper.Count > 0)
            {
                _context.Shopkeepers.RemoveRange(shopKeeper);
            }

            await _context.SaveChangesAsync();

            return await Task.FromResult(new Response
            {
                status = "success",
                message = "ลบสำเร็จ",
            });
        }

        [HttpDelete("deleteAsset/{checkShopId}/{assetId}")]
        public async Task<Response> deleteAsset(int checkShopId, int assetId)
        {
            var checkShopTransaction = _context.CheckShopTransactions.Where(c => c.checkShopId == checkShopId && c.assetId == assetId).ToList();
            if (checkShopTransaction.Count > 0)
            {
                foreach (var detail in checkShopTransaction)
                {
                    var checkDetail = _context.CheckShopTransactionDetails.Where(c => c.checkShopTransactionId == detail.checkShopTransactionId).ToList();
                    if (checkDetail.Count > 0)
                    {
                        _context.CheckShopTransactionDetails.RemoveRange(checkDetail);
                    }
                }
                await _context.SaveChangesAsync();

                _context.CheckShopTransactions.RemoveRange(checkShopTransaction);
                await _context.SaveChangesAsync();
            }

            var asset = _context.Assets.Where(c => c.assetId == assetId).ToList();
            if (asset.Count > 0)
            {
                _context.Assets.RemoveRange(asset);
            }
            else
            {
                return await Task.FromResult(new Response
                {
                    status = "error",
                    message = "ไม่พบข้อมูลที่ต้องการลบ",
                });
            }
            await _context.SaveChangesAsync();

            return await Task.FromResult(new Response
            {
                status = "success",
                message = "ลบสำเร็จ",
            });
        }

        [HttpPut("updateRemark/{detailId}")]
        public async Task<Response> updateRemark(int detailId, String remark)
        {
            var transactionDetail = await _context.CheckShopTransactionDetails.FindAsync(detailId);
            if (transactionDetail == null)
            {
                return await Task.FromResult(new Response
                {
                    status = "error",
                    message = "ไม่พบข้อมูล",
                });
            }

            transactionDetail.remark = remark;
            _context.CheckShopTransactionDetails.Update(transactionDetail);
            await _context.SaveChangesAsync();

            return await Task.FromResult(new Response
            {
                status = "success",
                message = "บันทึกสำเร็จ",
            });
        }

        [HttpPost("addCheckShopDetail/")]
        public async Task<Response> addCheckShopDetail(CheckShopDetail shopDetail)
        {
            try
            {
                _context.CheckShopDetails.Add(shopDetail);

                await _context.SaveChangesAsync();

                return await Task.FromResult(new Response
                {
                    status = "success",
                    message = "บันทึกสำเร็จ",
                });
            }
            catch (Exception e)
            {
                return await Task.FromResult(new Response
                {
                    status = "error",
                    message = e.Message,
                });
            }
        }

        [HttpPut("updateCheckShopDetail/")]
        public async Task<Response> updateCheckShopDetail(CheckShopDetail shopDetail)
        {
            _context.CheckShopDetails.Update(shopDetail);
            await _context.SaveChangesAsync();

            return await Task.FromResult(new Response
            {
                status = "success",
                message = "บันทึกสำเร็จ",
            });
        }

        [HttpPost("addCheckShopSigned/")]
        public async Task<ResponseWithId> addCheckShopSigned(CheckShopSigned shopSigned)
        {
            try
            {
                var data = new CheckShopSigned
                {
                    checkShopId = shopSigned.checkShopId,
                    assetTypeID = shopSigned.assetTypeID,
                    compCode = shopSigned.compCode,
                    confirmStatus = shopSigned.confirmStatus,
                    customerSigned = shopSigned.customerSigned,
                    employeeSigned = shopSigned.employeeSigned,
                    remark = shopSigned.remark,
                    checkerId = shopSigned.checkerId,
                };
                _context.CheckShopSigneds.Add(data);
                await _context.SaveChangesAsync();

                var addedData = await _context.CheckShopSigneds.FindAsync(data.id);

                return await Task.FromResult(new ResponseWithId
                {
                    status = "success",
                    message = "บันทึกสำเร็จ",
                    id = addedData.id.ToString(),
                });
            }
            catch (Exception e)
            {
                return await Task.FromResult(new ResponseWithId
                {
                    status = "error",
                    message = e.Message,
                });
            }
        }

        [HttpPost("uploadCustomerSigned/{shopSignedId}")]
        public async Task<Response> uploadCustomerSigned(int shopSignedId, [FromForm(Name = "imageFile")] IFormFile imageFile)
        {
            try
            {
                string[] TypeExtension = { ".jpg", ".png" };

                if (!(TypeExtension.Contains(System.IO.Path.GetExtension(imageFile.FileName))))
                    throw new Exception("Only jpg or png files can be uploaded.");

                using (var fileContentStream = new MemoryStream())
                {
                    await imageFile.CopyToAsync(fileContentStream);
                    await System.IO.File.WriteAllBytesAsync(Path.Combine(signaturePath, imageFile.FileName), fileContentStream.ToArray());
                }

                var checkShopSigned = _context.CheckShopSigneds.Where(c => c.id == shopSignedId).First();
                checkShopSigned.customerSigned = signaturePath + "\\" + imageFile.FileName;

                _context.CheckShopSigneds.Update(checkShopSigned);
                await _context.SaveChangesAsync();

                return await Task.FromResult(new Response
                {
                    status = "success",
                    message = "บันทึกสำเร็จ",
                });
            }
            catch (Exception e)
            {
                return await Task.FromResult(new Response
                {
                    status = "error",
                    message = e.Message,
                });
            }
        }

        [HttpPost("uploadEmployeeSigned/{shopSignedId}")]
        public async Task<Response> uploadEmployeeSigned(int shopSignedId, [FromForm(Name = "imageFile")] IFormFile imageFile)
        {
            try
            {
                string[] TypeExtension = { ".jpg", ".png" };

                string trttr = System.IO.Path.GetFileNameWithoutExtension(imageFile.FileName);
                if (!(TypeExtension.Contains(System.IO.Path.GetExtension(imageFile.FileName))))
                    throw new Exception("Only jpg or png files can be uploaded.");

                using (var fileContentStream = new MemoryStream())
                {
                    await imageFile.CopyToAsync(fileContentStream);
                    await System.IO.File.WriteAllBytesAsync(Path.Combine(signaturePath, imageFile.FileName), fileContentStream.ToArray());
                }

                var checkShopSigned = _context.CheckShopSigneds.Where(c => c.id == shopSignedId).First();
                checkShopSigned.employeeSigned = signaturePath + "\\" + imageFile.FileName;

                _context.CheckShopSigneds.Update(checkShopSigned);
                await _context.SaveChangesAsync();

                return await Task.FromResult(new Response
                {
                    status = "success",
                    message = "บันทึกสำเร็จ",
                });
            }
            catch (Exception e)
            {
                return await Task.FromResult(new Response
                {
                    status = "error",
                    message = e.Message,
                });
            }
        }

        [HttpGet("getEmployeeSignature/{shopSignedId}")]
        public IActionResult getEmployeeSignature(int shopSignedId)
        {
            try
            {
                var checkShopSigned = _context.CheckShopSigneds.Where(c => c.id == shopSignedId).First();
                var image = System.IO.File.OpenRead(checkShopSigned.employeeSigned);
                return File(image, "image/jpeg");
            }
            catch
            {
                return NotFound();
            }
        }

        [HttpGet("getEmployeeSignatureById/{empid}")]
        public IActionResult getEmployeeSignatureById(int empid)
        {
            try
            {
                var checkShopSigned = _context.Employees.Where(c => c.empId == empid).First();
                var image = System.IO.File.OpenRead(checkShopSigned.signature);
                return File(image, "image/jpeg");
            }
            catch
            {
                return NotFound();
            }
        }

        [HttpGet("getCustomerSignature/{shopSignedId}")]
        public IActionResult getCustomerSignature(int shopSignedId)
        {
            try
            {
                var checkShopSigned = _context.CheckShopSigneds.Where(c => c.id == shopSignedId).First();
                var image = System.IO.File.OpenRead(checkShopSigned.customerSigned);
                return File(image, "image/jpeg");
            }
            catch
            {
                return NotFound();
            }
        }

        [HttpGet("getCheckShopDetail/{checkShopId}")]
        public async Task<CheckShopDetailResponse> getCheckShopDetail(String checkShopId)
        {
            try
            {
                var checkShopDetail = _context.CheckShopDetails.Where(c => c.checkShopId == int.Parse(checkShopId)).First();

                return await Task.FromResult(new CheckShopDetailResponse
                {
                    status = "success",
                    message = "บันทึกสำเร็จ",
                    checkShopDetail = checkShopDetail,
                });
            }
            catch (Exception e)
            {
                return await Task.FromResult(new CheckShopDetailResponse
                {
                    status = "error",
                    message = e.Message,
                });
            }
        }

        [HttpGet("getCheckShopSigneds/{checkShopId}")]
        public async Task<CheckShopSignedResponse> getCheckShopSigneds(String checkShopId)
        {
            try
            {
                var checkShopSigneds = _context.CheckShopSigneds.Where(c => c.checkShopId == int.Parse(checkShopId)).ToList();

                return await Task.FromResult(new CheckShopSignedResponse
                {
                    status = "success",
                    message = "บันทึกสำเร็จ",
                    checkShopSigneds = checkShopSigneds,
                });
            }
            catch (Exception e)
            {
                return await Task.FromResult(new CheckShopSignedResponse
                {
                    status = "error",
                    message = e.Message,
                });
            }
        }

        [HttpGet("getCheckShopDetailWithShopKeeper/{checkShopId}/{compCode}")]
        public async Task<CheckShopDetailWithKeeperResponse> getCheckShopDetailWithShopKeeper(String checkShopId, String compCode)
        {
            try
            {
                var checkShopDetail = _context.CheckShopDetails.Where(c => c.checkShopId == int.Parse(checkShopId)).First();
                var checkShop = _context.CheckShops.Where(c => c.checkShopId == int.Parse(checkShopId)).First();
                var task = _context.Tasks.Where(c => c.taskID == checkShop.taskID).First();
                var shop = _context.Shops.Where(c => c.shopId == task.shopId).First();
                var shopKeeper = _context.Shopkeepers.Where(c => c.shopId == shop.shopId).ToList();

                Employee employee = null;

                var currentKeeper = _context.CheckShopKeepers.Where(c => c.compCode == compCode && c.isActive == true && c.checkShopId == int.Parse(checkShopId)).ToList();
                if (currentKeeper.Count > 0)
                {
                    var emp = _context.Employees.Where(c => c.empId == int.Parse(currentKeeper[0].empId)).First();
                    employee = emp;
                }
                else
                {
                    foreach (var keeper in shopKeeper)
                    {
                        var emp = _context.Employees.Where(c => c.empId == keeper.empId).First();
                        if (emp.compCode == compCode)
                        {
                            employee = emp;
                            break;
                        }
                    }
                }

                if (employee == null)
                {
                    return await Task.FromResult(new CheckShopDetailWithKeeperResponse
                    {
                        status = "error",
                        message = "ไม่พบข้อมูลผู้ดูแล หรือข้อมลูผู้ดูแลไม่ตรงกับบริษัทที่สังกัด",
                    });
                }
                else
                {
                    return await Task.FromResult(new CheckShopDetailWithKeeperResponse
                    {
                        status = "success",
                        message = "บันทึกสำเร็จ",
                        checkShopDetail = checkShopDetail,
                        employee = employee,
                    });
                }
            }
            catch (Exception e)
            {
                return await Task.FromResult(new CheckShopDetailWithKeeperResponse
                {
                    status = "error",
                    message = e.Message,
                });
            }
        }

        [HttpGet("getAssetWithSignature/{checkShopId}/{compCode}/{type}/{signedId}")]
        public async Task<CheckShopSignedWithDetailResponse> getAssetWithSignature(int checkShopId, String compCode, String type, int signedId)
        {
            try
            {
                List<CheckShopTransaction> checkTransaction;
                if (type == "beer")
                {
                    checkTransaction = _context.CheckShopTransactions.Where(c => c.checkShopId == checkShopId && c.compCode == compCode && c.assetTypeID == 3).ToList();
                }
                else if (type == "pop-pos")
                {
                    checkTransaction = _context.CheckShopTransactions.Where(c => c.checkShopId == checkShopId && c.compCode == compCode && (c.assetTypeID == 2 || c.assetTypeID == 1)).ToList();
                }
                else if (type == "mac-pos")
                {
                    checkTransaction = _context.CheckShopTransactions.Where(c => c.checkShopId == checkShopId && c.compCode == compCode && c.assetTypeID == 4).ToList();
                }
                else
                {
                    checkTransaction = _context.CheckShopTransactions.Where(c => c.checkShopId == checkShopId && c.compCode == compCode).ToList();
                }
                var checkAssetList = new List<CheckAsset>();
                foreach (var transaction in checkTransaction)
                {
                    var asset = _context.Assets.Where(c => c.assetId == transaction.assetId).First();
                    checkAssetList.Add(new CheckAsset
                    {
                        checkShopTransactionId = transaction.checkShopTransactionId,
                        checkShopId = transaction.checkShopId,
                        assetId = transaction.assetId,
                        assetTypeID = transaction.assetTypeID,
                        assetType = asset.assetType,
                        assetCode = asset.assetCode,
                        assetName = asset.assetName,
                        accountType = asset.accountType,
                        status = asset.status,
                        region = asset.region,
                        compCode = asset.compCode,
                        departmentId = asset.departmentId,
                        unitCount = asset.unitCount,
                        price = asset.price,
                        amount = asset.amount,
                        accountBalance = transaction.accountBalance,
                        avaliable = transaction.avaliable,
                        damaged = transaction.damaged,
                    });
                }

                var checkShopDetail = _context.CheckShopDetails.Where(c => c.checkShopId == checkShopId).First();
                var checkShop = _context.CheckShops.Where(c => c.checkShopId == checkShopId).First();
                var task = _context.Tasks.Where(c => c.taskID == checkShop.taskID).First();
                var shop = _context.Shops.Where(c => c.shopId == task.shopId).First();
                var shopKeeper = _context.Shopkeepers.Where(c => c.shopId == shop.shopId).ToList();
              

                Employee employee = null;
                var currentKeeper = _context.CheckShopKeepers.Where(c => c.compCode == compCode && c.isActive == true && c.checkShopId == checkShopId).ToList();
                if (currentKeeper.Count > 0)
                {
                    var emp = _context.Employees.Where(c => c.empId == int.Parse(currentKeeper[0].empId)).First();
                    employee = emp;
                }
                else
                {
                    foreach (var keeper in shopKeeper)
                    {
                        var emp = _context.Employees.Where(c => c.empId == keeper.empId).First();
                        if (emp.compCode == compCode)
                        {
                            employee = emp;
                            break;
                        }
                    }
                }

                var checkShopSigneds = _context.CheckShopSigneds.Where(c => c.id == signedId).First();
                var empChecker = _context.Employees.Where(c => c.empId == int.Parse(checkShopSigneds.checkerId)).ToList();
                Checker checker = new Checker();
                if (empChecker.Count > 0)
                {
                    checker.empId = empChecker[0].empId.ToString();
                    checker.empName = empChecker[0].empName;
                }


                Employee EmployeeMANAGER_MCMC = new Employee();
                EmployeeCC EmployeeMCMC = new EmployeeCC();
                if (compCode == "3200")
                {
                    EmployeeMANAGER_MCMC = _context.Employees.Where(c => c.empId == int.Parse(MANAGER_MCMC)).First();
                    EmployeeMCMC.empId = EmployeeMANAGER_MCMC.empId;
                    EmployeeMCMC.empName = EmployeeMANAGER_MCMC.empName;
                    EmployeeMCMC.empPosition = EmployeeMANAGER_MCMC.empPosition;
                    EmployeeMCMC.signature = EmployeeMANAGER_MCMC.signature;
            
                }           

                return await Task.FromResult(new CheckShopSignedWithDetailResponse
                {
                    status = "success",
                    message = "",
                    checkShopSigned = checkShopSigneds,
                    employee = employee,
                    checkTransaction = checkAssetList,
                    checkShopDetail = checkShopDetail,
                    checker = checker,
                    planDate = task.planDate,
                    checkDate = task.checkDate,
                    accountDate = task.accountDate,
                    employeeCC = EmployeeMCMC,
                    shop = shop,
                });
            }
            catch (Exception e)
            {
                return await Task.FromResult(new CheckShopSignedWithDetailResponse
                {
                    status = "error",
                    message = e.Message,

                });
            }
        }

        [HttpPost("addAsset")]
        public async Task<ActionResult<Response>> addAsset(Asset asset, int checkShopId, int accountBalance, int assetTypeId)
        {
            _context.Database.BeginTransaction();
            try
            {
                _context.Assets.Add(asset);
                await _context.SaveChangesAsync();
                var assetData = await _context.Assets.FindAsync(asset.assetId);
                var type = _context.AssetTypes.Where(c => c.typeName == assetData.assetType).First();

                //create asset transaction
                var transaction = new CheckShopTransaction
                {
                    checkShopId = checkShopId,
                    assetId = assetData.assetId,
                    compCode = assetData.compCode,
                    assetTypeID = assetTypeId,
                    accountBalance = accountBalance,
                };
                _context.CheckShopTransactions.Add(transaction);
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                _context.Database.RollbackTransaction();
                return await Task.FromResult(new Response
                {
                    status = "error",
                    message = e.Message
                });
            }

            _context.Database.CommitTransaction();
            return await Task.FromResult(new Response
            {
                status = "success",
                message = "เพิ่มสำเร็จ"
            });
        }

        [HttpGet("downloadTemplate")]
        public IActionResult downloadTemplate()
        {
            try
            {
                var excel = System.IO.File.OpenRead(assetTemplatePath + "\\Template_MAI_ยืนยันยอดลูกหนี้.xlsx");
                return File(excel, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Template_MAI_ยืนยันยอดลูกหนี้.xlsx");
            }
            catch
            {
                return NotFound();
            }
        }

        private String getOldKeeper(String compCode, int shopId)
        {
            var keepers = _context.Shopkeepers.Where(c => c.shopId == shopId && c.isNew == false).ToList();
            if (keepers.Count > 0)
            {
                foreach (var keeper in keepers)
                {
                    var emp = _context.Employees.Where(c => c.empId == keeper.empId && c.compCode == compCode).ToList();
                    if (emp.Count > 0)
                    {
                        return emp[0].empId.ToString();
                    }
                }
            }
            return "";
        }

        private bool isHasNewKeeper(String compCode, int shopId)
        {
            var keepers = _context.Shopkeepers.Where(c => c.shopId == shopId && c.isNew == true).ToList();
            if (keepers.Count > 0)
            {
                foreach (var keeper in keepers)
                {
                    var employee = _context.Employees.Where(c => c.empId == keeper.empId && c.compCode == compCode).ToList();
                    if (employee.Count > 0)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        [HttpGet("downloadReportForKeeper/{periodId}/{compCode}/{empId}")]
        public IActionResult downloadReportForKeeper(int periodId, String compCode, int empId)
        {
            var period = _context.Periods.Where(c => c.periodId == periodId).First();
            var tasks = _context.Tasks.Where(c => c.periodId == periodId).ToList();
            var company = _context.Companies.Where(c => c.compCode == compCode).First();
            var employee = _context.Employees.Where(c => c.empId == empId).First();
            var department = _context.Departments.Where(c => c.departmentId == employee.departmentId).First();

            List<AssetTask> correctTask = new();
            List<AssetTask> incorrectTask = new();
            List<AssetTask> notFound = new();

            List<String> correctOldKeeper = new();
            List<String> incorrectOldKeeper = new();
            List<String> notFoundOldKeeper = new();
            List<ReportByAssetTaskResp> reportByAssetTaskRespsCorrectTask = new List<ReportByAssetTaskResp>();
            List<ReportByAssetTaskResp> reportByAssetTaskRespsIncorrectTask = new List<ReportByAssetTaskResp>();

            foreach (var task in tasks)
            { //.Where(x=>x.taskID == 39437)
                var shop = _context.Shops.Where(c => c.shopId == task.shopId).First();
                var keeper = _context.Shopkeepers.Where(c => c.shopId == shop.shopId && c.empId == employee.empId).ToList();
                if (keeper.Count > 0)
                {
                    var checkShop = _context.CheckShops.Where(c => c.taskID == task.taskID).First();
                    var checkShopSigneds = _context.CheckShopSigneds.Where(c => c.checkShopId == checkShop.checkShopId && c.compCode == compCode).ToList();

                    var emp = "";
                    var isHaveNewKeeper = false;
                    if (keeper[0].isNew)
                    {
                        emp = getOldKeeper(compCode, shop.shopId);
                    }
                    else
                    {
                        isHaveNewKeeper = isHasNewKeeper(compCode, shop.shopId);
                    }

                    if (!isHaveNewKeeper)
                    {
                        if (checkShopSigneds.Count > 0)
                        {
                            foreach (var checkSigned in checkShopSigneds)
                            {
                                if (checkSigned.confirmStatus == "ถูกต้อง")
                                {
                                    correctTask.Add(task);
                                    correctOldKeeper.Add(emp);
                                    ReportByAssetTaskResp reportByAssetTaskResp = new ReportByAssetTaskResp();
                                    string jsonString = JsonSerializer.Serialize(task);
                                    reportByAssetTaskResp = JsonSerializer.Deserialize<ReportByAssetTaskResp>(jsonString);
                                    reportByAssetTaskResp.assetTypeID = checkSigned.assetTypeID;
                                    reportByAssetTaskRespsCorrectTask.Add(reportByAssetTaskResp);
                                }
                                else
                                {
                                    incorrectTask.Add(task);
                                    incorrectOldKeeper.Add(emp);
                                    ReportByAssetTaskResp reportByAssetTaskResp = new ReportByAssetTaskResp();
                                    string jsonString = JsonSerializer.Serialize(task);
                                    reportByAssetTaskResp = JsonSerializer.Deserialize<ReportByAssetTaskResp>(jsonString);
                                    reportByAssetTaskResp.assetTypeID = checkSigned.assetTypeID;
                                    reportByAssetTaskRespsIncorrectTask.Add(reportByAssetTaskResp);
                                }
                            }
                        }
                        else
                        {
                            notFound.Add(task);
                            notFoundOldKeeper.Add(emp);
                        }
                    }
                }
            }

            var stream = new MemoryStream();
            using (var xlPackage = new ExcelPackage(stream))
            {
                var worksheet = xlPackage.Workbook.Worksheets.Add("รายงานสำหรับผู้ดูแล");
                worksheet.View.ShowGridLines = false;
                var namedStyle = xlPackage.Workbook.Styles.CreateNamedStyle("HyperLink");
                namedStyle.Style.Font.UnderLine = true;
                namedStyle.Style.Font.Color.SetColor(Color.Blue);
                const int startRow = 11;
                var row = startRow;

                //Create Headers and format them
                worksheet.Cells["B1"].Value = "รายงานสำหรับผู้ดูแลลูกค้าเซ็นรับทราบ";
                using (var r = worksheet.Cells["B1:L1"])
                {
                    r.Merge = true;
                    r.Style.Font.Color.SetColor(Color.White);
                    r.Style.HorizontalAlignment = ExcelHorizontalAlignment.CenterContinuous;
                    r.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    r.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(23, 55, 93));
                }

                worksheet.Cells["B3"].Value = "สรุปผลการตรวจยืนยันยอดลูกหนี้ POP POS และอุปกรณ์เบียร์สด";
                worksheet.Cells["B5"].Value = "ระหว่างวันที่ " + period.startDate.ToString("dd-MM-yyyy") + " ถึง วันที่ " + period.endDate.ToString("dd-MM-yyyy");
                worksheet.Cells["B6"].Value = "บริษัท " + company.companyName + " จำกัด";
                worksheet.Cells["B7"].Value = "หน่วยขาย/โกดัง : " + department.departmentName;
                worksheet.Cells["B8"].Value = "ชื่อพนักงานผู้ดูแลปัจจุบัน : " + employee.empName;

                worksheet.Cells["B10"].Value = "ชื่อลูกค้า";
                worksheet.Cells["C10"].Value = "ผู้ดูแลตามบัญชี";
                worksheet.Cells["D10"].Value = "สถานะร้านค้า";
                worksheet.Cells["E10"].Value = "วันที่ตามบัญชี";
                worksheet.Cells["F10"].Value = "วันที่ตรวจนับ";
                worksheet.Cells["G10"].Value = "รหัสทรัพย์สิน";
                worksheet.Cells["H10"].Value = "ชื่อPOP/POS/อุปกรณ์เบียร์สด";
                worksheet.Cells["I10"].Value = "ยอดตามบัญชี";
                worksheet.Cells["J10"].Value = "รวมตรวจนับ";
                worksheet.Cells["K10"].Value = " ผลต่าง(ขาด)เกิน";
                worksheet.Cells["L10"].Value = "ผลการยืนยัน";

                worksheet.Column(2).Width = 70;
                worksheet.Column(3).Width = 40;
                worksheet.Column(4).Width = 20;
                worksheet.Column(5).Width = 20;
                worksheet.Column(6).Width = 20;
                worksheet.Column(7).Width = 20;
                worksheet.Column(8).Width = 70;
                worksheet.Column(9).Width = 20;
                worksheet.Column(10).Width = 20;
                worksheet.Column(11).Width = 20;
                worksheet.Column(12).Width = 20;

                worksheet.Cells["B10:L10"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["B10:L10"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(184, 204, 228));
                worksheet.Cells["B10:L10"].Style.Font.Bold = true;

                var tableHeader = worksheet.Cells["B10:L10"];
                tableHeader.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                tableHeader.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                tableHeader.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                tableHeader.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                tableHeader.Style.HorizontalAlignment = ExcelHorizontalAlignment.CenterContinuous;

                String imagePath = "";
                var grandAccount = 0;
                var grandAmount = 0;

                List<Checker> checkers = new();
                int i = 0;
                //Create correct row
                //if (correctTask.Count > 0)
                if (reportByAssetTaskRespsCorrectTask.Count > 0)
                {
                    var account = 0;
                    var amount = 0;
                    foreach (var task in reportByAssetTaskRespsCorrectTask)
                    {
                        var checkShop = _context.CheckShops.Where(c => c.taskID == task.taskID).First();
                        var checkShopSigneds = _context.CheckShopSigneds.Where(c => c.checkShopId == checkShop.checkShopId && c.compCode == compCode).ToList();

                        createCell(worksheet, row, 2, task.taskName);

                        if (correctOldKeeper[i] != "")
                        {
                            var emp = _context.Employees.Where(c => c.empId == int.Parse(correctOldKeeper[i])).First();
                            createCellCenter(worksheet, row, 3, emp.empName);
                        }
                        else
                        {
                            createCellCenter(worksheet, row, 3, employee.empName);
                        }
                        i++;

                        createCellCenter(worksheet, row, 4, getShopStatus(checkShop.shopStatus));

                        createCellCenter(worksheet, row, 5, task.accountDate.ToString("dd-MM-yyyy"));

                        if (task.checkDate.ToString("dd-MM-yyyy") == "01-01-0001")
                        {
                            createCellCenter(worksheet, row, 6, "-");
                        }
                        else
                        {
                            createCellCenter(worksheet, row, 6, task.checkDate.ToString("dd-MM-yyyy"));
                        }

                        int assetType = 0;

                        if (checkShopSigneds.Count > 0)
                        {
                            foreach (var checkSigned in checkShopSigneds)
                            {
                                var empCheck = _context.Employees.Where(c => c.empId == int.Parse(checkSigned.checkerId)).First();
                                Checker checker = new();
                                checker.empId = empCheck.empId.ToString();
                                checker.empName = empCheck.empName;

                                if (!isCheckerInList(checkers, empCheck.empId.ToString()))
                                {
                                    checkers.Add(checker);
                                }

                                if (checkSigned.confirmStatus == "ถูกต้อง")
                                {
                                    assetType = checkSigned.assetTypeID;
                                    createCellCenter(worksheet, row, 12, checkSigned.confirmStatus);
                                    break;
                                }
                                else
                                {
                                    assetType = checkSigned.assetTypeID;
                                    createCellCenter(worksheet, row, 12, "ไม่ประสงค์ยืนยันยอด");
                                }
                            }
                        }
                        else
                        {
                            createCellCenter(worksheet, row, 12, "-");
                        }

                        List<CheckShopTransaction> checkShopTransaction;

                        //if (assetType == 0)
                        //{
                        //    checkShopTransaction = _context.CheckShopTransactions.Where(c => c.checkShopId == checkShop.checkShopId && c.compCode == compCode).ToList();
                        //}
                        //else if (assetType == 3)
                        //{
                        //    checkShopTransaction = _context.CheckShopTransactions.Where(c => c.checkShopId == checkShop.checkShopId && c.compCode == compCode && c.assetTypeID == 3).ToList();
                        //}
                        //else 
                        //{
                        //    checkShopTransaction = _context.CheckShopTransactions.Where(c => c.checkShopId == checkShop.checkShopId && c.compCode == compCode && (c.assetTypeID == 1 || c.assetTypeID == 2)).ToList();
                        //}
                        //int[] arrTypeAssetId = new int[0];

                        //if(checkShopSigneds.Count > 0)
                        //{
                        //    arrTypeAssetId = new int[checkShopSigneds.Count];
                        //    int iType = 0;
                        //    foreach(var item in checkShopSigneds)
                        //    {
                        //        arrTypeAssetId[iType] = item.assetTypeID;
                        //        iType++;
                        //    }
                        //}
                        /// (c.assetTypeID == 1 || c.assetTypeID == 2)

                        checkShopTransaction = _context.CheckShopTransactions.Where(c => c.checkShopId == checkShop.checkShopId && c.compCode == compCode && ((c.assetTypeID == 1 && c.assetTypeID == task.assetTypeID) || (c.assetTypeID == 2 && 1 == task.assetTypeID) || (c.assetTypeID == 3 && c.assetTypeID == task.assetTypeID))).ToList();


                        if (checkShopTransaction.Count > 0)
                        {
                            worksheet.Cells[row, 2, row + checkShopTransaction.Count - 1, 2].Merge = true;
                            worksheet.Cells[row, 3, row + checkShopTransaction.Count - 1, 3].Merge = true;
                            worksheet.Cells[row, 4, row + checkShopTransaction.Count - 1, 4].Merge = true;
                            worksheet.Cells[row, 5, row + checkShopTransaction.Count - 1, 5].Merge = true;
                            worksheet.Cells[row, 6, row + checkShopTransaction.Count - 1, 6].Merge = true;
                            worksheet.Cells[row, 12, row + checkShopTransaction.Count - 1, 12].Merge = true;


                            var currentRow = row;
                            foreach (var transaction in checkShopTransaction)
                            {
                                var asset = _context.Assets.Where(c => c.assetId == transaction.assetId).First();

                                account += transaction.accountBalance;
                                amount += transaction.avaliable + transaction.damaged;

                                var different = "";
                                if (transaction.accountBalance > (transaction.avaliable + transaction.damaged))
                                {
                                    different = "(" + (transaction.accountBalance - (transaction.avaliable + transaction.damaged)).ToString() + ")";
                                }
                                else
                                {
                                    different = ((transaction.avaliable + transaction.damaged) - transaction.accountBalance).ToString();
                                }

                                createCell(worksheet, currentRow, 7, asset.assetCode);
                                createCell(worksheet, currentRow, 8, asset.assetName);
                                createCellCenter(worksheet, currentRow, 9, transaction.accountBalance.ToString());
                                createCellCenter(worksheet, currentRow, 10, (transaction.avaliable + transaction.damaged).ToString());
                                createCellCenter(worksheet, currentRow, 11, different);
                                currentRow++;
                            }

                            row += checkShopTransaction.Count;
                        }
                        else
                        {
                            createCellCenter(worksheet, row, 7, "-");
                            createCellCenter(worksheet, row, 8, "-");
                            createCellCenter(worksheet, row, 9, "0");
                            createCellCenter(worksheet, row, 10, "0");
                            createCellCenter(worksheet, row, 11, "0");
                            row++;
                        }
                    }

                    var diff = "";
                    if (account > amount)
                    {
                        diff = "(" + (account - amount).ToString() + ")";
                    }
                    else
                    {
                        diff = (amount - account).ToString();
                    }

                    createCellCenter(worksheet, row, 2, "ถูกต้อง Total");
                    createCellCenter(worksheet, row, 9, account.ToString());
                    createCellCenter(worksheet, row, 10, amount.ToString());
                    createCellCenter(worksheet, row, 11, diff);
                    createCellCenter(worksheet, row, 12, "");
                    worksheet.Cells[row, 2, row, 6].Merge = true;
                    worksheet.Cells[row, 2, row, 12].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[row, 2, row, 12].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(217, 217, 217));
                    worksheet.Cells[row, 2, row, 12].Style.Font.Bold = true;

                    grandAccount += account;
                    grandAmount += amount;

                    row++;
                }

                //Create incorrect row
                i = 0;
                //if (incorrectTask.Count > 0)
                if (reportByAssetTaskRespsIncorrectTask.Count > 0)
                {
                    var account = 0;
                    var amount = 0;

                    foreach (var task in reportByAssetTaskRespsIncorrectTask)
                    {

                        var checkShop = _context.CheckShops.Where(c => c.taskID == task.taskID).First();
                        var checkShopSigneds = _context.CheckShopSigneds.Where(c => c.checkShopId == checkShop.checkShopId && c.compCode == compCode).ToList();

                        createCell(worksheet, row, 2, task.taskName);

                        if (incorrectOldKeeper[i] != "")
                        {
                            var emp = _context.Employees.Where(c => c.empId == int.Parse(incorrectOldKeeper[i])).First();
                            createCellCenter(worksheet, row, 3, emp.empName);
                        }
                        else
                        {
                            createCellCenter(worksheet, row, 3, employee.empName);
                        }
                        i++;

                        createCellCenter(worksheet, row, 4, getShopStatus(checkShop.shopStatus));

                        createCellCenter(worksheet, row, 5, task.accountDate.ToString("dd-MM-yyyy"));

                        if (task.checkDate.ToString("dd-MM-yyyy") == "01-01-0001")
                        {
                            createCellCenter(worksheet, row, 6, "-");
                        }
                        else
                        {
                            createCellCenter(worksheet, row, 6, task.checkDate.ToString("dd-MM-yyyy"));
                        }

                        int assetType = 0;

                        if (checkShopSigneds.Count > 0)
                        {
                            foreach (var checkSigned in checkShopSigneds)
                            {
                                var empCheck = _context.Employees.Where(c => c.empId == int.Parse(checkSigned.checkerId)).First();
                                Checker checker = new();
                                checker.empId = empCheck.empId.ToString();
                                checker.empName = empCheck.empName;

                                if (!isCheckerInList(checkers, empCheck.empId.ToString()))
                                {
                                    checkers.Add(checker);
                                }

                                if (checkSigned.confirmStatus == "ไม่ถูกต้อง")
                                {
                                    assetType = checkSigned.assetTypeID;
                                    createCellCenter(worksheet, row, 12, checkSigned.confirmStatus);
                                    break;
                                }
                                else
                                {
                                    assetType = checkSigned.assetTypeID;
                                    createCellCenter(worksheet, row, 12, "ไม่ประสงค์ยืนยันยอด");
                                }
                            }
                        }
                        else
                        {
                            createCellCenter(worksheet, row, 12, "-");
                        }

                        List<CheckShopTransaction> checkShopTransaction;

                        //if (assetType == 0)
                        //{
                        //    checkShopTransaction = _context.CheckShopTransactions.Where(c => c.checkShopId == checkShop.checkShopId && c.compCode == compCode).ToList();
                        //}
                        //else if (assetType == 3)
                        //{
                        //    checkShopTransaction = _context.CheckShopTransactions.Where(c => c.checkShopId == checkShop.checkShopId && c.compCode == compCode && c.assetTypeID == 3).ToList();
                        //}
                        //else
                        //{
                        //    checkShopTransaction = _context.CheckShopTransactions.Where(c => c.checkShopId == checkShop.checkShopId && c.compCode == compCode && (c.assetTypeID == 1 || c.assetTypeID == 2)).ToList();
                        //}
                        checkShopTransaction = _context.CheckShopTransactions.Where(c => c.checkShopId == checkShop.checkShopId && c.compCode == compCode && ((c.assetTypeID == 1 && c.assetTypeID == task.assetTypeID) || (c.assetTypeID == 2 && 1 == task.assetTypeID) || (c.assetTypeID == 3 && c.assetTypeID == task.assetTypeID))).ToList();

                        if (checkShopTransaction.Count > 0)
                        {
                            worksheet.Cells[row, 2, row + checkShopTransaction.Count - 1, 2].Merge = true;
                            worksheet.Cells[row, 3, row + checkShopTransaction.Count - 1, 3].Merge = true;
                            worksheet.Cells[row, 4, row + checkShopTransaction.Count - 1, 4].Merge = true;
                            worksheet.Cells[row, 5, row + checkShopTransaction.Count - 1, 5].Merge = true;
                            worksheet.Cells[row, 6, row + checkShopTransaction.Count - 1, 6].Merge = true;
                            worksheet.Cells[row, 12, row + checkShopTransaction.Count - 1, 12].Merge = true;


                            var currentRow = row;
                            foreach (var transaction in checkShopTransaction)
                            {
                                var asset = _context.Assets.Where(c => c.assetId == transaction.assetId).First();

                                account += transaction.accountBalance;
                                amount += transaction.avaliable + transaction.damaged;

                                var different = "";
                                if (transaction.accountBalance > (transaction.avaliable + transaction.damaged))
                                {
                                    different = "(" + (transaction.accountBalance - (transaction.avaliable + transaction.damaged)).ToString() + ")";
                                }
                                else
                                {
                                    different = ((transaction.avaliable + transaction.damaged) - transaction.accountBalance).ToString();
                                }

                                createCell(worksheet, currentRow, 7, asset.assetCode);
                                createCell(worksheet, currentRow, 8, asset.assetName);
                                createCellCenter(worksheet, currentRow, 9, transaction.accountBalance.ToString());
                                createCellCenter(worksheet, currentRow, 10, (transaction.avaliable + transaction.damaged).ToString());
                                createCellCenter(worksheet, currentRow, 11, different);
                                currentRow++;
                            }

                            row += checkShopTransaction.Count;
                        }
                        else
                        {
                            createCellCenter(worksheet, row, 7, "-");
                            createCellCenter(worksheet, row, 8, "-");
                            createCellCenter(worksheet, row, 9, "0");
                            createCellCenter(worksheet, row, 10, "0");
                            createCellCenter(worksheet, row, 11, "0");
                            row++;
                        }
                    }

                    var diff = "";
                    if (account > amount)
                    {
                        diff = "(" + (account - amount).ToString() + ")";
                    }
                    else
                    {
                        diff = (amount - account).ToString();
                    }

                    createCellCenter(worksheet, row, 2, "ไม่ถูกต้อง Total");
                    createCellCenter(worksheet, row, 9, account.ToString());
                    createCellCenter(worksheet, row, 10, amount.ToString());
                    createCellCenter(worksheet, row, 11, diff);
                    createCellCenter(worksheet, row, 12, "");
                    worksheet.Cells[row, 2, row, 6].Merge = true;
                    worksheet.Cells[row, 2, row, 12].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[row, 2, row, 12].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(217, 217, 217));
                    worksheet.Cells[row, 2, row, 12].Style.Font.Bold = true;

                    grandAccount += account;
                    grandAmount += amount;

                    row++;
                }

                //Create not found row
                i = 0;
                if (notFound.Count > 0)
                {
                    var account = 0;
                    var amount = 0;

                    foreach (var task in notFound)
                    {

                        var checkShop = _context.CheckShops.Where(c => c.taskID == task.taskID).First();
                        var checkShopTransaction = _context.CheckShopTransactions.Where(c => c.checkShopId == checkShop.checkShopId && c.compCode == compCode).ToList();
                        var checkShopSigneds = _context.CheckShopSigneds.Where(c => c.checkShopId == checkShop.checkShopId && c.compCode == compCode).ToList();

                        createCell(worksheet, row, 2, task.taskName);

                        if (notFoundOldKeeper[i] != "")
                        {
                            var emp = _context.Employees.Where(c => c.empId == int.Parse(notFoundOldKeeper[i])).First();
                            createCell(worksheet, row, 3, emp.empName);
                        }
                        else
                        {
                            createCellCenter(worksheet, row, 3, "-");
                        }
                        i++;

                        createCellCenter(worksheet, row, 4, getShopStatus(checkShop.shopStatus));

                        createCellCenter(worksheet, row, 5, task.accountDate.ToString("dd-MM-yyyy"));

                        if (task.checkDate.ToString("dd-MM-yyyy") == "01-01-0001")
                        {
                            createCellCenter(worksheet, row, 6, "-");
                        }
                        else
                        {
                            createCellCenter(worksheet, row, 6, task.checkDate.ToString("dd-MM-yyyy"));
                        }

                        if (checkShopSigneds.Count > 0)
                        {
                            var empCheck = _context.Employees.Where(c => c.empId == int.Parse(checkShopSigneds[0].checkerId)).First();
                            Checker checker = new();
                            checker.empId = empCheck.empId.ToString();
                            checker.empName = empCheck.empName;

                            if (!isCheckerInList(checkers, empCheck.empId.ToString()))
                            {
                                checkers.Add(checker);
                            }

                            createCellCenter(worksheet, row, 12, checkShopSigneds[0].confirmStatus);
                        }
                        else
                        {
                            createCellCenter(worksheet, row, 12, "-");
                        }

                        if (checkShopTransaction.Count > 0)
                        {
                            worksheet.Cells[row, 2, row + checkShopTransaction.Count - 1, 2].Merge = true;
                            worksheet.Cells[row, 3, row + checkShopTransaction.Count - 1, 3].Merge = true;
                            worksheet.Cells[row, 4, row + checkShopTransaction.Count - 1, 4].Merge = true;
                            worksheet.Cells[row, 5, row + checkShopTransaction.Count - 1, 5].Merge = true;
                            worksheet.Cells[row, 6, row + checkShopTransaction.Count - 1, 6].Merge = true;
                            worksheet.Cells[row, 12, row + checkShopTransaction.Count - 1, 12].Merge = true;


                            var currentRow = row;
                            foreach (var transaction in checkShopTransaction)
                            {
                                var asset = _context.Assets.Where(c => c.assetId == transaction.assetId).First();

                                account += transaction.accountBalance;
                                amount += transaction.avaliable + transaction.damaged;

                                var different = "";
                                if (transaction.accountBalance > (transaction.avaliable + transaction.damaged))
                                {
                                    different = "(" + (transaction.accountBalance - (transaction.avaliable + transaction.damaged)).ToString() + ")";
                                }
                                else
                                {
                                    different = ((transaction.avaliable + transaction.damaged) - transaction.accountBalance).ToString();
                                }

                                createCell(worksheet, currentRow, 7, asset.assetCode);
                                createCell(worksheet, currentRow, 8, asset.assetName);
                                createCellCenter(worksheet, currentRow, 9, transaction.accountBalance.ToString());
                                createCellCenter(worksheet, currentRow, 10, (transaction.avaliable + transaction.damaged).ToString());
                                createCellCenter(worksheet, currentRow, 11, different);
                                currentRow++;
                            }

                            row += checkShopTransaction.Count;
                        }
                        else
                        {
                            createCellCenter(worksheet, row, 7, "-");
                            createCellCenter(worksheet, row, 8, "-");
                            createCellCenter(worksheet, row, 9, "0");
                            createCellCenter(worksheet, row, 10, "0");
                            createCellCenter(worksheet, row, 11, "0");
                            row++;
                        }
                    }

                    var diff = "";
                    if (account > amount)
                    {
                        diff = "(" + (account - amount).ToString() + ")";
                    }
                    else
                    {
                        diff = (amount - account).ToString();
                    }

                    createCellCenter(worksheet, row, 2, "ไม่ได้ยืนยัน Total");
                    createCellCenter(worksheet, row, 9, account.ToString());
                    createCellCenter(worksheet, row, 10, amount.ToString());
                    createCellCenter(worksheet, row, 11, diff);
                    createCellCenter(worksheet, row, 12, "");
                    worksheet.Cells[row, 2, row, 6].Merge = true;
                    worksheet.Cells[row, 2, row, 12].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[row, 2, row, 12].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(217, 217, 217));
                    worksheet.Cells[row, 2, row, 12].Style.Font.Bold = true;

                    grandAccount += account;
                    grandAmount += amount;

                    row++;
                }

                var grandDiff = "";
                if (grandAccount > grandAmount)
                {
                    grandDiff = "(" + (grandAccount - grandAmount).ToString() + ")";
                }
                else
                {
                    grandDiff = (grandAmount - grandAccount).ToString();
                }

                createCellCenter(worksheet, row, 2, "Grand Total");
                createCellCenter(worksheet, row, 9, grandAccount.ToString());
                createCellCenter(worksheet, row, 10, grandAmount.ToString());
                createCellCenter(worksheet, row, 11, grandDiff);
                createCellCenter(worksheet, row, 12, "");
                worksheet.Cells[row, 2, row, 6].Merge = true;
                worksheet.Cells[row, 2, row, 12].Style.Font.Bold = true;
                row++;

                var tableBody = worksheet.Cells[startRow, 2, row - 1, 12];
                tableBody.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                tableBody.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                tableBody.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                tableBody.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                //tableBody.Style.HorizontalAlignment = ExcelHorizontalAlignment.CenterContinuous;

                row += 8;
                createCellCenter(worksheet, row + 5, 2, "ลงชื่อ.............................................................เจ้าหน้าที่ผู้ดูแลลูกค้า");
                createCellCenter(worksheet, row + 6, 2, "(" + employee.empName + ")");

                if (checkers.Count > 0)
                {
                    foreach (var checker in checkers)
                    {
                        createCellCenter(worksheet, row + 5, 7, "ลงชื่อ.............................................................เจ้าหน้าที่ตรวจปฏิบัติการ");
                        createCellCenter(worksheet, row + 6, 7, "(" + checker.empName + ")");

                        var checkEmp = _context.Employees.Where(c => c.empId == int.Parse(checker.empId)).First();

                        try
                        {
                            using Image image = Image.FromFile(checkEmp.signature);
                            var excelImage = worksheet.Drawings.AddPicture("signature_" + checker.empId + row.ToString(), image);
                            excelImage.SetSize(120, 120);
                            excelImage.SetPosition(row - 2, 0, 5, 120);
                        }
                        catch (Exception e)
                        {
                            //do nothing
                        }

                        row += 12;
                    }
                }

                // set some core property values
                xlPackage.Workbook.Properties.Title = "MAI ยืนยันยอดลูกหนี้";
                xlPackage.Workbook.Properties.Author = "MAI admin";
                xlPackage.Workbook.Properties.Subject = "Keeper report";
                // save the new spreadsheet
                xlPackage.Save();
                // Response.Clear();
            }
            stream.Position = 0;
            return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "รายงานผู้ดูแล_" + employee.empName + ".xlsx");
        }

        private bool isCheckerInList(List<Checker> checkers, String empId)
        {
            foreach (var checker in checkers)
            {
                if (checker.empId == empId)
                {
                    return true;
                }
            }
            return false;
        }

        private void createCell(ExcelWorksheet worksheet, int row, int range, String value)
        {
            var table = worksheet.Cells[row, range];
            table.Value = value;
            //table.Style.HorizontalAlignment = ExcelHorizontalAlignment.CenterContinuous;
            table.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        }

        private void createCellCenter(ExcelWorksheet worksheet, int row, int range, String value)
        {
            var table = worksheet.Cells[row, range];
            table.Value = value;
            table.Style.HorizontalAlignment = ExcelHorizontalAlignment.CenterContinuous;
            table.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        }

        private void setBorders(ExcelRange table)
        {
            table.Style.Border.Top.Style = ExcelBorderStyle.Thin;
            table.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            table.Style.Border.Right.Style = ExcelBorderStyle.Thin;
            table.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
        }

        private String getShopStatus(String status)
        {
            if (status == "open")
            {
                return "เปิด";
            }
            else if (status == "close")
            {
                return "ปิด";
            }
            else if (status == "go-out-of-business")
            {
                return "เลิกกิจการ";
            }
            else if (status == "unchecked")
            {
                return "ไม่ได้เข้าตรวจ";
            }
            else
            {
                return "หาร้านไม่พบ";
            }
        }

        private List<Employee> getKeeperWithCompCodeAndCenter(List<Shopkeeper> shopkeepers, String compCode, String center)
        {
            List<Employee> employees = new();
            foreach (var shop in shopkeepers)
            {
                var employee = _context.Employees.Where(c => c.empId == shop.empId && c.compCode == compCode).ToList();
                if (employee.Count > 0)
                {
                    var department = _context.Departments.Where(c => c.departmentId == employee[0].departmentId && c.center.Trim() == center.Trim()).ToList();
                    if (department.Count > 0)
                    {
                        employees.Add(employee[0]);
                    }
                }
            }
            return employees;
        }

        [HttpGet("downloadReportByCenter/{periodId}/{compCode}/{center}")]
        public IActionResult downloadReportByCompany(int periodId, String compCode, String center)
        {
            var period = _context.Periods.Where(c => c.periodId == periodId).First();
            var tasks = _context.Tasks.Where(c => c.periodId == periodId).ToList();
            var company = _context.Companies.Where(c => c.compCode == compCode).First();
            var department = getDepartmentByPeriodAndCenter(periodId, center, compCode, tasks);

            int allOpen = 0;
            int allClose = 0;
            int allnotFound = 0;
            int allGoOut = 0;
            int allUnCheck = 0;

            int openCorrect = 0;
            int openIncorrect = 0;
            int openIgnore = 0;
            int openUnCheck = 0;

            int closeCorrect = 0;
            int closeIncorrect = 0;
            int closeIgnore = 0;
            int closeUnCheck = 0;

            int goOutCorrect = 0;
            int goOutIncorrect = 0;
            int goOutIgnore = 0;
            int goOutUnCheck = 0;

            int notFoundCorrect = 0;
            int notFoundIncorrect = 0;
            int notFoundIgnore = 0;
            int notFoundUnCheck = 0;

            foreach (var task in tasks)
            {
                var shop = _context.Shops.Where(c => c.shopId == task.shopId).First();
                var keeper = _context.Shopkeepers.Where(c => c.shopId == shop.shopId).ToList();
                var employees = getKeeperWithCompCodeAndCenter(keeper, compCode, center);
                if (employees.Count > 0)
                {
                    var checkShop = _context.CheckShops.Where(c => c.taskID == task.taskID).First();
                    var checkShopSigned = _context.CheckShopSigneds.Where(c => c.checkShopId == checkShop.checkShopId && c.compCode == compCode).ToList();

                    if (checkShop.shopStatus == "open")
                    {
                        allOpen++;
                        if (checkShopSigned.Count > 0)
                        {
                            if (checkShopSigned[0].confirmStatus == "ถูกต้อง" && checkShopSigned[0].customerSigned.Trim() != "")
                            {
                                openCorrect++;
                            }
                            else if (checkShopSigned[0].confirmStatus == "ไม่ถูกต้อง" && checkShopSigned[0].customerSigned.Trim() != "")
                            {
                                openIncorrect++;
                            }
                            else
                            {
                                openIgnore++;
                            }
                        }
                        else
                        {
                            openUnCheck++;
                        }
                    }
                    else if (checkShop.shopStatus == "close")
                    {
                        allClose++;
                        if (checkShopSigned.Count > 0)
                        {
                            if (checkShopSigned[0].confirmStatus == "ถูกต้อง" && checkShopSigned[0].customerSigned.Trim() != "")
                            {
                                closeCorrect++;
                            }
                            else if (checkShopSigned[0].confirmStatus == "ไม่ถูกต้อง" && checkShopSigned[0].customerSigned.Trim() != "")
                            {
                                closeIncorrect++;
                            }
                            else
                            {
                                closeIgnore++;
                            }
                        }
                        else
                        {
                            closeUnCheck++;
                        }
                    }
                    else if (checkShop.shopStatus == "go-out-of-business")
                    {
                        allGoOut++;
                        if (checkShopSigned.Count > 0)
                        {
                            if (checkShopSigned[0].confirmStatus == "ถูกต้อง" && checkShopSigned[0].customerSigned.Trim() != "")
                            {
                                goOutCorrect++;
                            }
                            else if (checkShopSigned[0].confirmStatus == "ไม่ถูกต้อง" && checkShopSigned[0].customerSigned.Trim() != "")
                            {
                                goOutIncorrect++;
                            }
                            else
                            {
                                goOutIgnore++;
                            }
                        }
                        else
                        {
                            goOutUnCheck++;
                        }
                    }
                    else if (checkShop.shopStatus == "not-found")
                    {
                        allnotFound++;
                        if (checkShopSigned.Count > 0)
                        {
                            if (checkShopSigned[0].confirmStatus == "ถูกต้อง" && checkShopSigned[0].customerSigned.Trim() != "")
                            {
                                notFoundCorrect++;
                            }
                            else if (checkShopSigned[0].confirmStatus == "ไม่ถูกต้อง" && checkShopSigned[0].customerSigned.Trim() != "")
                            {
                                notFoundIncorrect++;
                            }
                            else
                            {
                                notFoundIgnore++;
                            }
                        }
                        else
                        {
                            notFoundUnCheck++;
                        }
                    }
                    else
                    {
                        allUnCheck++;
                    }
                }
            }

            var stream = new MemoryStream();
            using (var xlPackage = new ExcelPackage(stream))
            {
                var worksheet = xlPackage.Workbook.Worksheets.Add("รายงานสรุปทั้งหมด");
                worksheet.View.ShowGridLines = false;
                var namedStyle = xlPackage.Workbook.Styles.CreateNamedStyle("HyperLink");
                namedStyle.Style.Font.UnderLine = true;
                namedStyle.Style.Font.Color.SetColor(Color.Blue);
                const int startRow = 11;
                var row = startRow;

                //Create Headers and format them
                worksheet.Cells["B1"].Value = "รายงานสรุปผลการตรวจยืนยันยอดลูกหนี้";
                using (var r = worksheet.Cells["B1:L1"])
                {
                    r.Merge = true;
                    r.Style.Font.Color.SetColor(Color.White);
                    r.Style.HorizontalAlignment = ExcelHorizontalAlignment.CenterContinuous;
                    r.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    r.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(23, 55, 93));
                }

                worksheet.Cells["B3"].Value = "สรุปผลการตรวจยืนยันยอดลูกหนี้ POP POS และอุปกรณ์เบียร์สด";
                worksheet.Cells["B3:D3"].Merge = true;
                worksheet.Cells["B3:D3"].Style.Font.Bold = true;

                worksheet.Cells["B5"].Value = "ระหว่างวันที่ " + period.startDate.ToString("dd-MM-yyyy") + " ถึง วันที่ " + period.endDate.ToString("dd-MM-yyyy");
                worksheet.Cells["B5:D5"].Merge = true;
                worksheet.Cells["B5:D5"].Style.Font.Bold = true;

                worksheet.Cells["B6"].Value = "บริษัท " + company.companyName + " จำกัด";
                worksheet.Cells["B6"].Style.Font.Bold = true;

                worksheet.Cells["B7"].Value = "ศูนย์ " + center;
                worksheet.Cells["B7"].Style.Font.Bold = true;

                worksheet.Cells["B10"].Value = "สถานะร้านค้า";
                worksheet.Cells["B10:B11"].Merge = true;

                worksheet.Cells["C10"].Value = "ถูกต้อง";
                worksheet.Cells["C10:D10"].Merge = true;

                worksheet.Cells["E10"].Value = "ไม่ถูกต้อง";
                worksheet.Cells["E10:F10"].Merge = true;

                worksheet.Cells["G10"].Value = "ลูกค้าไม่ประสงค์ยืนยันยอด";
                worksheet.Cells["G10:H10"].Merge = true;

                worksheet.Cells["I10"].Value = "ไม่ได้ยืนยัน";
                worksheet.Cells["I10:J10"].Merge = true;

                worksheet.Cells["K10"].Value = "Total จำนวนร้าน";
                worksheet.Cells["K10:K11"].Merge = true;

                worksheet.Cells["L10"].Value = "Total % ร้านทั้งหมด";
                worksheet.Cells["L10:L11"].Merge = true;

                worksheet.Cells["C11"].Value = "จำนวนร้าน";
                worksheet.Cells["D11"].Value = "% ร้านทั้งหมด";
                worksheet.Cells["E11"].Value = "จำนวนร้าน";
                worksheet.Cells["F11"].Value = "% ร้านทั้งหมด";
                worksheet.Cells["G11"].Value = "จำนวนร้าน";
                worksheet.Cells["H11"].Value = "% ร้านทั้งหมด";
                worksheet.Cells["I11"].Value = "จำนวนร้าน";
                worksheet.Cells["J11"].Value = "% ร้านทั้งหมด";

                worksheet.Column(2).Width = 30;
                worksheet.Column(3).Width = 20;
                worksheet.Column(4).Width = 20;
                worksheet.Column(5).Width = 20;
                worksheet.Column(6).Width = 20;
                worksheet.Column(7).Width = 20;
                worksheet.Column(8).Width = 20;
                worksheet.Column(9).Width = 20;
                worksheet.Column(10).Width = 20;
                worksheet.Column(11).Width = 20;
                worksheet.Column(12).Width = 20;

                worksheet.Cells["B10:L10"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["B10:L10"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(184, 204, 228));
                worksheet.Cells["B10:L10"].Style.Font.Bold = true;

                worksheet.Cells["B11:L11"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["B11:L11"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(184, 204, 228));
                worksheet.Cells["B11:L11"].Style.Font.Bold = true;

                worksheet.Cells["B10:L10"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                worksheet.Cells["B10:L10"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                worksheet.Cells["B10:L10"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                worksheet.Cells["B10:L10"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                worksheet.Cells["B10:L10"].Style.HorizontalAlignment = ExcelHorizontalAlignment.CenterContinuous;

                worksheet.Cells["B11:L11"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                worksheet.Cells["B11:L11"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                worksheet.Cells["B11:L11"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                worksheet.Cells["B11:L11"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                worksheet.Cells["B11:L11"].Style.HorizontalAlignment = ExcelHorizontalAlignment.CenterContinuous;

                //open
                createCellCenter(worksheet, 12, 2, "เปิด");
                createCellCenter(worksheet, 12, 3, openCorrect.ToString());
                createCellCenter(worksheet, 12, 4, (calPercentText(openCorrect, allOpen)).ToString("P", CultureInfo.InvariantCulture));
                createCellCenter(worksheet, 12, 5, openIncorrect.ToString());
                createCellCenter(worksheet, 12, 6, (calPercentText(openIncorrect, allOpen)).ToString("P", CultureInfo.InvariantCulture));
                createCellCenter(worksheet, 12, 7, openIgnore.ToString());
                createCellCenter(worksheet, 12, 8, (calPercentText(openIgnore, allOpen)).ToString("P", CultureInfo.InvariantCulture));
                createCellCenter(worksheet, 12, 9, openUnCheck.ToString());
                createCellCenter(worksheet, 12, 10, (calPercentText(openUnCheck, allOpen)).ToString("P", CultureInfo.InvariantCulture));
                createCellCenter(worksheet, 12, 11, allOpen.ToString());
                createCellCenter(worksheet, 12, 12, "100%");

                //close
                createCellCenter(worksheet, 13, 2, "ปิด");
                createCellCenter(worksheet, 13, 3, closeCorrect.ToString());
                createCellCenter(worksheet, 13, 4, (calPercentText(closeCorrect, allClose)).ToString("P", CultureInfo.InvariantCulture));
                createCellCenter(worksheet, 13, 5, closeIncorrect.ToString());
                createCellCenter(worksheet, 13, 6, (calPercentText(closeIncorrect, allClose)).ToString("P", CultureInfo.InvariantCulture));
                createCellCenter(worksheet, 13, 7, closeIgnore.ToString());
                createCellCenter(worksheet, 13, 8, (calPercentText(closeIgnore, allClose)).ToString("P", CultureInfo.InvariantCulture));
                createCellCenter(worksheet, 13, 9, closeUnCheck.ToString());
                createCellCenter(worksheet, 13, 10, (calPercentText(closeUnCheck, allClose)).ToString("P", CultureInfo.InvariantCulture));
                createCellCenter(worksheet, 13, 11, allClose.ToString());
                createCellCenter(worksheet, 13, 12, "100%");

                //go out
                createCellCenter(worksheet, 14, 2, "เลิกกิจการ");
                createCellCenter(worksheet, 14, 3, goOutCorrect.ToString());
                createCellCenter(worksheet, 14, 4, (calPercentText(goOutCorrect, allGoOut)).ToString("P", CultureInfo.InvariantCulture));
                createCellCenter(worksheet, 14, 5, goOutIncorrect.ToString());
                createCellCenter(worksheet, 14, 6, (calPercentText(goOutIncorrect, allGoOut)).ToString("P", CultureInfo.InvariantCulture));
                createCellCenter(worksheet, 14, 7, goOutIgnore.ToString());
                createCellCenter(worksheet, 14, 8, (calPercentText(goOutIgnore, allGoOut)).ToString("P", CultureInfo.InvariantCulture));
                createCellCenter(worksheet, 14, 9, goOutUnCheck.ToString());
                createCellCenter(worksheet, 14, 10, (calPercentText(goOutUnCheck, allGoOut)).ToString("P", CultureInfo.InvariantCulture));
                createCellCenter(worksheet, 14, 11, allGoOut.ToString());
                createCellCenter(worksheet, 14, 12, "100%");

                //Not found
                createCellCenter(worksheet, 15, 2, "หาร้านไม่พบ");
                createCellCenter(worksheet, 15, 3, notFoundCorrect.ToString());
                createCellCenter(worksheet, 15, 4, (calPercentText(notFoundCorrect, allnotFound)).ToString("P", CultureInfo.InvariantCulture));
                createCellCenter(worksheet, 15, 5, notFoundIncorrect.ToString());
                createCellCenter(worksheet, 15, 6, (calPercentText(notFoundIncorrect, allnotFound)).ToString("P", CultureInfo.InvariantCulture));
                createCellCenter(worksheet, 15, 7, notFoundIgnore.ToString());
                createCellCenter(worksheet, 15, 8, (calPercentText(notFoundIgnore, allnotFound)).ToString("P", CultureInfo.InvariantCulture));
                createCellCenter(worksheet, 15, 9, notFoundUnCheck.ToString());
                createCellCenter(worksheet, 15, 10, (calPercentText(notFoundUnCheck, allnotFound)).ToString("P", CultureInfo.InvariantCulture));
                createCellCenter(worksheet, 15, 11, allnotFound.ToString());
                createCellCenter(worksheet, 15, 12, "100%");

                //uncheck
                createCellCenter(worksheet, 16, 2, "ไม่ได้เข้าร้าน");
                createCellCenter(worksheet, 16, 3, "0");
                createCellCenter(worksheet, 16, 4, (0).ToString("P", CultureInfo.InvariantCulture));
                createCellCenter(worksheet, 16, 5, "0");
                createCellCenter(worksheet, 16, 6, (0).ToString("P", CultureInfo.InvariantCulture));
                createCellCenter(worksheet, 16, 7, "0");
                createCellCenter(worksheet, 16, 8, (0).ToString("P", CultureInfo.InvariantCulture));
                createCellCenter(worksheet, 16, 9, allUnCheck.ToString());
                createCellCenter(worksheet, 16, 10, (calPercentText(allUnCheck, allUnCheck)).ToString("P", CultureInfo.InvariantCulture));
                createCellCenter(worksheet, 16, 11, allUnCheck.ToString());
                createCellCenter(worksheet, 16, 12, "100%");

                int allCorrect = openCorrect + closeCorrect + goOutCorrect + notFoundCorrect;
                int allIncorrect = openIncorrect + closeIncorrect + goOutIncorrect + notFoundIncorrect;
                int allIgnore = openIgnore + closeIgnore + goOutIgnore + notFoundIgnore;
                int allNotcheck = openUnCheck + closeUnCheck + goOutUnCheck + notFoundUnCheck + allUnCheck;
                int all = allOpen + allClose + allGoOut + allnotFound + allUnCheck;

                //Grand Total
                createCellCenter(worksheet, 17, 2, "Grand Total");
                createCellCenter(worksheet, 17, 3, allCorrect.ToString());
                createCellCenter(worksheet, 17, 4, (calPercentText(allCorrect, all)).ToString("P", CultureInfo.InvariantCulture));
                createCellCenter(worksheet, 17, 5, allIncorrect.ToString());
                createCellCenter(worksheet, 17, 6, (calPercentText(allIncorrect, all)).ToString("P", CultureInfo.InvariantCulture));
                createCellCenter(worksheet, 17, 7, allIgnore.ToString());
                createCellCenter(worksheet, 17, 8, (calPercentText(allIgnore, all)).ToString("P", CultureInfo.InvariantCulture));
                createCellCenter(worksheet, 17, 9, allNotcheck.ToString());
                createCellCenter(worksheet, 17, 10, (calPercentText(allNotcheck, all)).ToString("P", CultureInfo.InvariantCulture));
                createCellCenter(worksheet, 17, 11, all.ToString());
                createCellCenter(worksheet, 17, 12, "100%");


                var tableBody = worksheet.Cells[12, 2, 17, 12];
                tableBody.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                tableBody.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                tableBody.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                tableBody.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                tableBody.Style.HorizontalAlignment = ExcelHorizontalAlignment.CenterContinuous;

                worksheet.Cells["B17:L17"].Style.Font.Bold = true;
                worksheet.Cells["B17:L17"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["B17:L17"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(209, 209, 209));

                // set some core property values
                xlPackage.Workbook.Properties.Title = "MAI ยืนยันยอดลูกหนี้";
                xlPackage.Workbook.Properties.Author = "MAI admin";
                xlPackage.Workbook.Properties.Subject = "Keeper report";
                // save the new spreadsheet
                xlPackage.Save();
                // Response.Clear();
            }
            stream.Position = 0;
            return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "รายงานสรุปผลการตรวจทั้งหมด-ศูนย์" + center + ".xlsx");
        }

        private double calPercentText(int current, int max)
        {
            if (max == 0)
            {
                return 0;
            }
            else
            {
                double c = (double)current;
                double m = (double)max;

                return (((c / m) * 100) / 100);
            }
        }

        [HttpGet("getEmployeeByPeriod/{periodId}")]
        public async Task<EmployeeResponse> getEmployeeByPeriod(int periodId)
        {
            try
            {
                var period = await _context.Periods.FindAsync(periodId);
                var tasks = _context.Tasks.Where(c => c.periodId == period.periodId).ToList();

                List<Employee> employees = new();
                foreach (var task in tasks)
                {
                    var shop = _context.Shops.Where(c => c.shopId == task.shopId).ToList();
                    var shopKeeper = _context.Shopkeepers.Where(c => c.shopId == shop[0].shopId).ToList();
                    foreach (var keeper in shopKeeper)
                    {
                        var employee = _context.Employees.Where(c => c.empId == keeper.empId).First();
                        if (!employees.Contains(employee))
                        {
                            employees.Add(employee);
                        }
                    }
                }
                return await Task.FromResult(new EmployeeResponse
                {
                    status = "success",
                    message = "",
                    employees = employees,
                });
            }
            catch (Exception e)
            {
                return await Task.FromResult(new EmployeeResponse
                {
                    status = "error",
                    message = e.Message,

                });
            }
        }

        [HttpGet("getEmployeeProfile/{employeeId}")]
        public IActionResult getEmployeeProfile(int employeeId)
        {
            try
            {
                String profile = "https://beverest.thaibev.com/photo/photos/" + employeeId + ".jpg";
                WebClient client = new WebClient();
                Stream stream = client.OpenRead(profile);
                return File(stream, "image/jpeg");
            }
            catch
            {
                return NotFound();
            }
        }

        private class EmployeeListComparer : IEqualityComparer<List<Employee>>
        {
            public bool Equals(List<Employee> x, List<Employee> y)
            {
                return x.SequenceEqual(y);
            }

            public int GetHashCode(List<Employee> obj)
            {
                return obj.Take(5).Aggregate(23, (sum, s) => sum ^= s.GetHashCode());
            }
        }

        private List<Employee> getKeeper(List<Shop> shops)
        {
            List<Employee> employee = new();
            foreach (var shop in shops)
            {
                var keepers = _context.Shopkeepers.Where(c => c.shopId == shop.shopId).ToList();
                foreach (var keeper in keepers)
                {
                    var employees = _context.Employees.Where(c => c.empId == keeper.empId).ToList();
                    employee.AddRange(employees);
                }
            }
            return employee;
        }

        [HttpGet("getDepartmentByPeriod/{periodId}")]
        public async Task<ActionResult<DepartmentResponse>> getDepartmentByPeriod(int periodId)
        {
            var tasks = _context.Tasks.Where(c => c.periodId == periodId).ToList();
            List<Employee> employees = new();
            foreach (var task in tasks)
            {
                var shops = _context.Shops.Where(c => c.shopId == task.shopId).ToList();
                foreach (var employee in getKeeper(shops))
                {
                    if (!employees.Contains(employee))
                    {
                        employees.Add(employee);
                    }
                }
            }

            List<Department> departments = new();
            foreach (var employee in employees)
            {
                var department = await _context.Departments.FindAsync(employee.departmentId);
                if (department != null && !departments.Contains(department))
                {
                    departments.Add(department);
                }
            }

            if (departments.Count > 0)
            {
                return await Task.FromResult(new DepartmentResponse
                {
                    status = "success",
                    message = "",
                    departments = departments
                });
            }
            else
            {
                return await Task.FromResult(new DepartmentResponse
                {
                    status = "error",
                    message = "ไม่พบข้อมูล"
                });
            }
        }

        [HttpGet("getDepartmentByPeriodDistinctCenter/{periodId}")]
        public async Task<ActionResult<DepartmentResponse>> getDepartmentByPeriodDistinctCenter(int periodId)
        {
            var tasks = _context.Tasks.Where(c => c.periodId == periodId).ToList();
            List<Employee> employees = new();
            foreach (var task in tasks)
            {
                var shops = _context.Shops.Where(c => c.shopId == task.shopId).ToList();
                foreach (var employee in getKeeper(shops))
                {
                    if (!employees.Contains(employee))
                    {
                        employees.Add(employee);
                    }
                }
            }

            List<Department> departments = new();
            List<String> centers = new();
            List<String> compCodes = new();
            foreach (var employee in employees)
            {
                var department = await _context.Departments.FindAsync(employee.departmentId);
                if (department != null)
                {
                    if (!centers.Contains(department.center))
                    {
                        centers.Add(department.center);
                        compCodes.Add(department.compCode);
                        departments.Add(department);
                    }
                    else if (centers.Contains(department.center) && !compCodes.Contains(department.compCode))
                    {
                        compCodes.Add(department.compCode);
                        departments.Add(department);
                    }
                }
            }

            if (departments.Count > 0)
            {
                return await Task.FromResult(new DepartmentResponse
                {
                    status = "success",
                    message = "",
                    departments = departments
                });
            }
            else
            {
                return await Task.FromResult(new DepartmentResponse
                {
                    status = "error",
                    message = "ไม่พบข้อมูล"
                });
            }
        }

        private List<Department> getDepartmentByPeriodAndCenter(int periodId, String center, String compCode, List<AssetTask> tasks)
        {
            List<Employee> employees = new();
            List<Department> departments = new();
            foreach (var task in tasks)
            {
                var shop = _context.Shops.Where(c => c.shopId == task.shopId).First();
                var keeper = _context.Shopkeepers.Where(c => c.shopId == shop.shopId).ToList();
                var employee = getKeeperWithCompCodeAndCenter(keeper, compCode, center);
                if (employee.Count > 0)
                {
                    employees.Add(employee[0]);
                    var department = _context.Departments.Where(c => c.departmentId == employee[0].departmentId).First();
                    if (department != null && compCode == department.compCode && !departments.Contains(department) && department.center == center)
                    {
                        departments.Add(department);
                    }
                }
            }
            return departments;
        }

        [HttpGet("downloadReportByDepartment/{periodId}/{compCode}/{center}")]
        public IActionResult downloadReportByDepartment(int periodId, String compCode, String center)
        {
            var period = _context.Periods.Where(c => c.periodId == periodId).First();
            var tasks = _context.Tasks.Where(c => c.periodId == periodId).ToList();
            var company = _context.Companies.Where(c => c.compCode == compCode).First();
            var departments = getDepartmentByPeriodAndCenter(periodId, center, compCode, tasks);

            var stream = new MemoryStream();
            using (var xlPackage = new ExcelPackage(stream))
            {
                var worksheet = xlPackage.Workbook.Worksheets.Add("รายงานสรุปรายหน่วย");
                worksheet.View.ShowGridLines = false;
                var namedStyle = xlPackage.Workbook.Styles.CreateNamedStyle("HyperLink");
                namedStyle.Style.Font.UnderLine = true;
                namedStyle.Style.Font.Color.SetColor(Color.Blue);
                const int startRow = 12;
                var row = startRow;

                //Create Headers and format them
                worksheet.Cells["B1"].Value = "รายงานสรุปผลการตรวจยืนยันยอดลูกหนี้";
                using (var r = worksheet.Cells["B1:L1"])
                {
                    r.Merge = true;
                    r.Style.Font.Color.SetColor(Color.White);
                    r.Style.HorizontalAlignment = ExcelHorizontalAlignment.CenterContinuous;
                    r.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    r.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(23, 55, 93));
                }

                worksheet.Cells["B3"].Value = "สรุปผลการตรวจยืนยันยอดลูกหนี้ POP POS และอุปกรณ์เบียร์สด";
                worksheet.Cells["B3:D3"].Merge = true;
                worksheet.Cells["B3:D3"].Style.Font.Bold = true;

                worksheet.Cells["B5"].Value = "ระหว่างวันที่ " + period.startDate.ToString("dd-MM-yyyy") + " ถึง วันที่ " + period.endDate.ToString("dd-MM-yyyy");
                worksheet.Cells["B5:D5"].Merge = true;
                worksheet.Cells["B5:D5"].Style.Font.Bold = true;

                worksheet.Cells["B6"].Value = "บริษัท " + company.companyName + " จำกัด";
                worksheet.Cells["B6"].Style.Font.Bold = true;

                worksheet.Cells["B7"].Value = "ศูนย์ " + center;
                worksheet.Cells["B7"].Style.Font.Bold = true;

                worksheet.Cells["B10"].Value = "หน่วยขาย/โกดัง";
                worksheet.Cells["B10:B11"].Merge = true;

                worksheet.Cells["C10"].Value = "สถานะร้านค้า";
                worksheet.Cells["C10:C11"].Merge = true;

                worksheet.Cells["D10"].Value = "ถูกต้อง";
                worksheet.Cells["D10:E10"].Merge = true;

                worksheet.Cells["F10"].Value = "ไม่ถูกต้อง";
                worksheet.Cells["F10:G10"].Merge = true;

                worksheet.Cells["H10"].Value = "ลูกค้าไม่ประสงค์ยืนยันยอด";
                worksheet.Cells["H10:I10"].Merge = true;

                worksheet.Cells["J10"].Value = "ไม่ได้ยืนยัน";
                worksheet.Cells["J10:K10"].Merge = true;

                worksheet.Cells["L10"].Value = "Total จำนวนร้าน";
                worksheet.Cells["L10:L11"].Merge = true;

                worksheet.Cells["M10"].Value = "Total % ร้านทั้งหมด";
                worksheet.Cells["M10:M11"].Merge = true;

                worksheet.Cells["D11"].Value = "จำนวนร้าน";
                worksheet.Cells["E11"].Value = "% ร้านทั้งหมด";
                worksheet.Cells["F11"].Value = "จำนวนร้าน";
                worksheet.Cells["G11"].Value = "% ร้านทั้งหมด";
                worksheet.Cells["H11"].Value = "จำนวนร้าน";
                worksheet.Cells["I11"].Value = "% ร้านทั้งหมด";
                worksheet.Cells["J11"].Value = "จำนวนร้าน";
                worksheet.Cells["K11"].Value = "% ร้านทั้งหมด";

                worksheet.Column(2).Width = 30;
                worksheet.Column(3).Width = 20;
                worksheet.Column(4).Width = 20;
                worksheet.Column(5).Width = 20;
                worksheet.Column(6).Width = 20;
                worksheet.Column(7).Width = 20;
                worksheet.Column(8).Width = 20;
                worksheet.Column(9).Width = 20;
                worksheet.Column(10).Width = 20;
                worksheet.Column(11).Width = 20;
                worksheet.Column(12).Width = 20;
                worksheet.Column(13).Width = 20;

                worksheet.Cells["B10:M10"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["B10:M10"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(184, 204, 228));
                worksheet.Cells["B10:M10"].Style.Font.Bold = true;

                worksheet.Cells["B11:M11"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["B11:M11"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(184, 204, 228));
                worksheet.Cells["B11:M11"].Style.Font.Bold = true;

                worksheet.Cells["B10:M10"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                worksheet.Cells["B10:M10"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                worksheet.Cells["B10:M10"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                worksheet.Cells["B10:M10"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                worksheet.Cells["B10:M10"].Style.HorizontalAlignment = ExcelHorizontalAlignment.CenterContinuous;

                worksheet.Cells["B11:M11"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                worksheet.Cells["B11:M11"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                worksheet.Cells["B11:M11"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                worksheet.Cells["B11:M11"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                worksheet.Cells["B11:M11"].Style.HorizontalAlignment = ExcelHorizontalAlignment.CenterContinuous;

                int allCorrect = 0;
                int allIncorrect = 0;
                int allIgnore = 0;
                int allNotcheck = 0;
                int all = 0;

                foreach (var department in departments)
                {
                    int allOpen = 0;
                    int allClose = 0;
                    int allnotFound = 0;
                    int allGoOut = 0;
                    int allUnCheck = 0;

                    int openCorrect = 0;
                    int openIncorrect = 0;
                    int openIgnore = 0;
                    int openUnCheck = 0;

                    int closeCorrect = 0;
                    int closeIncorrect = 0;
                    int closeIgnore = 0;
                    int closeUnCheck = 0;

                    int goOutCorrect = 0;
                    int goOutIncorrect = 0;
                    int goOutIgnore = 0;
                    int goOutUnCheck = 0;

                    int notFoundCorrect = 0;
                    int notFoundIncorrect = 0;
                    int notFoundIgnore = 0;
                    int notFoundUnCheck = 0;

                    foreach (var task in tasks)
                    {
                        var shop = _context.Shops.Where(c => c.shopId == task.shopId).First();
                        var keeper = _context.Shopkeepers.Where(c => c.shopId == shop.shopId).ToList();
                        var employees = getKeeperWithCompCodeAndCenter(keeper, compCode, center);
                        if (employees.Count > 0 && employees[0].departmentId == department.departmentId)
                        {
                            var checkShop = _context.CheckShops.Where(c => c.taskID == task.taskID).First();
                            var checkShopSigned = _context.CheckShopSigneds.Where(c => c.checkShopId == checkShop.checkShopId && c.compCode == compCode).ToList();

                            if (checkShop.shopStatus == "open")
                            {
                                allOpen++;
                                if (checkShopSigned.Count > 0)
                                {
                                    if (checkShopSigned[0].confirmStatus == "ถูกต้อง" && checkShopSigned[0].customerSigned.Trim() != "")
                                    {
                                        openCorrect++;
                                    }
                                    else if (checkShopSigned[0].confirmStatus == "ไม่ถูกต้อง" && checkShopSigned[0].customerSigned.Trim() != "")
                                    {
                                        openIncorrect++;
                                    }
                                    else
                                    {
                                        openIgnore++;
                                    }
                                }
                                else
                                {
                                    openUnCheck++;
                                }
                            }
                            else if (checkShop.shopStatus == "close")
                            {
                                allClose++;
                                if (checkShopSigned.Count > 0)
                                {
                                    if (checkShopSigned[0].confirmStatus == "ถูกต้อง" && checkShopSigned[0].customerSigned.Trim() != "")
                                    {
                                        closeCorrect++;
                                    }
                                    else if (checkShopSigned[0].confirmStatus == "ไม่ถูกต้อง" && checkShopSigned[0].customerSigned.Trim() != "")
                                    {
                                        closeIncorrect++;
                                    }
                                    else
                                    {
                                        closeIgnore++;
                                    }
                                }
                                else
                                {
                                    closeUnCheck++;
                                }
                            }
                            else if (checkShop.shopStatus == "go-out-of-business")
                            {
                                allGoOut++;
                                if (checkShopSigned.Count > 0)
                                {
                                    if (checkShopSigned[0].confirmStatus == "ถูกต้อง" && checkShopSigned[0].customerSigned.Trim() != "")
                                    {
                                        goOutCorrect++;
                                    }
                                    else if (checkShopSigned[0].confirmStatus == "ไม่ถูกต้อง" && checkShopSigned[0].customerSigned.Trim() != "")
                                    {
                                        goOutIncorrect++;
                                    }
                                    else
                                    {
                                        goOutIgnore++;
                                    }
                                }
                                else
                                {
                                    goOutUnCheck++;
                                }
                            }
                            else if (checkShop.shopStatus == "not-found")
                            {
                                allnotFound++;
                                if (checkShopSigned.Count > 0)
                                {
                                    if (checkShopSigned[0].confirmStatus == "ถูกต้อง" && checkShopSigned[0].customerSigned.Trim() != "")
                                    {
                                        notFoundCorrect++;
                                    }
                                    else if (checkShopSigned[0].confirmStatus == "ไม่ถูกต้อง" && checkShopSigned[0].customerSigned.Trim() != "")
                                    {
                                        notFoundIncorrect++;
                                    }
                                    else
                                    {
                                        notFoundIgnore++;
                                    }
                                }
                                else
                                {
                                    notFoundUnCheck++;
                                }
                            }
                            else
                            {
                                allUnCheck++;
                            }
                        }
                    }

                    createCellCenter(worksheet, row, 2, department.departmentName);

                    //open
                    createCellCenter(worksheet, row, 3, "เปิด");
                    createCellCenter(worksheet, row, 4, openCorrect.ToString());
                    createCellCenter(worksheet, row, 5, (calPercentText(openCorrect, allOpen)).ToString("P", CultureInfo.InvariantCulture));
                    createCellCenter(worksheet, row, 6, openIncorrect.ToString());
                    createCellCenter(worksheet, row, 7, (calPercentText(openIncorrect, allOpen)).ToString("P", CultureInfo.InvariantCulture));
                    createCellCenter(worksheet, row, 8, openIgnore.ToString());
                    createCellCenter(worksheet, row, 9, (calPercentText(openIgnore, allOpen)).ToString("P", CultureInfo.InvariantCulture));
                    createCellCenter(worksheet, row, 10, openUnCheck.ToString());
                    createCellCenter(worksheet, row, 11, (calPercentText(openUnCheck, allOpen)).ToString("P", CultureInfo.InvariantCulture));
                    createCellCenter(worksheet, row, 12, allOpen.ToString());
                    createCellCenter(worksheet, row, 13, "100%");
                    row++;

                    //close
                    createCellCenter(worksheet, row, 3, "ปิด");
                    createCellCenter(worksheet, row, 4, closeCorrect.ToString());
                    createCellCenter(worksheet, row, 5, (calPercentText(closeCorrect, allClose)).ToString("P", CultureInfo.InvariantCulture));
                    createCellCenter(worksheet, row, 6, closeIncorrect.ToString());
                    createCellCenter(worksheet, row, 7, (calPercentText(closeIncorrect, allClose)).ToString("P", CultureInfo.InvariantCulture));
                    createCellCenter(worksheet, row, 8, closeIgnore.ToString());
                    createCellCenter(worksheet, row, 9, (calPercentText(closeIgnore, allClose)).ToString("P", CultureInfo.InvariantCulture));
                    createCellCenter(worksheet, row, 10, closeUnCheck.ToString());
                    createCellCenter(worksheet, row, 11, (calPercentText(closeUnCheck, allClose)).ToString("P", CultureInfo.InvariantCulture));
                    createCellCenter(worksheet, row, 12, allClose.ToString());
                    createCellCenter(worksheet, row, 13, "100%");
                    row++;

                    //go out
                    createCellCenter(worksheet, row, 3, "เลิกกิจการ");
                    createCellCenter(worksheet, row, 4, goOutCorrect.ToString());
                    createCellCenter(worksheet, row, 5, (calPercentText(goOutCorrect, allGoOut)).ToString("P", CultureInfo.InvariantCulture));
                    createCellCenter(worksheet, row, 6, goOutIncorrect.ToString());
                    createCellCenter(worksheet, row, 7, (calPercentText(goOutIncorrect, allGoOut)).ToString("P", CultureInfo.InvariantCulture));
                    createCellCenter(worksheet, row, 8, goOutIgnore.ToString());
                    createCellCenter(worksheet, row, 9, (calPercentText(goOutIgnore, allGoOut)).ToString("P", CultureInfo.InvariantCulture));
                    createCellCenter(worksheet, row, 10, goOutUnCheck.ToString());
                    createCellCenter(worksheet, row, 11, (calPercentText(goOutUnCheck, allGoOut)).ToString("P", CultureInfo.InvariantCulture));
                    createCellCenter(worksheet, row, 12, allGoOut.ToString());
                    createCellCenter(worksheet, row, 13, "100%");
                    row++;

                    //Not found
                    createCellCenter(worksheet, row, 3, "หาร้านไม่พบ");
                    createCellCenter(worksheet, row, 4, notFoundCorrect.ToString());
                    createCellCenter(worksheet, row, 5, (calPercentText(notFoundCorrect, allnotFound)).ToString("P", CultureInfo.InvariantCulture));
                    createCellCenter(worksheet, row, 6, notFoundIncorrect.ToString());
                    createCellCenter(worksheet, row, 7, (calPercentText(notFoundIncorrect, allnotFound)).ToString("P", CultureInfo.InvariantCulture));
                    createCellCenter(worksheet, row, 8, notFoundIgnore.ToString());
                    createCellCenter(worksheet, row, 9, (calPercentText(notFoundIgnore, allnotFound)).ToString("P", CultureInfo.InvariantCulture));
                    createCellCenter(worksheet, row, 10, notFoundUnCheck.ToString());
                    createCellCenter(worksheet, row, 11, (calPercentText(notFoundUnCheck, allnotFound)).ToString("P", CultureInfo.InvariantCulture));
                    createCellCenter(worksheet, row, 12, allnotFound.ToString());
                    createCellCenter(worksheet, row, 13, "100%");
                    row++;

                    //uncheck
                    createCellCenter(worksheet, row, 3, "ไม่ได้เข้าร้าน");
                    createCellCenter(worksheet, row, 4, "0");
                    createCellCenter(worksheet, row, 5, (0).ToString("P", CultureInfo.InvariantCulture));
                    createCellCenter(worksheet, row, 6, "0");
                    createCellCenter(worksheet, row, 7, (0).ToString("P", CultureInfo.InvariantCulture));
                    createCellCenter(worksheet, row, 8, "0");
                    createCellCenter(worksheet, row, 9, (0).ToString("P", CultureInfo.InvariantCulture));
                    createCellCenter(worksheet, row, 10, allUnCheck.ToString());
                    createCellCenter(worksheet, row, 11, (calPercentText(allUnCheck, allUnCheck)).ToString("P", CultureInfo.InvariantCulture));
                    createCellCenter(worksheet, row, 12, allUnCheck.ToString());
                    createCellCenter(worksheet, row, 13, "100%");
                    row++;

                    worksheet.Cells[row - 5, 2, row - 1, 2].Merge = true;

                    allCorrect += openCorrect + closeCorrect + goOutCorrect + notFoundCorrect;
                    allIncorrect += openIncorrect + closeIncorrect + goOutIncorrect + notFoundIncorrect;
                    allIgnore += openIgnore + closeIgnore + goOutIgnore + notFoundIgnore;
                    allNotcheck += openUnCheck + closeUnCheck + goOutUnCheck + notFoundUnCheck + allUnCheck;
                    all += allOpen + allClose + allGoOut + allnotFound + allUnCheck;
                }

                //Grand Total
                createCellCenter(worksheet, row, 2, "Grand Total");
                createCellCenter(worksheet, row, 4, allCorrect.ToString());
                createCellCenter(worksheet, row, 5, (calPercentText(allCorrect, all)).ToString("P", CultureInfo.InvariantCulture));
                createCellCenter(worksheet, row, 6, allIncorrect.ToString());
                createCellCenter(worksheet, row, 7, (calPercentText(allIncorrect, all)).ToString("P", CultureInfo.InvariantCulture));
                createCellCenter(worksheet, row, 8, allIgnore.ToString());
                createCellCenter(worksheet, row, 9, (calPercentText(allIgnore, all)).ToString("P", CultureInfo.InvariantCulture));
                createCellCenter(worksheet, row, 10, allNotcheck.ToString());
                createCellCenter(worksheet, row, 11, (calPercentText(allNotcheck, all)).ToString("P", CultureInfo.InvariantCulture));
                createCellCenter(worksheet, row, 12, all.ToString());
                createCellCenter(worksheet, row, 13, "100%");

                worksheet.Cells[row, 2, row, 3].Merge = true;
                worksheet.Cells[row, 2, row, 13].Style.Font.Bold = true;
                worksheet.Cells[row, 2, row, 13].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells[row, 2, row, 13].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(209, 209, 209));

                var tableBody = worksheet.Cells[startRow, 2, row, 13];
                tableBody.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                tableBody.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                tableBody.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                tableBody.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                tableBody.Style.HorizontalAlignment = ExcelHorizontalAlignment.CenterContinuous;

                // set some core property values
                xlPackage.Workbook.Properties.Title = "MAI ยืนยันยอดลูกหนี้";
                xlPackage.Workbook.Properties.Author = "MAI admin";
                xlPackage.Workbook.Properties.Subject = "Keeper report";
                // save the new spreadsheet
                xlPackage.Save();
                // Response.Clear();
            }
            stream.Position = 0;
            return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "รายงานสรุปผลการตรวจรายหน่วย-ศูนย์" + center + ".xlsx");
        }

        [HttpGet("downloadReportByEmployee/{periodId}/{compCode}/{center}")]
        public IActionResult downloadReportByEmployee(int periodId, String compCode, String center)
        {
            var period = _context.Periods.Where(c => c.periodId == periodId).First();
            var tasks = _context.Tasks.Where(c => c.periodId == periodId).ToList();
            var company = _context.Companies.Where(c => c.compCode == compCode).First();
            var allEmployee = getEmployeeByPeriodAndCenter(periodId, center, compCode, tasks);

            var stream = new MemoryStream();
            using (var xlPackage = new ExcelPackage(stream))
            {
                var worksheet = xlPackage.Workbook.Worksheets.Add("รายงานสรุปรายพนักงาน");
                worksheet.View.ShowGridLines = false;
                var namedStyle = xlPackage.Workbook.Styles.CreateNamedStyle("HyperLink");
                namedStyle.Style.Font.UnderLine = true;
                namedStyle.Style.Font.Color.SetColor(Color.Blue);
                const int startRow = 12;
                var row = startRow;

                //Create Headers and format them
                worksheet.Cells["B1"].Value = "รายงานสรุปผลการตรวจยืนยันยอดลูกหนี้";
                using (var r = worksheet.Cells["B1:L1"])
                {
                    r.Merge = true;
                    r.Style.Font.Color.SetColor(Color.White);
                    r.Style.HorizontalAlignment = ExcelHorizontalAlignment.CenterContinuous;
                    r.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    r.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(23, 55, 93));
                }

                worksheet.Cells["B3"].Value = "สรุปผลการตรวจยืนยันยอดลูกหนี้ POP POS และอุปกรณ์เบียร์สด";
                worksheet.Cells["B3:D3"].Merge = true;
                worksheet.Cells["B3:D3"].Style.Font.Bold = true;

                worksheet.Cells["B5"].Value = "ระหว่างวันที่ " + period.startDate.ToString("dd-MM-yyyy") + " ถึง วันที่ " + period.endDate.ToString("dd-MM-yyyy");
                worksheet.Cells["B5:D5"].Merge = true;
                worksheet.Cells["B5:D5"].Style.Font.Bold = true;

                worksheet.Cells["B6"].Value = "บริษัท " + company.companyName + " จำกัด";
                worksheet.Cells["B6"].Style.Font.Bold = true;

                worksheet.Cells["B7"].Value = "ศูนย์ " + center;
                worksheet.Cells["B7"].Style.Font.Bold = true;

                worksheet.Cells["B10"].Value = "ชื่อพนักงานผู้ดูแลปัจจุบัน";
                worksheet.Cells["B10:B11"].Merge = true;

                worksheet.Cells["C10"].Value = "สถานะร้านค้า";
                worksheet.Cells["C10:C11"].Merge = true;

                worksheet.Cells["D10"].Value = "ถูกต้อง";
                worksheet.Cells["D10:E10"].Merge = true;

                worksheet.Cells["F10"].Value = "ไม่ถูกต้อง";
                worksheet.Cells["F10:G10"].Merge = true;

                worksheet.Cells["H10"].Value = "ลูกค้าไม่ประสงค์ยืนยันยอด";
                worksheet.Cells["H10:I10"].Merge = true;

                worksheet.Cells["J10"].Value = "ไม่ได้ยืนยัน";
                worksheet.Cells["J10:K10"].Merge = true;

                worksheet.Cells["L10"].Value = "Total จำนวนร้าน";
                worksheet.Cells["L10:L11"].Merge = true;

                worksheet.Cells["M10"].Value = "Total % ร้านทั้งหมด";
                worksheet.Cells["M10:M11"].Merge = true;

                worksheet.Cells["D11"].Value = "จำนวนร้าน";
                worksheet.Cells["E11"].Value = "% ร้านทั้งหมด";
                worksheet.Cells["F11"].Value = "จำนวนร้าน";
                worksheet.Cells["G11"].Value = "% ร้านทั้งหมด";
                worksheet.Cells["H11"].Value = "จำนวนร้าน";
                worksheet.Cells["I11"].Value = "% ร้านทั้งหมด";
                worksheet.Cells["J11"].Value = "จำนวนร้าน";
                worksheet.Cells["K11"].Value = "% ร้านทั้งหมด";

                worksheet.Column(2).Width = 30;
                worksheet.Column(3).Width = 20;
                worksheet.Column(4).Width = 20;
                worksheet.Column(5).Width = 20;
                worksheet.Column(6).Width = 20;
                worksheet.Column(7).Width = 20;
                worksheet.Column(8).Width = 20;
                worksheet.Column(9).Width = 20;
                worksheet.Column(10).Width = 20;
                worksheet.Column(11).Width = 20;
                worksheet.Column(12).Width = 20;
                worksheet.Column(13).Width = 20;

                worksheet.Cells["B10:M10"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["B10:M10"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(184, 204, 228));
                worksheet.Cells["B10:M10"].Style.Font.Bold = true;

                worksheet.Cells["B11:M11"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["B11:M11"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(184, 204, 228));
                worksheet.Cells["B11:M11"].Style.Font.Bold = true;

                worksheet.Cells["B10:M10"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                worksheet.Cells["B10:M10"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                worksheet.Cells["B10:M10"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                worksheet.Cells["B10:M10"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                worksheet.Cells["B10:M10"].Style.HorizontalAlignment = ExcelHorizontalAlignment.CenterContinuous;

                worksheet.Cells["B11:M11"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                worksheet.Cells["B11:M11"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                worksheet.Cells["B11:M11"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                worksheet.Cells["B11:M11"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                worksheet.Cells["B11:M11"].Style.HorizontalAlignment = ExcelHorizontalAlignment.CenterContinuous;

                int allCorrect = 0;
                int allIncorrect = 0;
                int allIgnore = 0;
                int allNotcheck = 0;
                int all = 0;

                foreach (var employee in allEmployee)
                {
                    int allOpen = 0;
                    int allClose = 0;
                    int allnotFound = 0;
                    int allGoOut = 0;
                    int allUnCheck = 0;

                    int openCorrect = 0;
                    int openIncorrect = 0;
                    int openIgnore = 0;
                    int openUnCheck = 0;

                    int closeCorrect = 0;
                    int closeIncorrect = 0;
                    int closeIgnore = 0;
                    int closeUnCheck = 0;

                    int goOutCorrect = 0;
                    int goOutIncorrect = 0;
                    int goOutIgnore = 0;
                    int goOutUnCheck = 0;

                    int notFoundCorrect = 0;
                    int notFoundIncorrect = 0;
                    int notFoundIgnore = 0;
                    int notFoundUnCheck = 0;

                    foreach (var task in tasks)
                    {
                        var shop = _context.Shops.Where(c => c.shopId == task.shopId).First();
                        var keeper = _context.Shopkeepers.Where(c => c.shopId == shop.shopId).ToList();
                        var employees = getKeeperWithCompCodeAndCenter(keeper, compCode, center);
                        if (employees.Count > 0 && employees[0].empId == employee.empId)
                        {
                            var checkShop = _context.CheckShops.Where(c => c.taskID == task.taskID).First();
                            var checkShopSigned = _context.CheckShopSigneds.Where(c => c.checkShopId == checkShop.checkShopId && c.compCode == compCode).ToList();

                            if (checkShop.shopStatus == "open")
                            {
                                allOpen++;
                                if (checkShopSigned.Count > 0)
                                {
                                    if (checkShopSigned[0].confirmStatus == "ถูกต้อง" && checkShopSigned[0].customerSigned.Trim() != "")
                                    {
                                        openCorrect++;
                                    }
                                    else if (checkShopSigned[0].confirmStatus == "ไม่ถูกต้อง" && checkShopSigned[0].customerSigned.Trim() != "")
                                    {
                                        openIncorrect++;
                                    }
                                    else
                                    {
                                        openIgnore++;
                                    }
                                }
                                else
                                {
                                    openUnCheck++;
                                }
                            }
                            else if (checkShop.shopStatus == "close")
                            {
                                allClose++;
                                if (checkShopSigned.Count > 0)
                                {
                                    if (checkShopSigned[0].confirmStatus == "ถูกต้อง" && checkShopSigned[0].customerSigned.Trim() != "")
                                    {
                                        closeCorrect++;
                                    }
                                    else if (checkShopSigned[0].confirmStatus == "ไม่ถูกต้อง" && checkShopSigned[0].customerSigned.Trim() != "")
                                    {
                                        closeIncorrect++;
                                    }
                                    else
                                    {
                                        closeIgnore++;
                                    }
                                }
                                else
                                {
                                    closeUnCheck++;
                                }
                            }
                            else if (checkShop.shopStatus == "go-out-of-business")
                            {
                                allGoOut++;
                                if (checkShopSigned.Count > 0)
                                {
                                    if (checkShopSigned[0].confirmStatus == "ถูกต้อง" && checkShopSigned[0].customerSigned.Trim() != "")
                                    {
                                        goOutCorrect++;
                                    }
                                    else if (checkShopSigned[0].confirmStatus == "ไม่ถูกต้อง" && checkShopSigned[0].customerSigned.Trim() != "")
                                    {
                                        goOutIncorrect++;
                                    }
                                    else
                                    {
                                        goOutIgnore++;
                                    }
                                }
                                else
                                {
                                    goOutUnCheck++;
                                }
                            }
                            else if (checkShop.shopStatus == "not-found")
                            {
                                allnotFound++;
                                if (checkShopSigned.Count > 0)
                                {
                                    if (checkShopSigned[0].confirmStatus == "ถูกต้อง" && checkShopSigned[0].customerSigned.Trim() != "")
                                    {
                                        notFoundCorrect++;
                                    }
                                    else if (checkShopSigned[0].confirmStatus == "ไม่ถูกต้อง" && checkShopSigned[0].customerSigned.Trim() != "")
                                    {
                                        notFoundIncorrect++;
                                    }
                                    else
                                    {
                                        notFoundIgnore++;
                                    }
                                }
                                else
                                {
                                    notFoundUnCheck++;
                                }
                            }
                            else
                            {
                                allUnCheck++;
                            }
                        }
                    }

                    createCellCenter(worksheet, row, 2, employee.empName);

                    //open
                    createCellCenter(worksheet, row, 3, "เปิด");
                    createCellCenter(worksheet, row, 4, openCorrect.ToString());
                    createCellCenter(worksheet, row, 5, (calPercentText(openCorrect, allOpen)).ToString("P", CultureInfo.InvariantCulture));
                    createCellCenter(worksheet, row, 6, openIncorrect.ToString());
                    createCellCenter(worksheet, row, 7, (calPercentText(openIncorrect, allOpen)).ToString("P", CultureInfo.InvariantCulture));
                    createCellCenter(worksheet, row, 8, openIgnore.ToString());
                    createCellCenter(worksheet, row, 9, (calPercentText(openIgnore, allOpen)).ToString("P", CultureInfo.InvariantCulture));
                    createCellCenter(worksheet, row, 10, openUnCheck.ToString());
                    createCellCenter(worksheet, row, 11, (calPercentText(openUnCheck, allOpen)).ToString("P", CultureInfo.InvariantCulture));
                    createCellCenter(worksheet, row, 12, allOpen.ToString());
                    createCellCenter(worksheet, row, 13, "100%");
                    row++;

                    //close
                    createCellCenter(worksheet, row, 3, "ปิด");
                    createCellCenter(worksheet, row, 4, closeCorrect.ToString());
                    createCellCenter(worksheet, row, 5, (calPercentText(closeCorrect, allClose)).ToString("P", CultureInfo.InvariantCulture));
                    createCellCenter(worksheet, row, 6, closeIncorrect.ToString());
                    createCellCenter(worksheet, row, 7, (calPercentText(closeIncorrect, allClose)).ToString("P", CultureInfo.InvariantCulture));
                    createCellCenter(worksheet, row, 8, closeIgnore.ToString());
                    createCellCenter(worksheet, row, 9, (calPercentText(closeIgnore, allClose)).ToString("P", CultureInfo.InvariantCulture));
                    createCellCenter(worksheet, row, 10, closeUnCheck.ToString());
                    createCellCenter(worksheet, row, 11, (calPercentText(closeUnCheck, allClose)).ToString("P", CultureInfo.InvariantCulture));
                    createCellCenter(worksheet, row, 12, allClose.ToString());
                    createCellCenter(worksheet, row, 13, "100%");
                    row++;

                    //go out
                    createCellCenter(worksheet, row, 3, "เลิกกิจการ");
                    createCellCenter(worksheet, row, 4, goOutCorrect.ToString());
                    createCellCenter(worksheet, row, 5, (calPercentText(goOutCorrect, allGoOut)).ToString("P", CultureInfo.InvariantCulture));
                    createCellCenter(worksheet, row, 6, goOutIncorrect.ToString());
                    createCellCenter(worksheet, row, 7, (calPercentText(goOutIncorrect, allGoOut)).ToString("P", CultureInfo.InvariantCulture));
                    createCellCenter(worksheet, row, 8, goOutIgnore.ToString());
                    createCellCenter(worksheet, row, 9, (calPercentText(goOutIgnore, allGoOut)).ToString("P", CultureInfo.InvariantCulture));
                    createCellCenter(worksheet, row, 10, goOutUnCheck.ToString());
                    createCellCenter(worksheet, row, 11, (calPercentText(goOutUnCheck, allGoOut)).ToString("P", CultureInfo.InvariantCulture));
                    createCellCenter(worksheet, row, 12, allGoOut.ToString());
                    createCellCenter(worksheet, row, 13, "100%");
                    row++;

                    //Not found
                    createCellCenter(worksheet, row, 3, "หาร้านไม่พบ");
                    createCellCenter(worksheet, row, 4, notFoundCorrect.ToString());
                    createCellCenter(worksheet, row, 5, (calPercentText(notFoundCorrect, allnotFound)).ToString("P", CultureInfo.InvariantCulture));
                    createCellCenter(worksheet, row, 6, notFoundIncorrect.ToString());
                    createCellCenter(worksheet, row, 7, (calPercentText(notFoundIncorrect, allnotFound)).ToString("P", CultureInfo.InvariantCulture));
                    createCellCenter(worksheet, row, 8, notFoundIgnore.ToString());
                    createCellCenter(worksheet, row, 9, (calPercentText(notFoundIgnore, allnotFound)).ToString("P", CultureInfo.InvariantCulture));
                    createCellCenter(worksheet, row, 10, notFoundUnCheck.ToString());
                    createCellCenter(worksheet, row, 11, (calPercentText(notFoundUnCheck, allnotFound)).ToString("P", CultureInfo.InvariantCulture));
                    createCellCenter(worksheet, row, 12, allnotFound.ToString());
                    createCellCenter(worksheet, row, 13, "100%");
                    row++;

                    //uncheck
                    createCellCenter(worksheet, row, 3, "ไม่ได้เข้าร้าน");
                    createCellCenter(worksheet, row, 4, "0");
                    createCellCenter(worksheet, row, 5, (0).ToString("P", CultureInfo.InvariantCulture));
                    createCellCenter(worksheet, row, 6, "0");
                    createCellCenter(worksheet, row, 7, (0).ToString("P", CultureInfo.InvariantCulture));
                    createCellCenter(worksheet, row, 8, "0");
                    createCellCenter(worksheet, row, 9, (0).ToString("P", CultureInfo.InvariantCulture));
                    createCellCenter(worksheet, row, 10, allUnCheck.ToString());
                    createCellCenter(worksheet, row, 11, (calPercentText(allUnCheck, allUnCheck)).ToString("P", CultureInfo.InvariantCulture));
                    createCellCenter(worksheet, row, 12, allUnCheck.ToString());
                    createCellCenter(worksheet, row, 13, "100%");
                    row++;

                    worksheet.Cells[row - 5, 2, row - 1, 2].Merge = true;

                    allCorrect += openCorrect + closeCorrect + goOutCorrect + notFoundCorrect;
                    allIncorrect += openIncorrect + closeIncorrect + goOutIncorrect + notFoundIncorrect;
                    allIgnore += openIgnore + closeIgnore + goOutIgnore + notFoundIgnore;
                    allNotcheck += openUnCheck + closeUnCheck + goOutUnCheck + notFoundUnCheck + allUnCheck;
                    all += allOpen + allClose + allGoOut + allnotFound + allUnCheck;
                }

                //Grand Total
                createCellCenter(worksheet, row, 2, "Grand Total");
                createCellCenter(worksheet, row, 4, allCorrect.ToString());
                createCellCenter(worksheet, row, 5, (calPercentText(allCorrect, all)).ToString("P", CultureInfo.InvariantCulture));
                createCellCenter(worksheet, row, 6, allIncorrect.ToString());
                createCellCenter(worksheet, row, 7, (calPercentText(allIncorrect, all)).ToString("P", CultureInfo.InvariantCulture));
                createCellCenter(worksheet, row, 8, allIgnore.ToString());
                createCellCenter(worksheet, row, 9, (calPercentText(allIgnore, all)).ToString("P", CultureInfo.InvariantCulture));
                createCellCenter(worksheet, row, 10, allNotcheck.ToString());
                createCellCenter(worksheet, row, 11, (calPercentText(allNotcheck, all)).ToString("P", CultureInfo.InvariantCulture));
                createCellCenter(worksheet, row, 12, all.ToString());
                createCellCenter(worksheet, row, 13, "100%");

                worksheet.Cells[row, 2, row, 3].Merge = true;
                worksheet.Cells[row, 2, row, 13].Style.Font.Bold = true;
                worksheet.Cells[row, 2, row, 13].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells[row, 2, row, 13].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(209, 209, 209));

                var tableBody = worksheet.Cells[startRow, 2, row, 13];
                tableBody.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                tableBody.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                tableBody.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                tableBody.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                tableBody.Style.HorizontalAlignment = ExcelHorizontalAlignment.CenterContinuous;

                // set some core property values
                xlPackage.Workbook.Properties.Title = "MAI ยืนยันยอดลูกหนี้";
                xlPackage.Workbook.Properties.Author = "MAI admin";
                xlPackage.Workbook.Properties.Subject = "Keeper report";
                // save the new spreadsheet
                xlPackage.Save();
                // Response.Clear();
            }
            stream.Position = 0;
            return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "รายงานสรุปผลการตรวจรายพนักงาน-ศูนย์" + center + ".xlsx");
        }

        private List<Employee> getEmployeeByPeriodAndCenter(int periodId, String center, String compCode, List<AssetTask> tasks)
        {
            List<Employee> employees = new();
            List<Department> departments = new();
            foreach (var task in tasks)
            {
                var shop = _context.Shops.Where(c => c.shopId == task.shopId).First();
                List<Shopkeeper> keeper = _context.Shopkeepers.Where(c => c.shopId == shop.shopId && c.isNew == true).ToList();
                if (keeper.Count == 0)
                {
                    keeper = _context.Shopkeepers.Where(c => c.shopId == shop.shopId).ToList();
                }
                var employee = getCurrentKeeperWithCompCodeAndCenter(keeper, compCode, center);
                if (employee.Count > 0 && !employees.Contains(employee[0]))
                {
                    employees.Add(employee[0]);
                }
            }
            return employees;
        }

        private List<Employee> getCurrentKeeperWithCompCodeAndCenter(List<Shopkeeper> shopkeepers, String compCode, String center)
        {
            List<Employee> employees = new();
            foreach (var shop in shopkeepers)
            {
                var employee = _context.Employees.Where(c => c.empId == shop.empId && c.compCode == compCode).ToList();
                if (employee.Count > 0)
                {
                    var department = _context.Departments.Where(c => c.departmentId == employee[0].departmentId && c.center.Trim() == center.Trim()).ToList();
                    if (department.Count > 0)
                    {
                        employees.Add(employee[0]);
                    }
                }
            }
            return employees;
        }

        [HttpGet("getDepartmentByCompCode/{compCode}")]
        public async Task<ActionResult<DepartmentResponse>> getDepartmentByCompCode(String compCode)
        {
            var departments = _context.Departments.Where(c => c.compCode == compCode).ToList();
            if (departments.Count > 0)
            {
                return await Task.FromResult(new DepartmentResponse
                {
                    status = "success",
                    message = "",
                    departments = departments
                });
            }
            else
            {
                return await Task.FromResult(new DepartmentResponse
                {
                    status = "error",
                    message = "ไม่พบข้อมูล"
                });
            }
        }

        private List<Employee> getEmployeeByCompanyAndTask(int periodId, String compCode, List<AssetTask> tasks)
        {
            List<Employee> employees = new();
            foreach (var task in tasks)
            {
                var shop = _context.Shops.Where(c => c.shopId == task.shopId).First();
                List<Shopkeeper> keeper = _context.Shopkeepers.Where(c => c.shopId == shop.shopId && c.isNew == true).ToList();
                if (keeper.Count == 0)
                {
                    keeper = _context.Shopkeepers.Where(c => c.shopId == shop.shopId).ToList();
                }
                var employee = getCurrentKeeperWithCompCode(keeper, compCode);
                if (employee.Count > 0 && !employees.Contains(employee[0]))
                {
                    employees.Add(employee[0]);
                }
            }
            return employees;
        }

        private List<Employee> getCurrentKeeperWithCompCode(List<Shopkeeper> shopkeepers, String compCode)
        {
            List<Employee> employees = new();
            foreach (var shop in shopkeepers)
            {
                var employee = _context.Employees.Where(c => c.empId == shop.empId && c.compCode == compCode).ToList();
                if (employee.Count > 0)
                {
                    employees.Add(employee[0]);
                }
            }
            return employees;
        }


        [HttpGet("downloadDiffReport/{periodId}/{region}/{compCode}")]
        public IActionResult downloadDiffReport(int periodId, String region, String compCode)
        {
            try
            {
                var period = _context.Periods.Where(c => c.periodId == periodId).First();
                var tasks = _context.Tasks.Where(c => c.periodId == periodId).ToList();
                var company = _context.Companies.Where(c => c.compCode == compCode).First();
                var allEmployee = getEmployeeByCompanyAndTask(periodId, compCode, tasks);

                var stream = new MemoryStream();
                using (var xlPackage = new ExcelPackage(stream))
                {
                    var worksheet = xlPackage.Workbook.Worksheets.Add("รายงานผลต่าง");
                    worksheet.View.ShowGridLines = false;
                    var namedStyle = xlPackage.Workbook.Styles.CreateNamedStyle("HyperLink");
                    namedStyle.Style.Font.UnderLine = true;
                    namedStyle.Style.Font.Color.SetColor(Color.Blue);
                    const int startRow = 11;
                    var row = startRow;

                    //Create Headers and format them
                    worksheet.Cells["B1"].Value = "สรุปผลต่างยอดยืม POP POS / อุปกรณ์เบียร์สด ตามบัญชีเทียบตามตรวจนับ";
                    using (var r = worksheet.Cells["B1:H1"])
                    {
                        r.Merge = true;
                        r.Style.Font.Color.SetColor(Color.White);
                        r.Style.HorizontalAlignment = ExcelHorizontalAlignment.CenterContinuous;
                        r.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        r.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(23, 55, 93));
                    }

                    worksheet.Cells["B3"].Value = " ";
                    worksheet.Cells["B5"].Value = "ระหว่างวันที่ " + period.startDate.ToString("dd-MM-yyyy") + " ถึง วันที่ " + period.endDate.ToString("dd-MM-yyyy");
                    worksheet.Cells["B6"].Value = "บริษัท " + company.companyName + " จำกัด";

                    worksheet.Cells["B10"].Value = "ผู้ดูแลตามบัญชี";
                    worksheet.Cells["C10"].Value = "ชื่อลูกค้า";
                    worksheet.Cells["D10"].Value = "ผลการยืนยัน";
                    worksheet.Cells["E10"].Value = "รหัส";
                    worksheet.Cells["F10"].Value = "ชื่อ POP/POS/อุปกรณ์เบียร์สด";
                    worksheet.Cells["G10"].Value = "ยอดตามบัญชี";
                    worksheet.Cells["H10"].Value = "รวมตรวจนับ";
                    worksheet.Cells["I10"].Value = "ผลต่าง(ขาด)เกิน";

                    worksheet.Column(2).Width = 40;
                    worksheet.Column(3).Width = 70;
                    worksheet.Column(4).Width = 20;
                    worksheet.Column(5).Width = 20;
                    worksheet.Column(6).Width = 70;
                    worksheet.Column(7).Width = 20;
                    worksheet.Column(8).Width = 20;
                    worksheet.Column(9).Width = 20;

                    worksheet.Cells["B10:I10"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells["B10:I10"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(184, 204, 228));
                    worksheet.Cells["B10:I10"].Style.Font.Bold = true;

                    var tableHeader = worksheet.Cells["B10:I10"];
                    tableHeader.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    tableHeader.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    tableHeader.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    tableHeader.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    tableHeader.Style.HorizontalAlignment = ExcelHorizontalAlignment.CenterContinuous;

                    int sumAccount = 0;
                    int sumAmount = 0;

                    foreach (var employee in allEmployee)
                    {
                        List<AssetTask> currectTask = new();

                        foreach (var task in tasks)
                        {
                            var shop = _context.Shops.Where(c => c.shopId == task.shopId).First();
                            var keeper = _context.Shopkeepers.Where(c => c.shopId == shop.shopId && c.empId == employee.empId).ToList();
                            if (keeper.Count > 0)
                            {
                                var isHaveNewKeeper = false;
                                if (!keeper[0].isNew)
                                {
                                    isHaveNewKeeper = isHasNewKeeper(employee.compCode, shop.shopId);
                                }

                                if (!isHaveNewKeeper)
                                {
                                    currectTask.Add(task);
                                }
                            }
                        }

                        if (currectTask.Count > 0)
                        {
                            int beginRow = row;
                            createCell(worksheet, row, 2, employee.empName);

                            var account = 0;
                            var amount = 0;

                            foreach (var task in currectTask)
                            {
                                int startRows = row;
                                createCell(worksheet, row, 3, task.taskName);

                                var checkShop = _context.CheckShops.Where(c => c.taskID == task.taskID).First();
                                var checkShopSigneds = _context.CheckShopSigneds.Where(c => c.checkShopId == checkShop.checkShopId && c.compCode == employee.compCode).ToList();

                                if (checkShopSigneds.Count > 0)
                                {
                                    foreach (var checkSigned in checkShopSigneds)
                                    {
                                        createCellCenter(worksheet, row, 4, checkSigned.confirmStatus);

                                        List<CheckShopTransaction> checkShopTransaction;
                                        if (checkSigned.assetTypeID == 1 || checkSigned.assetTypeID == 2)
                                        {
                                            checkShopTransaction = _context.CheckShopTransactions.Where(c => c.checkShopId == checkShop.checkShopId && c.compCode == employee.compCode && (c.assetTypeID == 1 || c.assetTypeID == 2)).ToList();
                                        }
                                        else
                                        {
                                            checkShopTransaction = _context.CheckShopTransactions.Where(c => c.checkShopId == checkShop.checkShopId && c.compCode == employee.compCode && (c.assetTypeID == 3 || c.assetTypeID == 4)).ToList();
                                        }
                                        if (checkShopTransaction.Count > 0)
                                        {
                                            var currentRow = row;
                                            foreach (var transaction in checkShopTransaction)
                                            {
                                                var asset = _context.Assets.Where(c => c.assetId == transaction.assetId).First();
                                                var different = "";
                                                if (transaction.accountBalance > (transaction.avaliable + transaction.damaged))
                                                {
                                                    different = "(" + (transaction.accountBalance - (transaction.avaliable + transaction.damaged)).ToString() + ")";
                                                }
                                                else
                                                {
                                                    different = ((transaction.avaliable + transaction.damaged) - transaction.accountBalance).ToString();
                                                }

                                                account += transaction.accountBalance;
                                                amount += (transaction.avaliable + transaction.damaged);

                                                createCell(worksheet, currentRow, 5, asset.assetCode);
                                                createCell(worksheet, currentRow, 6, asset.assetName);
                                                createCellCenter(worksheet, currentRow, 7, transaction.accountBalance.ToString());
                                                createCellCenter(worksheet, currentRow, 8, (transaction.avaliable + transaction.damaged).ToString());
                                                createCellCenter(worksheet, currentRow, 9, different);
                                                currentRow++;
                                            }
                                            worksheet.Cells[row, 4, row + checkShopTransaction.Count - 1, 4].Merge = true;
                                            row += checkShopTransaction.Count;
                                        }
                                        else
                                        {
                                            createCellCenter(worksheet, row, 5, "-");
                                            createCellCenter(worksheet, row, 6, "-");
                                            createCellCenter(worksheet, row, 7, "0");
                                            createCellCenter(worksheet, row, 8, "0");
                                            createCellCenter(worksheet, row, 9, "0");
                                            worksheet.Cells[row, 4, row - 1, 4].Merge = true;
                                            row++;
                                        }
                                    }
                                }
                                else
                                {
                                    createCellCenter(worksheet, row, 4, "-");

                                    var checkShopTransaction = _context.CheckShopTransactions.Where(c => c.checkShopId == checkShop.checkShopId && c.compCode == employee.compCode).ToList();
                                    if (checkShopTransaction.Count > 0)
                                    {
                                        var currentRow = row;
                                        foreach (var transaction in checkShopTransaction)
                                        {
                                            var asset = _context.Assets.Where(c => c.assetId == transaction.assetId).First();
                                            var different = "";
                                            if (transaction.accountBalance > (transaction.avaliable + transaction.damaged))
                                            {
                                                different = "(" + (transaction.accountBalance - (transaction.avaliable + transaction.damaged)).ToString() + ")";
                                            }
                                            else
                                            {
                                                different = ((transaction.avaliable + transaction.damaged) - transaction.accountBalance).ToString();
                                            }

                                            account += transaction.accountBalance;
                                            amount += (transaction.avaliable + transaction.damaged);

                                            createCell(worksheet, currentRow, 5, asset.assetCode);
                                            createCell(worksheet, currentRow, 6, asset.assetName);
                                            createCellCenter(worksheet, currentRow, 7, transaction.accountBalance.ToString());
                                            createCellCenter(worksheet, currentRow, 8, (transaction.avaliable + transaction.damaged).ToString());
                                            createCellCenter(worksheet, currentRow, 9, different);
                                            currentRow++;
                                        }
                                        worksheet.Cells[row, 4, row + checkShopTransaction.Count - 1, 4].Merge = true;
                                        row += checkShopTransaction.Count;
                                    }
                                    else
                                    {
                                        createCellCenter(worksheet, row, 5, "-");
                                        createCellCenter(worksheet, row, 6, "-");
                                        createCellCenter(worksheet, row, 7, "0");
                                        createCellCenter(worksheet, row, 8, "0");
                                        createCellCenter(worksheet, row, 9, "0");
                                        worksheet.Cells[row, 4, row - 1, 4].Merge = true;
                                        row++;
                                    }
                                }
                                worksheet.Cells[startRows, 3, row - 1, 3].Merge = true;
                            }
                            worksheet.Cells[beginRow, 2, row - 1, 2].Merge = true;

                            var diff = "";
                            if (account > amount)
                            {
                                diff = "(" + (account - amount).ToString() + ")";
                            }
                            else
                            {
                                diff = (amount - account).ToString();
                            }

                            sumAmount += amount;
                            sumAccount += account;

                            createCellCenter(worksheet, row, 2, employee.empName + " Total");
                            createCellCenter(worksheet, row, 7, account.ToString());
                            createCellCenter(worksheet, row, 8, amount.ToString());
                            createCellCenter(worksheet, row, 9, diff);

                            worksheet.Cells[row, 2, row, 6].Merge = true;
                            worksheet.Cells[row, 2, row, 9].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            worksheet.Cells[row, 2, row, 9].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(217, 217, 217));
                            worksheet.Cells[row, 2, row, 9].Style.Font.Bold = true;

                            row++;
                        }
                    }

                    var sumDiff = "";
                    if (sumAccount > sumAmount)
                    {
                        sumDiff = "(" + (sumAccount - sumAmount).ToString() + ")";
                    }
                    else
                    {
                        sumDiff = (sumAmount - sumAccount).ToString();
                    }


                    createCellCenter(worksheet, row, 2, "Grand Total");
                    createCellCenter(worksheet, row, 7, sumAccount.ToString());
                    createCellCenter(worksheet, row, 8, sumAmount.ToString());
                    createCellCenter(worksheet, row, 9, sumDiff);

                    worksheet.Cells[row, 2, row, 5].Merge = true;
                    //worksheet.Cells[row, 2, row, 8].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    //worksheet.Cells[row, 2, row, 8].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(217, 217, 217));
                    worksheet.Cells[row, 2, row, 9].Style.Font.Bold = true;

                    row++;

                    var tableBody = worksheet.Cells[startRow, 2, row - 1, 9];
                    tableBody.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    tableBody.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    tableBody.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    tableBody.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                    // set some core property values
                    xlPackage.Workbook.Properties.Title = "MAI ยืนยันยอดลูกหนี้";
                    xlPackage.Workbook.Properties.Author = "MAI admin";
                    xlPackage.Workbook.Properties.Subject = "Diff report";
                    // save the new spreadsheet
                    xlPackage.Save();
                    // Response.Clear();
                }
                stream.Position = 0;
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "รายงานผลต่าง_บริษัท_" + company.companyName + "_จำกัด.xlsx");
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            } 
        }

        private int createKeeperRow(List<AssetTask> tasks, Employee employee, ExcelWorksheet worksheet, int row)
        {
            List<AssetTask> currectTask = new();

            foreach (var task in tasks)
            {
                var shop = _context.Shops.Where(c => c.shopId == task.shopId).First();
                var keeper = _context.Shopkeepers.Where(c => c.shopId == shop.shopId && c.empId == employee.empId).ToList();
                if (keeper.Count > 0)
                {
                    var isHaveNewKeeper = false;
                    if (!keeper[0].isNew)
                    {
                        isHaveNewKeeper = isHasNewKeeper(employee.compCode, shop.shopId);
                    }

                    if (!isHaveNewKeeper)
                    {
                        currectTask.Add(task);
                    }
                }
            }

            if (currectTask.Count > 0)
            {
                int beginRow = row;
                createCell(worksheet, row, 2, employee.empName);

                var account = 0;
                var amount = 0;

                foreach (var task in currectTask)
                {
                    int startRow = row;
                    createCell(worksheet, row, 3, task.taskName);

                    var checkShop = _context.CheckShops.Where(c => c.taskID == task.taskID).First();
                    var checkShopSigneds = _context.CheckShopSigneds.Where(c => c.checkShopId == checkShop.checkShopId && c.compCode == employee.compCode).ToList();

                    if (checkShopSigneds.Count > 0)
                    {
                        foreach (var checkSigned in checkShopSigneds)
                        {
                            createCellCenter(worksheet, row, 4, checkSigned.confirmStatus);

                            List<CheckShopTransaction> checkShopTransaction;
                            if (checkSigned.assetTypeID == 1 || checkSigned.assetTypeID == 2)
                            {
                                checkShopTransaction = _context.CheckShopTransactions.Where(c => c.checkShopId == checkShop.checkShopId && c.compCode == employee.compCode && (c.assetTypeID == 1 || c.assetTypeID == 2)).ToList();
                            }
                            else
                            {
                                checkShopTransaction = _context.CheckShopTransactions.Where(c => c.checkShopId == checkShop.checkShopId && c.compCode == employee.compCode && c.assetTypeID == 3).ToList();
                            }
                            if (checkShopTransaction.Count > 0)
                            {
                                var currentRow = row;
                                foreach (var transaction in checkShopTransaction)
                                {
                                    var asset = _context.Assets.Where(c => c.assetId == transaction.assetId).First();
                                    var different = "";
                                    if (transaction.accountBalance > (transaction.avaliable + transaction.damaged))
                                    {
                                        different = "(" + (transaction.accountBalance - (transaction.avaliable + transaction.damaged)).ToString() + ")";
                                    }
                                    else
                                    {
                                        different = ((transaction.avaliable + transaction.damaged) - transaction.accountBalance).ToString();
                                    }

                                    account += transaction.accountBalance;
                                    amount += (transaction.avaliable + transaction.damaged);

                                    createCell(worksheet, currentRow, 5, asset.assetName);
                                    createCellCenter(worksheet, currentRow, 6, transaction.accountBalance.ToString());
                                    createCellCenter(worksheet, currentRow, 7, (transaction.avaliable + transaction.damaged).ToString());
                                    createCellCenter(worksheet, currentRow, 8, different);
                                    currentRow++;
                                }
                                worksheet.Cells[row, 4, row + checkShopTransaction.Count - 1, 4].Merge = true;
                                row += checkShopTransaction.Count;
                            }
                            else
                            {
                                createCellCenter(worksheet, row, 5, "-");
                                createCellCenter(worksheet, row, 6, "0");
                                createCellCenter(worksheet, row, 7, "0");
                                createCellCenter(worksheet, row, 8, "0");
                                worksheet.Cells[row, 4, row - 1, 4].Merge = true;
                                row++;
                            }
                        }
                    }
                    else
                    {
                        createCellCenter(worksheet, row, 4, "-");

                        var checkShopTransaction = _context.CheckShopTransactions.Where(c => c.checkShopId == checkShop.checkShopId && c.compCode == employee.compCode).ToList();
                        if (checkShopTransaction.Count > 0)
                        {
                            var currentRow = row;
                            foreach (var transaction in checkShopTransaction)
                            {
                                var asset = _context.Assets.Where(c => c.assetId == transaction.assetId).First();
                                var different = "";
                                if (transaction.accountBalance > (transaction.avaliable + transaction.damaged))
                                {
                                    different = "(" + (transaction.accountBalance - (transaction.avaliable + transaction.damaged)).ToString() + ")";
                                }
                                else
                                {
                                    different = ((transaction.avaliable + transaction.damaged) - transaction.accountBalance).ToString();
                                }

                                account += transaction.accountBalance;
                                amount += (transaction.avaliable + transaction.damaged);

                                createCell(worksheet, currentRow, 5, asset.assetName);
                                createCellCenter(worksheet, currentRow, 6, transaction.accountBalance.ToString());
                                createCellCenter(worksheet, currentRow, 7, (transaction.avaliable + transaction.damaged).ToString());
                                createCellCenter(worksheet, currentRow, 8, different);
                                currentRow++;
                            }
                            worksheet.Cells[row, 4, row + checkShopTransaction.Count - 1, 4].Merge = true;
                            row += checkShopTransaction.Count;
                        }
                        else
                        {
                            createCellCenter(worksheet, row, 5, "-");
                            createCellCenter(worksheet, row, 6, "0");
                            createCellCenter(worksheet, row, 7, "0");
                            createCellCenter(worksheet, row, 8, "0");
                            worksheet.Cells[row, 4, row - 1, 4].Merge = true;
                            row++;
                        }
                    }
                    worksheet.Cells[startRow, 3, row - 1, 3].Merge = true;
                }
                worksheet.Cells[beginRow, 2, row - 1, 2].Merge = true;

                var diff = "";
                if (account > amount)
                {
                    diff = "(" + (account - amount).ToString() + ")";
                }
                else
                {
                    diff = (amount - account).ToString();
                }

                createCellCenter(worksheet, row, 2, employee.empName + " Total");
                createCellCenter(worksheet, row, 6, account.ToString());
                createCellCenter(worksheet, row, 7, amount.ToString());
                createCellCenter(worksheet, row, 8, diff);

                worksheet.Cells[row, 2, row, 5].Merge = true;
                worksheet.Cells[row, 2, row, 8].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells[row, 2, row, 8].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(217, 217, 217));
                worksheet.Cells[row, 2, row, 8].Style.Font.Bold = true;

                row++;
            }

            return row;
        }

        private int generateKeeperRow(List<AssetTask> currectTask, ExcelWorksheet worksheet, Employee employee, int row)
        {
            if (currectTask.Count > 0)
            {
                int beginRow = row;
                createCell(worksheet, row, 2, employee.empName);

                var account = 0;
                var amount = 0;

                foreach (var task in currectTask)
                {
                    int startRow = row;
                    createCell(worksheet, row, 3, task.taskName);

                    var checkShop = _context.CheckShops.Where(c => c.taskID == task.taskID).First();
                    var checkShopSigneds = _context.CheckShopSigneds.Where(c => c.checkShopId == checkShop.checkShopId && c.compCode == employee.compCode).ToList();

                    if (checkShopSigneds.Count > 0)
                    {
                        foreach (var checkSigned in checkShopSigneds)
                        {
                            createCellCenter(worksheet, row, 4, checkSigned.confirmStatus);

                            List<CheckShopTransaction> checkShopTransaction;
                            if (checkSigned.assetTypeID == 1 || checkSigned.assetTypeID == 2)
                            {
                                checkShopTransaction = _context.CheckShopTransactions.Where(c => c.checkShopId == checkShop.checkShopId && c.compCode == employee.compCode && (c.assetTypeID == 1 || c.assetTypeID == 2)).ToList();
                            }
                            else
                            {
                                checkShopTransaction = _context.CheckShopTransactions.Where(c => c.checkShopId == checkShop.checkShopId && c.compCode == employee.compCode && c.assetTypeID == 3).ToList();
                            }
                            if (checkShopTransaction.Count > 0)
                            {
                                var currentRow = row;
                                foreach (var transaction in checkShopTransaction)
                                {
                                    var asset = _context.Assets.Where(c => c.assetId == transaction.assetId).First();
                                    var different = "";
                                    if (transaction.accountBalance > (transaction.avaliable + transaction.damaged))
                                    {
                                        different = "(" + (transaction.accountBalance - (transaction.avaliable + transaction.damaged)).ToString() + ")";
                                    }
                                    else
                                    {
                                        different = ((transaction.avaliable + transaction.damaged) - transaction.accountBalance).ToString();
                                    }

                                    account += transaction.accountBalance;
                                    amount += (transaction.avaliable + transaction.damaged);

                                    createCell(worksheet, currentRow, 5, asset.assetName);
                                    createCellCenter(worksheet, currentRow, 6, transaction.accountBalance.ToString());
                                    createCellCenter(worksheet, currentRow, 7, (transaction.avaliable + transaction.damaged).ToString());
                                    createCellCenter(worksheet, currentRow, 8, different);
                                    currentRow++;
                                }
                                worksheet.Cells[row, 4, row + checkShopTransaction.Count - 1, 4].Merge = true;
                                row += checkShopTransaction.Count;
                            }
                            else
                            {
                                createCellCenter(worksheet, row, 5, "-");
                                createCellCenter(worksheet, row, 6, "0");
                                createCellCenter(worksheet, row, 7, "0");
                                createCellCenter(worksheet, row, 8, "0");
                                worksheet.Cells[row, 4, row - 1, 4].Merge = true;
                                row++;
                            }
                        }
                    }
                    else
                    {
                        createCellCenter(worksheet, row, 4, "-");

                        var checkShopTransaction = _context.CheckShopTransactions.Where(c => c.checkShopId == checkShop.checkShopId && c.compCode == employee.compCode).ToList();
                        if (checkShopTransaction.Count > 0)
                        {
                            var currentRow = row;
                            foreach (var transaction in checkShopTransaction)
                            {
                                var asset = _context.Assets.Where(c => c.assetId == transaction.assetId).First();
                                var different = "";
                                if (transaction.accountBalance > (transaction.avaliable + transaction.damaged))
                                {
                                    different = "(" + (transaction.accountBalance - (transaction.avaliable + transaction.damaged)).ToString() + ")";
                                }
                                else
                                {
                                    different = ((transaction.avaliable + transaction.damaged) - transaction.accountBalance).ToString();
                                }

                                account += transaction.accountBalance;
                                amount += (transaction.avaliable + transaction.damaged);

                                createCell(worksheet, currentRow, 5, asset.assetName);
                                createCellCenter(worksheet, currentRow, 6, transaction.accountBalance.ToString());
                                createCellCenter(worksheet, currentRow, 7, (transaction.avaliable + transaction.damaged).ToString());
                                createCellCenter(worksheet, currentRow, 8, different);
                                currentRow++;
                            }
                            worksheet.Cells[row, 4, row + checkShopTransaction.Count - 1, 4].Merge = true;
                            row += checkShopTransaction.Count;
                        }
                        else
                        {
                            createCellCenter(worksheet, row, 5, "-");
                            createCellCenter(worksheet, row, 6, "0");
                            createCellCenter(worksheet, row, 7, "0");
                            createCellCenter(worksheet, row, 8, "0");
                            worksheet.Cells[row, 4, row - 1, 4].Merge = true;
                            row++;
                        }
                    }
                    worksheet.Cells[startRow, 3, row - 1, 3].Merge = true;
                }
                worksheet.Cells[beginRow, 2, row - 1, 2].Merge = true;

                var diff = "";
                if (account > amount)
                {
                    diff = "(" + (account - amount).ToString() + ")";
                }
                else
                {
                    diff = (amount - account).ToString();
                }

                createCellCenter(worksheet, row, 2, employee.empName + " Total");
                createCellCenter(worksheet, row, 6, account.ToString());
                createCellCenter(worksheet, row, 7, amount.ToString());
                createCellCenter(worksheet, row, 8, diff);

                worksheet.Cells[row, 2, row, 5].Merge = true;
                worksheet.Cells[row, 2, row, 8].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells[row, 2, row, 8].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(217, 217, 217));
                worksheet.Cells[row, 2, row, 8].Style.Font.Bold = true;

                row++;
            }
            return row;
        }

        public static T DeserializeObject<T>(string data)
        {
            var x = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(data);
            return x;
        }
    }
}
