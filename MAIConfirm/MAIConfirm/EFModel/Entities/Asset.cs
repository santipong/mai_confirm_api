﻿using System;
using System.Collections.Generic;

#nullable disable

namespace MAIConfirm.EFModel.Entities
{
    public partial class Asset
    {
        public int AssetId { get; set; }
        public string AssetCode { get; set; }
        public string AssetName { get; set; }
        public string AssetType { get; set; }
        public string AccountType { get; set; }
        public string Status { get; set; }
        public string Region { get; set; }
        public string CompCode { get; set; }
        public int DepartmentId { get; set; }
        public string UnitCount { get; set; }
        public double? Price { get; set; }
        public int? Amount { get; set; }
        public byte[] CreatedAt { get; set; }
        public bool IsNew { get; set; }
    }
}
