﻿using System;
using System.Collections.Generic;

#nullable disable

namespace MAIConfirm.EFModel.Entities
{
    public partial class CheckShopKeeper
    {
        public int Id { get; set; }
        public int CheckShopId { get; set; }
        public string EmpId { get; set; }
        public string EmpName { get; set; }
        public string CompCode { get; set; }
        public bool IsActive { get; set; }
        public byte[] CreatedAt { get; set; }
    }
}
