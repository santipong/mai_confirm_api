﻿using System;
using System.Collections.Generic;

#nullable disable

namespace MAIConfirm.EFModel.Entities
{
    public partial class CheckShop
    {
        public int CheckShopId { get; set; }
        public int TaskId { get; set; }
        public string ShopStatus { get; set; }
        public string ShopImage { get; set; }
        public string CheckLocation { get; set; }
        public string Remark { get; set; }
        public byte[] CreatedAt { get; set; }
    }
}
