﻿using System;
using System.Collections.Generic;

#nullable disable

namespace MAIConfirm.EFModel.Entities
{
    public partial class Employee
    {
        public int EmpId { get; set; }
        public string EmpName { get; set; }
        public string Email { get; set; }
        public string EmpPosition { get; set; }
        public string Signature { get; set; }
        public string Region { get; set; }
        public string CompCode { get; set; }
        public byte[] CreatedAt { get; set; }
        public int? DepartmentId { get; set; }
    }
}
