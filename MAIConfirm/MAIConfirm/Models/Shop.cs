﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MAIConfirm.Models
{
    public class Shop
    {
        public int shopId { get; set; }
        public string shopCode { get; set; }
        public string shopName { get; set; }
        public string address { get; set; }
        public string location { get; set; }
    }
}
