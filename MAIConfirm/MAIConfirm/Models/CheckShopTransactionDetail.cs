﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MAIConfirm.Models
{
    public class CheckShopTransactionDetail
    {
		public int id { get; set; }
		public int checkShopTransactionId { get; set; }
		public int amount { get; set; }
		public string assetStatus { get; set; }
		public string attachImage { get; set; }
		public string remark { get; set; }
	}

	public class CheckTransactionResponse
	{
		public string status { get; set; }
		public string message { get; set; }
		public List<CheckShopTransactionDetail> checkShopTransactionDetails { get; set; }
	}
}
