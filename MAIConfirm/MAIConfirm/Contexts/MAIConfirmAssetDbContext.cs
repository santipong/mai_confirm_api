﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MAIConfirm.Models;

namespace MAIConfirm.Models
{
    public class MAIConfirmAssetDbContext : DbContext
    {
        public DbSet<Period> Periods { get; set; }
        public DbSet<Asset> Assets { get; set; }
        public DbSet<CheckShop> CheckShops { get; set; }
        public DbSet<CheckShopDetail> CheckShopDetails { get; set; }
        public DbSet<CheckShopSigned> CheckShopSigneds { get; set; }
        public DbSet<CheckShopTransaction> CheckShopTransactions { get; set; }
        public DbSet<CheckShopTransactionDetail> CheckShopTransactionDetails { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Shop> Shops { get; set; }
        public DbSet<Shopkeeper> Shopkeepers { get; set; }
        public DbSet<AssetTask> Tasks { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<AssetType> AssetTypes { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<CheckShopKeeper> CheckShopKeepers { get; set; }

        public DbSet<MAIConfirm.Models.Asset> Asset { get; set; }

        public MAIConfirmAssetDbContext(DbContextOptions<MAIConfirmAssetDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Period>().ToTable("Period");
            modelBuilder.Entity<Asset>().ToTable("Asset");
            modelBuilder.Entity<CheckShop>().ToTable("CheckShop");
            modelBuilder.Entity<CheckShopDetail>().ToTable("CheckShopDetail");
            modelBuilder.Entity<CheckShopSigned>().ToTable("CheckShopSigned");
            modelBuilder.Entity<CheckShopTransaction>().ToTable("CheckShopTransaction");
            modelBuilder.Entity<CheckShopTransactionDetail>().ToTable("CheckShopTransactionDetail");
            modelBuilder.Entity<Department>().ToTable("Department");
            modelBuilder.Entity<Employee>().ToTable("Employee");
            modelBuilder.Entity<Shop>().ToTable("Shop");
            modelBuilder.Entity<Shopkeeper>().ToTable("Shopkeeper");
            modelBuilder.Entity<AssetTask>().ToTable("Task");
            modelBuilder.Entity<Company>().ToTable("Company");
            modelBuilder.Entity<AssetType>().ToTable("AssetType");
            modelBuilder.Entity<User>().ToTable("User");
            modelBuilder.Entity<CheckShopKeeper>().ToTable("CheckShopKeeper");
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.EnableSensitiveDataLogging();
        }
    }
}
