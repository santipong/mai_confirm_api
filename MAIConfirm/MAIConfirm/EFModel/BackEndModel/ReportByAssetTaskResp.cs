﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MAIConfirm.EFModel.BackEndModel
{
    public class ReportByAssetTaskResp
    {
		public int? taskID { get; set; }
		public int? periodId { get; set; }
		public int? shopId { get; set; }
		public string taskName { get; set; }
		public DateTime planDate { get; set; }
		public DateTime checkDate { get; set; }
		public DateTime accountDate { get; set; }
		public int? assetTypeID { get; set; }
	}

}
