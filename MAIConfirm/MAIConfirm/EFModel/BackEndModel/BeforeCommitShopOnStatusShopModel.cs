﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MAIConfirm.EFModel.BackEndModel
{
    public class BeforeCommitShopOnStatusShopModel
    {
        public string status { get; set; }
        public string message { get; set; }
        public BeforeCommitShopOnStatusShopResp beforeCommitShopOnStatusShopResp { get; set; }
    }

    public class BeforeCommitShopOnStatusShopResp
    {
        public int? isFlagUnChecked { get; set; }
        public string shopStatus { get; set; }
    }

    public class BeforeCommitShopOnStatusShopReq
    {
        public int? taskID { get; set; }
    }
}
