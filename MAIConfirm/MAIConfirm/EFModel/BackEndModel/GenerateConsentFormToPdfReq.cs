﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MAIConfirm.EFModel.BackEndModel
{
    public class GenerateConsentFormToPdfReq
    {
        public int? periodId { get; set; }
    }

    public class GenerateConsentFormToPdfResp
    {
        public string compName { get; set; }
        public string compAdr { get; set; }
        public string compCod { get; set; }
        public string laterTitle { get; set; }
        public string taskName { get; set; }
        public string shopCode { get; set; }
        public string laterAdr { get; set; }
        public string laterDescripton { get; set; }
        public string laterDatetime { get; set; }
        public int? taskID { get; set; }
        public int? periodId { get; set; }
        public int? shopId { get; set; }
        public int? checkShopId { get; set; }
        public string checkingDate { get; set; }
        public int? typeId { get; set; }
        public string typeName { get; set; }
        public string folderName { get; set; }
        public string confirmStatus { get; set; }
        public string remark { get; set; }
    }

    public class GetCheckShopTransactionResp
    {
        public string assetCode { get; set; }
        public string assetName { get; set; }
        public string unitCount { get; set; }
        public int? accountBalance { get; set; }
        public int? avaliable { get; set; }
        public int? damaged { get; set; }
        public int? diffTotal { get; set; }
        public int? typeId { get; set; }
        public string compCode { get; set; }
    }

    public class GetCheckShopDetailSignResp
    {
        public string customerName { get; set; }
        public string customerRelation { get; set; }
        public int? checkShopId { get; set; }
        public string customerSigned { get; set; }
        public string checkerId { get; set; }
        public string empName { get; set; }
        public string empPosition { get; set; }
        public string signature { get; set; }
        public int? assetTypeID { get; set; }
        public string compCode { get; set; }

    }

    public class GetSaleOnShopResp
    {
        public string compCode { get; set; }
        public int? assetTypeID { get; set; }
        public string EmpName { get; set; }
        public string EmpPosition { get; set; }
        public string employeeSigned { get; set; }

    }

}
