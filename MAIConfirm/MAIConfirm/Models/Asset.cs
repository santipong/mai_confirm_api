﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MAIConfirm.Models
{
    public class Asset
    {
		public int assetId { get; set; }
		public string assetCode { get; set; }
		public string assetName { get; set; }
		public string assetType { get; set; }
		public string accountType { get; set; }
		public string status { get; set; }
		public string region { get; set; }
		public string compCode { get; set; }
		public int departmentId { get; set; }
		public string unitCount { get; set; }
		public double price { get; set; }
		public int amount { get; set; }
		public bool isNew { get; set; }
	}

	public class CheckAsset
	{
		public int checkShopTransactionId { get; set; }
		public int checkShopId { get; set; }
		public int assetId { get; set; }
		public int assetTypeID { get; set; }
		public string assetCode { get; set; }
		public string assetName { get; set; }
		public string assetType { get; set; }
		public string accountType { get; set; }
		public string status { get; set; }
		public string region { get; set; }
		public string compCode { get; set; }
		public int departmentId { get; set; }
		public string unitCount { get; set; }
		public double price { get; set; }
		public int amount { get; set; }
		public int accountBalance { get; set; }
		public int avaliable { get; set; }
		public int damaged { get; set; }
		public bool isNew { get; set; }
	}

	public class CheckAssetResponse
	{
		public string status { get; set; }
		public string message { get; set; }
		public List<CheckAsset> checkAssets { get; set; }
	}
}
