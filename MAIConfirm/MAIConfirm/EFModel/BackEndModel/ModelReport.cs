﻿using System;

namespace MAIConfirm.EFModel.BackEndModel
{
    public class ModelReport
    {
        public class Req_ReportExcelConfirmImage
        {
            public int? compCode { get; set; }
            public int? periodId { get; set; }
            public string center { get; set; }            
        }

        public class Res_HeaderReportExcelConfirmImage
        {          
            public int? periodId { get; set; }
            public string periodNName { get; set; }
            public string startDate { get; set; }
            public string endDate { get; set; }
            public string departmentName { get; set; }
            public string departmentCenter { get; set; }
            public string companyName { get; set; }
            //public string CountGood { get; set; }
            //public string CountBad { get; set; }
            public int? CountImageGood { get; set; }
            public int? CountImageBad { get; set; }
        }

        public class Res_ReportExcelConfirmImage
        {
            public string departmentName { get; set; }
            public int? empId { get; set; }            
            public string empName { get; set; }
            public string shopCode { get; set; }
            public string shopName { get; set; }
            public string shopStatus { get; set; }
            public string shopImage { get; set; }
            public string remark { get; set; }
            public string checkDate { get; set; }
            public string assetType { get; set; }
            public string assetCode { get; set; }
            public string assetName { get; set; }
            public int? CountGood { get; set; }
            public int? CountBad { get; set; }
            public int? SumGood { get; set; }
            public int? SumBad { get; set; }
            public int? CountImageGood { get; set; }
            public int? CountImageBad { get; set; }
            public int? SumAccountBalance { get; set; }
            public int? DiffAccountBalance { get; set; }
        }

        public class Res_DeteilReportExcelConfirmImage
        {
            public string departmentName { get; set; }
            public int? empId { get; set; }
            public string empName { get; set; }
            public string shopCode { get; set; }
            public string shopName { get; set; }
            public string shopStatus { get; set; }
            public string shopImage { get; set; }
            public string remark { get; set; }
            public string checkDate { get; set; }
            public string assetType { get; set; }
            public string assetCode { get; set; }
            public string assetName { get; set; }
            public int? amount { get; set; }
            public string assetStatus { get; set; }
            public string attachImage { get; set; }
            public string remarkimage { get; set; }
        }
    }
}
