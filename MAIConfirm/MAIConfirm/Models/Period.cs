﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MAIConfirm.Models
{
    public class Period
    {
        public int periodId { get; set; }
        public string periodNName { get; set; }
        public string regionId { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
    }


    public class PeriodResponse
    {
        public string status { get; set; }
        public string message { get; set; }
        public List<Period> periods { get; set; }
    }
}
