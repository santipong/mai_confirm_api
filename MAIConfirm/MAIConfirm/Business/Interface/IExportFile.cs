﻿using MAIConfirm.EFModel.BackEndModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MAIConfirm.Business.Interface
{
    public interface IExportFile
    {
        List<GenerateConsentFormToPdfResp> generateConsentFormToPdfBC(GenerateConsentFormToPdfReq req);
        List<GetCheckShopTransactionResp> getCheckShopTransactionBC(int? checkShopId);
        List<GetCheckShopDetailSignResp> GetCheckShopDetailSignBC(int? checkShopId);
        GetSaleOnShopResp GetSaleOnShopBC(int? checkShopId, int? periodId, int? taskID, string compCod,int? typeId);
        BeforeCommitShopOnStatusShopModel BeforeCommitShopOnStatusShopBC(BeforeCommitShopOnStatusShopReq req);
        List<ModelReport.Res_ReportExcelConfirmImage> GetImageReportTaskOnSummary(ModelReport.Req_ReportExcelConfirmImage req);
        List<ModelReport.Res_DeteilReportExcelConfirmImage> GetDeteilImageReportTaskOnSummary(ModelReport.Req_ReportExcelConfirmImage req);
        List<ModelReport.Res_HeaderReportExcelConfirmImage> GetHeaderImageReportTaskOnSummary(ModelReport.Req_ReportExcelConfirmImage req);
    }
}
