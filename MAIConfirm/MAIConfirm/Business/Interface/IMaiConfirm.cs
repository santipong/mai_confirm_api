﻿using MAIConfirm.EFModel.BackEndModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MAIConfirm.Business.Interface
{
    public interface IMaiConfirm
    {
        GetStatusReportTaskOnSummaryResp GetStatusReportTaskOnSummary(GetStatusReportTaskOnSummaryReq req);
        List<GetStatusReportTaskOnSummaryResp> GetStatusReportTaskOnSummaryALL(GetStatusReportTaskOnSummaryReq req);
    }
}
