﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MAIConfirm.Models
{
    public class Response
    {
        public string status { get; set; }
        public string message { get; set; }
    }

    public class ResponseWithId
    {
        public string status { get; set; }
        public string message { get; set; }

        public string id { get; set; }
    }
}
