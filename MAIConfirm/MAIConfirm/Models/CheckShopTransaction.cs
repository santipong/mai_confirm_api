﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MAIConfirm.Models
{
    public class CheckShopTransaction
    {
		public int checkShopTransactionId { get; set; }
		public int checkShopId { get; set; }
		public int assetId { get; set; }
		public string compCode { get; set; }
		public int assetTypeID { get; set; }
		public int accountBalance { get; set; }
		public int avaliable { get; set; }
		public int damaged { get; set; }
	}
}
