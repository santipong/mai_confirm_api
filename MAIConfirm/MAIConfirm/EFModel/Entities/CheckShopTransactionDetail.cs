﻿using System;
using System.Collections.Generic;

#nullable disable

namespace MAIConfirm.EFModel.Entities
{
    public partial class CheckShopTransactionDetail
    {
        public int Id { get; set; }
        public int CheckShopTransactionId { get; set; }
        public int? Amount { get; set; }
        public string AssetStatus { get; set; }
        public string AttachImage { get; set; }
        public string Remark { get; set; }
        public byte[] CreatedAt { get; set; }
    }
}
