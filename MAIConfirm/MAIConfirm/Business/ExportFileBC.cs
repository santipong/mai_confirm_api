﻿using MAIConfirm.Business.Helper;
using MAIConfirm.Business.Interface;
using MAIConfirm.EFModel.BackEndModel;
using MAIConfirm.EFModel.Entities;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using static MAIConfirm.EFModel.BackEndModel.ModelReport;

namespace MAIConfirm.Business
{
    public class ExportFileBC : IExportFile
    {
        private readonly IConfiguration configuration;
        private string connectionString;
        static IConfiguration conf = (new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build());

        public ExportFileBC(IConfiguration config)
        {
            configuration = config;
        }

        List<GenerateConsentFormToPdfResp> IExportFile.generateConsentFormToPdfBC(GenerateConsentFormToPdfReq req)
        {
            connectionString = configuration.GetConnectionString(ConstMsg.MAICONFIRMDBCONNECTIONSTRING);
            SqlConnection conn = new SqlConnection(connectionString);
            List<GenerateConsentFormToPdfResp> model = new List<GenerateConsentFormToPdfResp>();

            try
            {
                conn.Open();
                SqlCommand cm = null;
                string sql = $@"

                            DECLARE @v_periodId INT SET @v_periodId = @periodIdx

                            SELECT DISTINCT compName = CASE WHEN d.compCode = '7800' THEN CONCAT('บริษัท ', d.companyName, ' (2012) จำกัด') ELSE CONCAT('บริษัท ', d.companyName, ' จำกัด') END
	                            , compAdr = d.[address]
                                , compCod = d.[compCode]
	                            , laterTitle = CONCAT('เรียน ', REPLACE(REPLACE(a.taskName,'/',''),'?',''))
                                , REPLACE(REPLACE(a.taskName,'/',''),'?','') as taskName
                                , shopCode = RIGHT(e.shopCode, 4)
	                            , laterAdr = e.[address]
	                            , laterDescripton = 'บริษัทขอแจ้งยืนยันยอดวัสดุ/อุปกรณ์ ที่ท่านคงค้างบริษัท'
	                            , laterDatetime = CONCAT('ณ วันที่ ', CONVERT(NVARCHAR(10),a.accountDate,103), ' (ตามบัญชี) ดังนี้')
	                            , a.taskID
	                            , a.periodId
	                            , a.shopId
	                            , b.checkShopId
	                            , checkingDate = CONCAT('วันที่ ', CONVERT(NVARCHAR(10),a.checkDate,103), ' (ตรวจนับ)')
	                            , f.typeId
                                , typeName =  CASE WHEN f.typeId = 1 THEN 'POPPOS' ELSE CASE WHEN f.typeId = 4 THEN 'เครื่อง POS' ELSE 'เบียร์สด' END END
                            	, folderName = CONCAT(SUBSTRING(
												REPLACE(
													REPLACE(
														REPLACE(
															REPLACE(
																REPLACE(
																	REPLACE(
																		REPLACE(
																			REPLACE(REPLACE(REPLACE(REPLACE(a.taskName,' ',''),'บริษัท',''),'บจก.',''),'ร้าน','')
																		,'(','')
																	,')','')
																,'คุณ','')
															,'นาง','')
														,'นาย','')
													,'นางสาว','')
												,'น.ส.','')
												,0,10),'_',SUBSTRING(d.compCode,0,3),'_',CASE WHEN f.typeId = 1 THEN 'POPPOS' ELSE CASE WHEN f.typeId = 4 THEN 'เครื่อง POS' ELSE 'เบียร์สด' END END)
                               
                                , c.confirmStatus
								, c.remark
                            FROM [dbo].[Task] a INNER JOIN [dbo].[CheckShop] b 
                            ON a.taskID = b.taskID INNER JOIN [dbo].[CheckShopSigned] c
                            ON b.checkShopId = c.checkShopId INNER JOIN [dbo].[Company] d
                            ON c.compCode = d.compCode INNER JOIN [dbo].[Shop] e
                            ON a.shopId = e.shopId INNER JOIN (
				                            SELECT DISTINCT a.checkShopId,a.assetId,a.compCode,typeId = CASE WHEN b.typeId = 1 OR  b.typeId = 2 THEN 1 ELSE CASE WHEN  b.typeId = 4 THEN b.typeId else 3 END END
				                            FROM  [dbo].[CheckShopTransaction] a INNER JOIN [dbo].[AssetType] b
				                            ON a.assetTypeID = b.typeId 
				                            ) f
                            ON b.checkShopId = f.checkShopId INNER JOIN [dbo].[Period] g
							ON a.periodId = g.periodId AND d.region = g.regionId
                            WHERE 1=1
                            AND a.periodId = @v_periodId
                            AND c.assetTypeID = f.typeId
                            /*AND a.taskID IN(26457,26470,26441)*/
                           /*AND a.taskID IN(33052)*/
                           /* AND a.taskID IN(26402,26397)*/
                          /* AND a.taskID IN(35543,35570,35595)*/

                ";
                cm = new SqlCommand(sql, conn);
                cm.Parameters.Add(new SqlParameter("@periodIdx", req.periodId));

                SqlDataReader reader = cm.ExecuteReader();
                if (reader.HasRows)
                {
                    model = new List<GenerateConsentFormToPdfResp>();
                    model = HelperBC.DataReaderMapToList<GenerateConsentFormToPdfResp>(reader);
                }
                else
                    throw new Exception(ConstMsg.FIND_REPORT_AUDIT_SHOP_NOT_FOUND);

                //using(var _db = new DBMaiConfirmAssetContext())
                //{
                //    List<EFModel.Entities.Task> task = _db.Tasks.ToList();
                //}

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conn.Close();
            }

            return model;
        }

        List<GetCheckShopTransactionResp> IExportFile.getCheckShopTransactionBC(int? checkShopId)
        {
            connectionString = configuration.GetConnectionString(ConstMsg.MAICONFIRMDBCONNECTIONSTRING);
            SqlConnection conn = new SqlConnection(connectionString);
            List<GetCheckShopTransactionResp> getCheckShopTransactionResps = null;
            try
            {
                conn.Open();
                SqlCommand cm = null;
                string sql = $@"
                                    DECLARE @v_checkShopId INT SET @v_checkShopId = @checkShopIdx
                                    SELECT 
	                                    b.assetCode
	                                    ,b.assetName
	                                    ,b.unitCount
	                                    ,a.accountBalance
	                                    ,a.avaliable
	                                    ,a.damaged
	                                    --,diffTotal = CASE	WHEN a.accountBalance <> 0 AND (ISNULL(a.avaliable,0) + ISNULL(a.damaged,0)) <> 0 THEN ((ISNULL(a.avaliable,0) + ISNULL(a.damaged,0))-ISNULL(a.accountBalance,0))
						                               --     WHEN a.accountBalance <> 0 AND (ISNULL(a.avaliable,0) + ISNULL(a.damaged,0)) = 0 THEN -ISNULL(a.accountBalance,0)
						                               --     ELSE 0
				                                 --   END
										,diffTotal = ((ISNULL(a.avaliable,0) + ISNULL(a.damaged,0))-ISNULL(a.accountBalance,0))
                                        ,typeId = CASE WHEN a.assetTypeID = 1 OR  a.assetTypeID = 2 THEN 1 ELSE CASE WHEN  a.assetTypeID = 4 THEN a.assetTypeID else 3 END END
                                        ,a.compCode
                                    FROM [dbo].[CheckShopTransaction] a INNER JOIN [dbo].[Asset] b
                                    ON a.assetId = b.assetId
                                    WHERE 1=1
                                    AND a.checkShopId = @v_checkShopId
                ";
                cm = new SqlCommand(sql, conn);
                cm.Parameters.Add(new SqlParameter("@checkShopIdx", checkShopId));

                SqlDataReader reader = cm.ExecuteReader();
                if (reader.HasRows)
                {
                    getCheckShopTransactionResps = new List<GetCheckShopTransactionResp>();
                    getCheckShopTransactionResps = HelperBC.DataReaderMapToList<GetCheckShopTransactionResp>(reader);
                }
                else
                    throw new Exception(ConstMsg.FIND_REPORT_AUDIT_SHOP_NOT_FOUND);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conn.Close();
            }

            return getCheckShopTransactionResps;
        }

        List<GetCheckShopDetailSignResp> IExportFile.GetCheckShopDetailSignBC(int? checkShopId)
        {
            connectionString = configuration.GetConnectionString(ConstMsg.MAICONFIRMDBCONNECTIONSTRING);
            SqlConnection conn = new SqlConnection(connectionString);
            List<GetCheckShopDetailSignResp> getCheckShopDetailSignResps = null;
            try
            {
                conn.Open();
                SqlCommand cm = null;
                string sql = $@"
                                        SELECT /*TOP 1 */
	                                        customerName = CONCAT('( ', a.customerName, ' ) ')
	                                        ,customerRelation = a.relation
	                                        , a.checkShopId
	                                        , b.customerSigned
	                                        , b.checkerId
	                                        , empName =  CONCAT('( ', c.empName, ' ) ')
	                                        , empPosition = ISNULL(c.empPosition,'เจ้าหน้าที่ตรวจปฏิบัติการ')
	                                        , c.[signature]
                                            , b.assetTypeID
                                            , b.compCode
                                        FROM [dbo].[CheckShopDetail] a LEFT JOIN [dbo].[CheckShopSigned] b
                                        ON a.checkShopId = b.checkShopId INNER JOIN [dbo].[Employee] c
                                        ON b.checkerId = c.empId
                                        WHERE a.checkShopId = @checkShopIdx
                                        /*ORDER BY a.id DESC*/
                ";
                cm = new SqlCommand(sql, conn);
                cm.Parameters.Add(new SqlParameter("@checkShopIdx", checkShopId));

                SqlDataReader reader = cm.ExecuteReader();
                if (reader.HasRows)
                {
                    getCheckShopDetailSignResps = new List<GetCheckShopDetailSignResp>();
                    getCheckShopDetailSignResps = HelperBC.DataReaderMapToList<GetCheckShopDetailSignResp>(reader);
                }
                else
                    throw new Exception(ConstMsg.FIND_REPORT_AUDIT_SHOP_NOT_FOUND);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conn.Close();
            }

            return getCheckShopDetailSignResps;
        }

        GetSaleOnShopResp IExportFile.GetSaleOnShopBC(int? checkShopId, int? periodId, int? taskID, string compCod,int? typeId)
        {
            connectionString = configuration.GetConnectionString(ConstMsg.MAICONFIRMDBCONNECTIONSTRING);
            SqlConnection conn = new SqlConnection(connectionString);
            GetSaleOnShopResp getSaleOnShopResp = null;
            try
            {
                conn.Open();
                SqlCommand cm = null;
                string sql = $@"
                           
                                    DECLARE @v_periodId INT SET @v_periodId = @periodIdx
                                    DECLARE @v_checkShopId INT SET @v_checkShopId = @checkShopIdx
                                    DECLARE @v_compCode NVARCHAR(5) SET @v_compCode = @compCodx
                                    DECLARE @v_taskId INT SET @v_taskId = @taskIDx
                                    DECLARE @v_typeId INT SET @v_typeId = @typeIdx
                                    SELECT e.compCode
	                                    ,EmpName = CONCAT('( ',d.empName, ' )') 
	                                    ,EmpPosition = 'พนักงานผู้ดูแล'
	                                    ,e.employeeSigned
										,e.assetTypeID
                                    FROM [Task] a INNER JOIN [dbo].[CheckShop] b 
                                    ON a.taskID = b.taskID INNER JOIN [dbo].[Shopkeeper] c
                                    ON a.shopId = c.shopId INNER JOIN [dbo].[Employee] d
                                    ON c.empId = d.empId  LEFT JOIN [dbo].[CheckShopSigned] e
                                    ON b.checkShopId = e.checkShopId AND e.compCode = @v_compCode LEFT JOIN (
											                                    SELECT DISTINCT a.checkShopId,a.compCode,typeId = CASE WHEN b.typeId = 1 OR  b.typeId = 2 THEN 1 ELSE CASE WHEN  b.typeId = 4 THEN b.typeId else 3 END END
											                                    FROM  [dbo].[CheckShopTransaction] a INNER JOIN [dbo].[AssetType] b
											                                    ON a.assetTypeID = b.typeId
                                    ) f
                                    ON f.checkShopId = b.checkShopId AND e.assetTypeID = f.typeId AND d.compCode = f.compCode 
                                    WHERE 1=1
                                    AND b.checkShopId = @v_checkShopId
                                    -- AND d.compCode = @v_compCode
                                    AND a.taskID = @v_taskId         
                                    AND a.periodId = @v_periodId       
                                    AND e.assetTypeID = @v_typeId       
                ";
                cm = new SqlCommand(sql, conn);
                cm.Parameters.Add(new SqlParameter("@checkShopIdx", checkShopId));
                cm.Parameters.Add(new SqlParameter("@periodIdx", periodId));
                cm.Parameters.Add(new SqlParameter("@taskIDx", taskID));
                cm.Parameters.Add(new SqlParameter("@typeIdx", typeId));
                cm.Parameters.Add(new SqlParameter("@compCodx", compCod));

                SqlDataReader reader = cm.ExecuteReader();
                if (reader.HasRows)
                {
                    getSaleOnShopResp = new GetSaleOnShopResp();
                    getSaleOnShopResp = HelperBC.DataReaderMapFirst<GetSaleOnShopResp>(reader);
                }
                else
                    throw new Exception(ConstMsg.FIND_REPORT_AUDIT_SHOP_NOT_FOUND);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conn.Close();
            }

            return getSaleOnShopResp;
        }

        BeforeCommitShopOnStatusShopModel IExportFile.BeforeCommitShopOnStatusShopBC(BeforeCommitShopOnStatusShopReq req)
        {
            BeforeCommitShopOnStatusShopModel model = new BeforeCommitShopOnStatusShopModel();
            model.beforeCommitShopOnStatusShopResp = null;

            connectionString = configuration.GetConnectionString(ConstMsg.MAICONFIRMDBCONNECTIONSTRING);
            SqlConnection conn = new SqlConnection(connectionString);

            try
            {
                conn.Open();
                SqlCommand cm = null;
                string sql = $@"
                                DECLARE @v_taskId INT SET @v_taskId = @taskIDx 
                                SELECT shopStatus,isFlagUnChecked = CASE WHEN shopStatus = 'unchecked' THEN 1 ELSE 0 END  FROM [dbo].[CheckShop] where taskID = @v_taskId
                ";
                cm = new SqlCommand(sql, conn);
                cm.Parameters.Add(new SqlParameter("@taskIDx", req.taskID));

                SqlDataReader reader = cm.ExecuteReader();
                if (reader.HasRows)
                {
                    model.beforeCommitShopOnStatusShopResp = new BeforeCommitShopOnStatusShopResp();
                    model.beforeCommitShopOnStatusShopResp = HelperBC.DataReaderMapFirst<BeforeCommitShopOnStatusShopResp>(reader);
                    model.status = ConstMsg.STATUS_SUCCESS;
                }
                else
                    throw new Exception(ConstMsg.FIND_REPORT_AUDIT_SHOP_NOT_FOUND);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conn.Close();
            }

            return model;
        }

        List<Res_ReportExcelConfirmImage> IExportFile.GetImageReportTaskOnSummary(Req_ReportExcelConfirmImage req)
        {
            connectionString = configuration.GetConnectionString(ConstMsg.MAICONFIRMDBCONNECTIONSTRING);
            SqlConnection conn = new SqlConnection(connectionString);
            List<Res_ReportExcelConfirmImage> reportTaskOnSummaryResp = null;
            try
            {
                conn.Open();
                SqlCommand cm = null;

                string sql = $@"
                                IF OBJECT_ID('tempdb..#data') IS NOT NULL DROP TABLE #data
								IF OBJECT_ID('tempdb..#data2') IS NOT NULL DROP TABLE #data2

                                DECLARE @v_period INT SET @v_period = @periodIdx 
                                DECLARE @v_center NVARCHAR(30) SET @v_center = @centerIdDx 
                                DECLARE @v_compCode INT SET @v_compCode = @compCodeIdDx 


                                select SUM(CAST(isNew AS INT)) as Sum_isNew,shopCode 
								into #data
								from (select distinct isNew,shopCode from (select distinct i.empId,i.isNew,h.shopCode from [CheckShopTransaction] a
                                inner join Asset c (nolock) on a.assetId = c.assetId
                                inner join [CheckShop] d (nolock) on a.checkShopId = d.checkShopId
                                inner join [Task] e (nolock) on d.taskID = e.taskID
                                inner join [Period] f (nolock) on e.periodId = f.periodId
                                inner join Department g (nolock) on c.departmentId = g.departmentId
                                inner join [Shop] h (nolock) on e.shopId = h.shopId
                                inner join Shopkeeper i (nolock) on i.shopId = h.shopId
								  where 1=1 
                               and f.periodId = @v_period
								-- and c.compCode = @v_compCode
                                -- and g.center =  @v_center
) a) c group by shopCode

								select distinct i.empId,h.shopCode 
								into #data2
								from [CheckShopTransaction] a
                                inner join Asset c (nolock) on a.assetId = c.assetId
                                inner join [CheckShop] d (nolock) on a.checkShopId = d.checkShopId
                                inner join [Task] e (nolock) on d.taskID = e.taskID
                                inner join [Period] f (nolock) on e.periodId = f.periodId
                                inner join Department g (nolock) on c.departmentId = g.departmentId
                                inner join [Shop] h (nolock) on e.shopId = h.shopId
                                inner join Shopkeeper i (nolock) on i.shopId = h.shopId
								inner join #data wh on wh.Sum_isNew = i.isNew and h.shopCode = wh.shopCode
								where 1=1 
                               and f.periodId = @v_period
								-- and c.compCode = @v_compCode
                                -- and g.center =  @v_center

                                 select 
                                 departmentName,empId,empName,shopCode,shopName,
                                 case 
									when shopStatus = 'open' then 'เปิด' 
									when shopStatus = 'close' then 'ปิด' 
									when shopStatus = 'go-out-of-business' then 'เลิกกิจการ'
									when shopStatus = 'not-found' then 'หาร้านไม่พบ'
									when shopStatus = 'unchecked' then 'ไม่ได้เข้าร้าน'
								 end as shopStatus,shopImage,remark,checkDate,assetType,assetCode,assetName
                                 ,sum(assetStatusGood) as CountGood,sum(assetStatusBad) as CountBad
                                 ,sum(amountGood) as SumGood,sum(amountBad) as SumBad
                                ,sum(ImageStatusGood) as CountImageGood,sum(ImageStatusBad) as CountImageBad
                                ,accountBalance as SumAccountBalance
								,(avaliable+damaged - accountBalance) as DiffAccountBalance
                                 from ( select g.departmentName,j.empId,j.empName,h.shopCode,h.shopName,d.shopStatus,d.shopImage,d.remark,CONVERT(VARCHAR, e.checkDate,103) as checkDate
                                ,c.assetType,c.assetCode,c.assetName,b.amount,b.assetStatus,case when b.assetStatus = 'available' then 1 else 0 end as assetStatusGood
								,case when b.assetStatus = 'damaged' then 1 else 0 end as assetStatusBad
                                ,case when b.assetStatus = 'available' then b.amount else 0 end as amountGood
								,case when b.assetStatus = 'damaged' then b.amount else 0 end as amountBad
								,case when b.attachImage != '' and b.assetStatus = 'available' then 1 else 0 end as ImageStatusGood
								,case when b.attachImage != '' and b.assetStatus = 'damaged' then 1 else 0 end as ImageStatusBad
                                ,a.accountBalance,a.avaliable,a.damaged
                                from [CheckShopTransaction] a
                                left join CheckShopTransactionDetail b on a.checkShopTransactionId = b.checkShopTransactionId
                                inner join Asset c (nolock) on a.assetId = c.assetId
                                inner join [CheckShop] d (nolock) on a.checkShopId = d.checkShopId
                                inner join [Task] e (nolock) on d.taskID = e.taskID
                                inner join [Period] f (nolock) on e.periodId = f.periodId
                                inner join Department g (nolock) on c.departmentId = g.departmentId
                                inner join [Shop] h (nolock) on e.shopId = h.shopId
                                inner join Shopkeeper i (nolock) on i.shopId = h.shopId
                                inner join Employee as j (nolock) on j.empId = i.empId -- and a.compCode = j.compCode
                                inner join #data2 wh on wh.empId = i.empId and wh.shopCode = h.shopCode
                                where 1=1 
                               and f.periodId = @v_period
								-- and c.compCode = @v_compCode
                                -- and g.center =  @v_center
                               ) a group by departmentName,empId,empName,shopCode,shopName,shopStatus,shopImage,remark,checkDate,assetType,assetCode,assetName,accountBalance,avaliable,damaged
								order by a.departmentName,a.empId,a.shopCode,a.assetType,a.assetCode          
                ";
                cm = new SqlCommand(sql, conn);
                cm.CommandTimeout = 5000;
                cm.Parameters.Add(new SqlParameter("@periodIdx", req.periodId));
                cm.Parameters.Add(new SqlParameter("@centerIdDx", req.center));
                cm.Parameters.Add(new SqlParameter("@compCodeIdDx", req.compCode));

                SqlDataReader reader = cm.ExecuteReader();
                if (reader.HasRows)
                {
                    reportTaskOnSummaryResp = new List<Res_ReportExcelConfirmImage>();
                    reportTaskOnSummaryResp = HelperBC.DataReaderMapToList<Res_ReportExcelConfirmImage>(reader);
                }
                else
                    throw new Exception(ConstMsg.FIND_REPORT_AUDIT_SHOP_NOT_FOUND);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conn.Close();
            }

            return reportTaskOnSummaryResp;
        }

        List<Res_DeteilReportExcelConfirmImage> IExportFile.GetDeteilImageReportTaskOnSummary(Req_ReportExcelConfirmImage req)
        {
            connectionString = configuration.GetConnectionString(ConstMsg.MAICONFIRMDBCONNECTIONSTRING);
            SqlConnection conn = new SqlConnection(connectionString);
            List<Res_DeteilReportExcelConfirmImage> reportTaskOnSummaryResp = null;
            try
            {
                conn.Open();
                SqlCommand cm = null;
                string sql = $@"
                                IF OBJECT_ID('tempdb..#data') IS NOT NULL DROP TABLE #data
                                IF OBJECT_ID('tempdb..#data2') IS NOT NULL DROP TABLE #data2

                                DECLARE @v_period INT SET @v_period = @periodIdx 
                                DECLARE @v_center NVARCHAR(30) SET @v_center = @centerIdDx 
                                DECLARE @v_compCode INT SET @v_compCode = @compCodeIdDx 

                                select SUM(CAST(isNew AS INT)) as Sum_isNew,shopCode 
								into #data
								from (select distinct isNew,shopCode from (select distinct i.empId,i.isNew,h.shopCode from [CheckShopTransaction] a
                                inner join Asset c on a.assetId = c.assetId
                                inner join [CheckShop] d on a.checkShopId = d.checkShopId
                                inner join [Task] e on d.taskID = e.taskID
                                inner join [Period] f on e.periodId = f.periodId
                                inner join Department g on c.departmentId = g.departmentId
                                inner join [Shop] h on e.shopId = h.shopId
                                inner join Shopkeeper i on i.shopId = h.shopId
								  where 1=1 
                                and f.periodId = @v_period
								-- and c.compCode = @v_compCode
                                -- and g.center =  @v_center
) a) c group by shopCode

								select distinct i.empId,h.shopCode 
								into #data2
								from [CheckShopTransaction] a
                                inner join Asset c on a.assetId = c.assetId
                                inner join [CheckShop] d on a.checkShopId = d.checkShopId
                                inner join [Task] e on d.taskID = e.taskID
                                inner join [Period] f on e.periodId = f.periodId
                                inner join Department g on c.departmentId = g.departmentId
                                inner join [Shop] h on e.shopId = h.shopId
                                inner join Shopkeeper i on i.shopId = h.shopId
								inner join #data wh on wh.Sum_isNew = i.isNew and h.shopCode = wh.shopCode
								where 1=1 
                               and f.periodId = @v_period
								-- and c.compCode = @v_compCode
                                -- and g.center =  @v_center

                                select --*
                                --f.periodNName,f.startDate,f.endDate,g.departmentName,
                                g.departmentName,j.empId,j.empName,h.shopCode,h.shopName,d.shopStatus,d.shopImage,d.remark,CONVERT(VARCHAR, e.checkDate,103) as checkDate
                                ,c.assetType,c.assetCode,c.assetName,b.amount,b.assetStatus,b.attachImage,b.remark as remarkimage
                                from [CheckShopTransaction] a
                                inner join CheckShopTransactionDetail b on a.checkShopTransactionId = b.checkShopTransactionId
                                inner join Asset c on a.assetId = c.assetId
                                inner join [CheckShop] d on a.checkShopId = d.checkShopId
                                inner join [Task] e on d.taskID = e.taskID
                                inner join [Period] f on e.periodId = f.periodId
                                inner join Department g on c.departmentId = g.departmentId
                                inner join [Shop] h on e.shopId = h.shopId
                                inner join Shopkeeper i on i.shopId = h.shopId
                                inner join Employee as j on j.empId = i.empId
                                inner join #data2 wh on wh.empId = i.empId and wh.shopCode = h.shopCode
                                where 1=1 
                              
                             and f.periodId = @v_period
								-- and c.compCode = @v_compCode
                                -- and g.center =  @v_center
                                and b.attachImage != ''
                                order by j.empId,h.shopCode,c.assetType,c.assetCode,b.assetStatus,b.id           
                ";


                cm = new SqlCommand(sql, conn);
                cm.Parameters.Add(new SqlParameter("@periodIdx", req.periodId));
                cm.Parameters.Add(new SqlParameter("@centerIdDx", req.center));
                cm.Parameters.Add(new SqlParameter("@compCodeIdDx", req.compCode));

                SqlDataReader reader = cm.ExecuteReader();
                if (reader.HasRows)
                {
                    reportTaskOnSummaryResp = new List<Res_DeteilReportExcelConfirmImage>();
                    reportTaskOnSummaryResp = HelperBC.DataReaderMapToList<Res_DeteilReportExcelConfirmImage>(reader);
                }
               // else
                    //throw new Exception(ConstMsg.FIND_REPORT_AUDIT_SHOP_NOT_FOUND);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conn.Close();
            }

            return reportTaskOnSummaryResp;
        }

        public List<Res_HeaderReportExcelConfirmImage> GetHeaderImageReportTaskOnSummary(Req_ReportExcelConfirmImage req)
        {
            connectionString = configuration.GetConnectionString(ConstMsg.MAICONFIRMDBCONNECTIONSTRING);
            SqlConnection conn = new SqlConnection(connectionString);
            List<Res_HeaderReportExcelConfirmImage> reportTaskOnSummaryResp = null;
            try
            {
                conn.Open();
                SqlCommand cm = null;
                string sql = $@"
                                IF OBJECT_ID('tempdb..#data') IS NOT NULL DROP TABLE #data
                                IF OBJECT_ID('tempdb..#data2') IS NOT NULL DROP TABLE #data2

                                DECLARE @v_period INT SET @v_period = @periodIdx 
                                DECLARE @v_center NVARCHAR(30) SET @v_center = @centerIdDx
                                DECLARE @v_compCode INT SET @v_compCode = @compCodeIdDx 

                                select SUM(CAST(isNew AS INT)) as Sum_isNew,shopCode 
								into #data
								from (select distinct isNew,shopCode from (select distinct i.empId,i.isNew,h.shopCode from [CheckShopTransaction] a
                                inner join Asset c on a.assetId = c.assetId
                                inner join [CheckShop] d on a.checkShopId = d.checkShopId
                                inner join [Task] e on d.taskID = e.taskID
                                inner join [Period] f on e.periodId = f.periodId
                                inner join Department g on c.departmentId = g.departmentId
                                inner join [Shop] h on e.shopId = h.shopId
                                inner join Shopkeeper i on i.shopId = h.shopId
								  where 1=1 
                                and f.periodId = @v_period
								-- and c.compCode = @v_compCode
                                -- and g.center =  @v_center
) a) c group by shopCode

								select distinct i.empId,h.shopCode 
								into #data2
								from [CheckShopTransaction] a
                                inner join Asset c on a.assetId = c.assetId
                                inner join [CheckShop] d on a.checkShopId = d.checkShopId
                                inner join [Task] e on d.taskID = e.taskID
                                inner join [Period] f on e.periodId = f.periodId
                                inner join Department g on c.departmentId = g.departmentId
                                inner join [Shop] h on e.shopId = h.shopId
                                inner join Shopkeeper i on i.shopId = h.shopId
								inner join #data wh on wh.Sum_isNew = i.isNew and h.shopCode = wh.shopCode
								where 1=1 
                                and f.periodId = @v_period
								-- and c.compCode = @v_compCode
                                -- and g.center =  @v_center

                                select b.periodId,b.periodNName,CONVERT(VARCHAR, b.startDate,103) as startDate,CONVERT(VARCHAR, b.endDate,103) as endDate,b.departmentName,b.center as departmentCenter,b.companyName,
                                max(b.CountImageGood) as CountImageGood,max(b.CountImageBad) as CountImageBad
                                from ( select  periodId,periodNName,startDate,endDate,departmentName,center,companyName,
                                 empId,empName,shopCode,shopName,shopStatus,shopImage,remark,checkDate,assetType,assetCode,assetName
                                ,sum(ImageStatusGood) as CountImageGood,sum(ImageStatusBad) as CountImageBad
                                 from ( select distinct f.periodId,f.periodNName,f.startDate,f.endDate
								 ,g.departmentName,g.center,k.companyName ,j.empId,j.empName,h.shopCode,h.shopName,d.shopStatus,d.shopImage,d.remark,CONVERT(VARCHAR, e.checkDate,103) as checkDate
                                ,c.assetType,c.assetCode,c.assetName,b.amount,b.assetStatus,b.attachImage
								,case when b.attachImage != '' and b.assetStatus = 'available' then 1 else 0 end as ImageStatusGood
								,case when b.attachImage != '' and b.assetStatus = 'damaged' then 1 else 0 end as ImageStatusBad
                                from [CheckShopTransaction] a
                                left join CheckShopTransactionDetail b on a.checkShopTransactionId = b.checkShopTransactionId
                                inner join Asset c on a.assetId = c.assetId
                                inner join [CheckShop] d on a.checkShopId = d.checkShopId
                                inner join [Task] e on d.taskID = e.taskID
                                inner join [Period] f on e.periodId = f.periodId
                                inner join Department g on c.departmentId = g.departmentId
                                inner join [Shop] h on e.shopId = h.shopId
                                left join Shopkeeper i on i.shopId = h.shopId
                                left join Employee as j on j.empId = i.empId
                                inner join [Company] k on g.compCode = k.compCode
                                inner join #data2 wh on wh.empId = i.empId and wh.shopCode = h.shopCode
                                where 1=1                             
								and f.periodId = @v_period
								-- and c.compCode = @v_compCode
                                -- and g.center =  @v_center
                               ) a group by center,departmentName,companyName,periodId,periodNName,startDate,endDate,empId,empName,shopCode,shopName,shopStatus,shopImage,remark,checkDate,assetType,assetCode,assetName
								--order by a.shopCode,a.empId,a.assetType,a.assetCode 
								) b 
								group by periodId,periodNName,startDate,endDate,center,departmentName,companyName        
                ";
                cm = new SqlCommand(sql, conn);
                cm.CommandTimeout = 5000;
                cm.Parameters.Add(new SqlParameter("@periodIdx", req.periodId));
                cm.Parameters.Add(new SqlParameter("@centerIdDx", req.center));
                cm.Parameters.Add(new SqlParameter("@compCodeIdDx", req.compCode));

                SqlDataReader reader = cm.ExecuteReader();
                if (reader.HasRows)
                {
                    reportTaskOnSummaryResp = new List<Res_HeaderReportExcelConfirmImage>();
                    reportTaskOnSummaryResp = HelperBC.DataReaderMapToList<Res_HeaderReportExcelConfirmImage>(reader);
                }
                else
                    throw new Exception(ConstMsg.FIND_REPORT_AUDIT_SHOP_NOT_FOUND);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conn.Close();
            }

            return reportTaskOnSummaryResp;
        }
    }
}
