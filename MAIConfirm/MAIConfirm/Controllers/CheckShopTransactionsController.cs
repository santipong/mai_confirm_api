﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MAIConfirm.Models;

namespace MAIConfirm.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("api/[controller]")]
    [ApiController]
    public class CheckShopTransactionsController : ControllerBase
    {
        private readonly MAIConfirmAssetDbContext _context;

        public CheckShopTransactionsController(MAIConfirmAssetDbContext context)
        {
            _context = context;
        }

        // GET: api/CheckShopTransactions
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CheckShopTransaction>>> GetCheckShopTransactions()
        {
            return await _context.CheckShopTransactions.ToListAsync();
        }

        // GET: api/CheckShopTransactions/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CheckShopTransaction>> GetCheckShopTransaction(int id)
        {
            var checkShopTransaction = await _context.CheckShopTransactions.FindAsync(id);

            if (checkShopTransaction == null)
            {
                return NotFound();
            }

            return checkShopTransaction;
        }

        // PUT: api/CheckShopTransactions/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCheckShopTransaction(int id, CheckShopTransaction checkShopTransaction)
        {
            if (id != checkShopTransaction.checkShopTransactionId)
            {
                return BadRequest();
            }

            _context.Entry(checkShopTransaction).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CheckShopTransactionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CheckShopTransactions
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<CheckShopTransaction>> PostCheckShopTransaction(CheckShopTransaction checkShopTransaction)
        {
            _context.CheckShopTransactions.Add(checkShopTransaction);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCheckShopTransaction", new { id = checkShopTransaction.checkShopTransactionId }, checkShopTransaction);
        }

        // DELETE: api/CheckShopTransactions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCheckShopTransaction(int id)
        {
            var checkShopTransaction = await _context.CheckShopTransactions.FindAsync(id);
            if (checkShopTransaction == null)
            {
                return NotFound();
            }

            _context.CheckShopTransactions.Remove(checkShopTransaction);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool CheckShopTransactionExists(int id)
        {
            return _context.CheckShopTransactions.Any(e => e.checkShopTransactionId == id);
        }
    }
}
