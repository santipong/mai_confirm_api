﻿using System;
using System.Collections.Generic;

#nullable disable

namespace MAIConfirm.EFModel.Entities
{
    public partial class Department
    {
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public string Region { get; set; }
        public string CompCode { get; set; }
        public string Area { get; set; }
        public string Center { get; set; }
        public string Province { get; set; }
        public string Channel { get; set; }
        public byte[] CreatedAt { get; set; }
    }
}
