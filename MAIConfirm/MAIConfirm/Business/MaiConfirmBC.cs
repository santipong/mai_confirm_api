﻿using MAIConfirm.Business.Helper;
using MAIConfirm.Business.Interface;
using MAIConfirm.EFModel.BackEndModel;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using static MAIConfirm.EFModel.BackEndModel.ModelReport;

namespace MAIConfirm.Business
{
    public class MaiConfirmBC: IMaiConfirm
    {
        private readonly IConfiguration configuration;
        private string connectionString;
        static IConfiguration conf = (new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build());

        public MaiConfirmBC(IConfiguration config)
        {
            configuration = config;
        }

        GetStatusReportTaskOnSummaryResp IMaiConfirm.GetStatusReportTaskOnSummary(GetStatusReportTaskOnSummaryReq req)
        {
            connectionString = configuration.GetConnectionString(ConstMsg.MAICONFIRMDBCONNECTIONSTRING);
            SqlConnection conn = new SqlConnection(connectionString);
            GetStatusReportTaskOnSummaryResp reportTaskOnSummaryResp = null;
            try
            {
                conn.Open();
                SqlCommand cm = null;
                string sql = $@"
                                IF OBJECT_ID(N'tempdb..#tempContract') IS NOT NULL BEGIN DROP TABLE #tempContract END
                                IF OBJECT_ID(N'tempdb..#tempTotalReportTask') IS NOT NULL BEGIN DROP TABLE #tempTotalReportTask END

                                DECLARE @v_periodId INT SET @v_periodId = @periodIdx
                                DECLARE @v_taskId INT SET @v_taskId = @taskIDx
                                SELECT 
	                                a.taskID	
	                                ,a.periodId	
	                                ,a.shopId
	                                ,b.checkShopId
	                                ,b.shopStatus
	                                ,assetTypeId = CASE WHEN c.assetTypeID = 1 OR c.assetTypeID = 2 THEN 1 ELSE 3 END
	                                ,c.compCode
                                INTO #tempContract
                                FROM [dbo].[Task] a INNER JOIN [dbo].[CheckShop] b
                                ON a.taskID = b.taskID INNER JOIN [dbo].[CheckShopTransaction] c
                                ON b.checkShopId = c.checkShopId
                                WHERE 1=1
                                AND a.periodId = @v_periodId
                                AND a.taskID = @v_taskId
                                GROUP BY a.taskID, a.periodId, a.shopId, b.checkShopId, b.shopStatus, (CASE WHEN c.assetTypeID = 1 OR c.assetTypeID = 2 THEN 1 ELSE 3 END), c.compCode

                                SELECT a.taskID, a.periodId, a.shopId, a.checkShopId,sumReportTaskApprove = COUNT(*) INTO #tempTotalReportTask FROM #tempContract a
                                GROUP BY a.taskID, a.periodId, a.shopId, a.checkShopId

                                SELECT a.taskID, a.periodId, a.shopId, a.checkShopId,approveSign = ISNULL(b.sumReport, 0) ,totalTask = a.sumReportTaskApprove,sumTask =  a.sumReportTaskApprove - ISNULL(b.sumReport, 0)
                                FROM #tempTotalReportTask a LEFT JOIN (SELECT a.checkShopId,sumReport = COUNT(*) FROM [dbo].[CheckShopSigned] a GROUP BY checkShopId) b
                                ON a.checkShopId = b.checkShopId             
                ";
                cm = new SqlCommand(sql, conn);
                cm.Parameters.Add(new SqlParameter("@periodIdx", req.periodId));
                cm.Parameters.Add(new SqlParameter("@taskIDx", req.taskID));

                SqlDataReader reader = cm.ExecuteReader();
                if (reader.HasRows)
                {
                    reportTaskOnSummaryResp = new GetStatusReportTaskOnSummaryResp();
                    reportTaskOnSummaryResp = HelperBC.DataReaderMapFirst<GetStatusReportTaskOnSummaryResp>(reader);
                }
                else
                    throw new Exception(ConstMsg.FIND_REPORT_AUDIT_SHOP_NOT_FOUND);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conn.Close();
            }

            return reportTaskOnSummaryResp;
        }

        List<GetStatusReportTaskOnSummaryResp> IMaiConfirm.GetStatusReportTaskOnSummaryALL(GetStatusReportTaskOnSummaryReq req)
        {
            connectionString = configuration.GetConnectionString(ConstMsg.MAICONFIRMDBCONNECTIONSTRING);
            SqlConnection conn = new SqlConnection(connectionString);
            List<GetStatusReportTaskOnSummaryResp> reportTaskOnSummaryResp = null;
            try
            {
                conn.Open();
                SqlCommand cm = null;
                string sql = $@"
                                IF OBJECT_ID(N'tempdb..#tempContract') IS NOT NULL BEGIN DROP TABLE #tempContract END
                                IF OBJECT_ID(N'tempdb..#tempTotalReportTask') IS NOT NULL BEGIN DROP TABLE #tempTotalReportTask END

                                DECLARE @v_periodId INT SET @v_periodId = @periodIdx
                                SELECT 
	                                a.taskID	
	                                ,a.periodId	
	                                ,a.shopId
	                                ,b.checkShopId
	                                ,b.shopStatus
	                                ,assetTypeId = CASE WHEN c.assetTypeID = 1 OR c.assetTypeID = 2 THEN 1 ELSE 3 END
	                                ,c.compCode
                                INTO #tempContract
                                FROM [dbo].[Task] a INNER JOIN [dbo].[CheckShop] b
                                ON a.taskID = b.taskID INNER JOIN [dbo].[CheckShopTransaction] c
                                ON b.checkShopId = c.checkShopId
                                WHERE 1=1
                                AND a.periodId = @v_periodId
                                GROUP BY a.taskID, a.periodId, a.shopId, b.checkShopId, b.shopStatus, (CASE WHEN c.assetTypeID = 1 OR c.assetTypeID = 2 THEN 1 ELSE 3 END), c.compCode

                                SELECT a.taskID, a.periodId, a.shopId, a.checkShopId,sumReportTaskApprove = COUNT(*) INTO #tempTotalReportTask FROM #tempContract a
                                GROUP BY a.taskID, a.periodId, a.shopId, a.checkShopId

                                SELECT a.taskID, a.periodId, a.shopId, a.checkShopId,approveSign = ISNULL(b.sumReport, 0) ,totalTask = a.sumReportTaskApprove,sumTask =  a.sumReportTaskApprove - ISNULL(b.sumReport, 0)
                                FROM #tempTotalReportTask a LEFT JOIN (SELECT a.checkShopId,sumReport = COUNT(*) FROM [dbo].[CheckShopSigned] a GROUP BY checkShopId) b
                                ON a.checkShopId = b.checkShopId             
                ";
                cm = new SqlCommand(sql, conn);
                cm.Parameters.Add(new SqlParameter("@periodIdx", req.periodId));

                SqlDataReader reader = cm.ExecuteReader();
                if (reader.HasRows)
                {
                    reportTaskOnSummaryResp = new List<GetStatusReportTaskOnSummaryResp>();
                    reportTaskOnSummaryResp = HelperBC.DataReaderMapToList<GetStatusReportTaskOnSummaryResp>(reader);
                }
                else
                    throw new Exception(ConstMsg.FIND_REPORT_AUDIT_SHOP_NOT_FOUND);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conn.Close();
            }

            return reportTaskOnSummaryResp;
        }


    }
}
