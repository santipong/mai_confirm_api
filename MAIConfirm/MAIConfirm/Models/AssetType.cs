﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MAIConfirm.Models
{
    public class AssetType
    {
        public int id { get; set; }
        public int typeId { get; set; }
        public String typeName { get; set; }
        public byte[] created_at { get; set; }
    }
}
