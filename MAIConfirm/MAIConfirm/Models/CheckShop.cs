﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MAIConfirm.Models
{
    public class CheckShop
    {
		public int checkShopId { get; set; }
		public int taskID { get; set; }
		public string shopStatus { get; set; }
		public string shopImage { get; set; }
		public string checkLocation { get; set; }
		public string remark { get; set; }
	}
}
