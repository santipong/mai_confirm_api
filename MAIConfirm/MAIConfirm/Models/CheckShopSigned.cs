﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MAIConfirm.Models
{
    public class CheckShopSigned
    {
		public int id { get; set; }
		public int checkShopId { get; set; }
		public string confirmStatus { get; set; }
		public string compCode { get; set; }
		public int assetTypeID { get; set; }
		public string customerSigned { get; set; }
		public string employeeSigned { get; set; }
		public string remark { get; set; }
		public string checkerId { get; set; }
	}

	public class CheckShopSignedResponse
	{
		public string status { get; set; }
		public string message { get; set; }
		public List<CheckShopSigned> checkShopSigneds { get; set; }
	}

	public class CheckShopSignedWithDetailResponse
	{
		public string status { get; set; }
		public string message { get; set; }
		public CheckShopSigned checkShopSigned { get; set; }
		public Employee employee { get; set; }
		public List<CheckAsset> checkTransaction { get; set; }
		public CheckShopDetail checkShopDetail { get; set; }
		public Checker checker { get; set; }
		public DateTime planDate { get; set; }
		public DateTime checkDate { get; set; }
		public DateTime accountDate { get; set; }
		public Shop shop { get; set; }
		public EmployeeCC employeeCC { get; set; }
	}

    public class EmployeeCC
    {
        public int empId { get; set; }
        public string empName { get; set; }
        public string empPosition { get; set; }
        public string signature { get; set; }
    }
}
