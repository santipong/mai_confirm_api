﻿using System;
using System.Collections.Generic;

#nullable disable

namespace MAIConfirm.EFModel.Entities
{
    public partial class Period
    {
        public int PeriodId { get; set; }
        public string PeriodNname { get; set; }
        public string RegionId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public byte[] CreatedAt { get; set; }
    }
}
