﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MAIConfirm.Models;

namespace MAIConfirm.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("api/[controller]")]
    [ApiController]
    public class CheckShopsController : ControllerBase
    {
        private readonly MAIConfirmAssetDbContext _context;

        public CheckShopsController(MAIConfirmAssetDbContext context)
        {
            _context = context;
        }

        // GET: api/CheckShops
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CheckShop>>> GetCheckShops()
        {
            return await _context.CheckShops.ToListAsync();
        }

        // GET: api/CheckShops/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CheckShop>> GetCheckShop(int id)
        {
            var checkShop = await _context.CheckShops.FindAsync(id);

            if (checkShop == null)
            {
                return NotFound();
            }

            return checkShop;
        }

        // PUT: api/CheckShops/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCheckShop(int id, CheckShop checkShop)
        {
            if (id != checkShop.checkShopId)
            {
                return BadRequest();
            }

            _context.Entry(checkShop).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CheckShopExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CheckShops
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<CheckShop>> PostCheckShop(CheckShop checkShop)
        {
            _context.CheckShops.Add(checkShop);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCheckShop", new { id = checkShop.checkShopId }, checkShop);
        }

        // DELETE: api/CheckShops/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCheckShop(int id)
        {
            var checkShop = await _context.CheckShops.FindAsync(id);
            if (checkShop == null)
            {
                return NotFound();
            }

            _context.CheckShops.Remove(checkShop);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool CheckShopExists(int id)
        {
            return _context.CheckShops.Any(e => e.checkShopId == id);
        }
    }
}
