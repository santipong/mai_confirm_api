﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MAIConfirm.Models
{
    public class Login
    {
        public string userId { get; set; }
        public string password { get; set; }
    }
    public class LoginV2
    {
        public string userId { get; set; }
    }

}
