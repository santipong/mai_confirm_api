﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MAIConfirm.Models;

namespace MAIConfirm.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("api/[controller]")]
    [ApiController]
    public class ShopkeepersController : ControllerBase
    {
        private readonly MAIConfirmAssetDbContext _context;

        public ShopkeepersController(MAIConfirmAssetDbContext context)
        {
            _context = context;
        }

        // GET: api/Shopkeepers
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Shopkeeper>>> GetShopkeepers()
        {
            return await _context.Shopkeepers.ToListAsync();
        }

        // GET: api/Shopkeepers/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Shopkeeper>> GetShopkeeper(int id)
        {
            var shopkeeper = await _context.Shopkeepers.FindAsync(id);

            if (shopkeeper == null)
            {
                return NotFound();
            }

            return shopkeeper;
        }

        // PUT: api/Shopkeepers/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutShopkeeper(int id, Shopkeeper shopkeeper)
        {
            if (id != shopkeeper.id)
            {
                return BadRequest();
            }

            _context.Entry(shopkeeper).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ShopkeeperExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Shopkeepers
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Shopkeeper>> PostShopkeeper(Shopkeeper shopkeeper)
        {
            _context.Shopkeepers.Add(shopkeeper);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetShopkeeper", new { id = shopkeeper.id }, shopkeeper);
        }

        // DELETE: api/Shopkeepers/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteShopkeeper(int id)
        {
            var shopkeeper = await _context.Shopkeepers.FindAsync(id);
            if (shopkeeper == null)
            {
                return NotFound();
            }

            _context.Shopkeepers.Remove(shopkeeper);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool ShopkeeperExists(int id)
        {
            return _context.Shopkeepers.Any(e => e.id == id);
        }
    }
}
