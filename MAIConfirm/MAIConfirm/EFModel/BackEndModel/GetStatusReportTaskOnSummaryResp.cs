﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MAIConfirm.EFModel.BackEndModel
{
    public class GetStatusReportTaskOnSummaryResp
    {
        public int? taskID { get; set; }
        public int? periodId { get; set; }
        public int? shopId { get; set; }
        public int? checkShopId { get; set; }
        public int? approveSign { get; set; }
        public int? totalTask { get; set; }
        public int? sumTask { get; set; }
    }

    public class GetStatusReportTaskOnSummaryReq
    {
        public int? taskID { get; set; }
        public int? periodId { get; set; }

    }

}
