﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MAIConfirm.Models
{
	public class Company
	{
		public int id { get; set; }
		public string compCode { get; set; }
		public string companyName { get; set; }
		public int region { get; set; }
		public byte[] created_at { get; set; }
		public string address { get; set; }
	}

	public class CompaniesResponse
	{
		public string status { get; set; }
		public string message { get; set; }
		public List<Company> companies { get; set; }
	}
}
