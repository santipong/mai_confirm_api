﻿using System;
using System.Collections.Generic;

#nullable disable

namespace MAIConfirm.EFModel.Entities
{
    public partial class AssetType
    {
        public int Id { get; set; }
        public int TypeId { get; set; }
        public byte[] CreatedAt { get; set; }
        public string TypeName { get; set; }
    }
}
