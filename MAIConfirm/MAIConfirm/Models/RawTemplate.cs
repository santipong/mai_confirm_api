﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MAIConfirm.Models
{
    public class CreateTemplateRequest 
    {
        public String periodName { get; set; }
        public String region { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public List<RawShop> rawData { get; set; }
    }

    public class RawShop 
    {
        public String shopId { get; set; }
        public List<RawTemplate> rawTemplates { get; set; }
    }

    public class RawTemplate
    {
        public String region { get; set; }
        public String companyName { get; set; }
        public String area { get; set; }
        public String center { get; set; }
        public String province { get; set; }
        public String department { get; set; }
        public String channel { get; set; }
        public String planDate { get; set; }
        public String customerCode { get; set; }
        public String customerName { get; set; }
        public String address { get; set; }
        public String location { get; set; }
        public String employeeId { get; set; }
        public String employeeName { get; set; }
        public String accountDate { get; set; }
        public String assetType { get; set; }
        public String assetCode { get; set; }
        public String assetName { get; set; }
        public String assetStatus { get; set; }
        public String unitCount { get; set; }
        public String unitPrice { get; set; }
        public String accountAmount { get; set; }
    }
}
