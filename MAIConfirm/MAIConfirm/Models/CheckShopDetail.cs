﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MAIConfirm.Models
{
    public class CheckShopDetail
    {
		public int id { get; set; }
		public int checkShopId { get; set; }
		public string customerName { get; set; }
		public string phoneNumber { get; set; }
		public string relation { get; set; }
	}

	public class CheckShopDetailResponse
	{
		public string status { get; set; }
		public string message { get; set; }
		public CheckShopDetail checkShopDetail { get; set; }
	}

	public class CheckShopDetailWithKeeperResponse
	{
		public string status { get; set; }
		public string message { get; set; }
		public CheckShopDetail checkShopDetail { get; set; }
		public Employee employee { get; set; }
	}
}
