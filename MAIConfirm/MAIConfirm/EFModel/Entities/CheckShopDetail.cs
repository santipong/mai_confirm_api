﻿using System;
using System.Collections.Generic;

#nullable disable

namespace MAIConfirm.EFModel.Entities
{
    public partial class CheckShopDetail
    {
        public int Id { get; set; }
        public int CheckShopId { get; set; }
        public string CustomerName { get; set; }
        public string PhoneNumber { get; set; }
        public string Relation { get; set; }
        public byte[] CreatedAt { get; set; }
    }
}
