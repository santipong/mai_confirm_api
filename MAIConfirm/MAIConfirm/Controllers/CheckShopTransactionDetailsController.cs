﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MAIConfirm.Models;

namespace MAIConfirm.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("api/[controller]")]
    [ApiController]
    public class CheckShopTransactionDetailsController : ControllerBase
    {
        private readonly MAIConfirmAssetDbContext _context;

        public CheckShopTransactionDetailsController(MAIConfirmAssetDbContext context)
        {
            _context = context;
        }

        // GET: api/CheckShopTransactionDetails
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CheckShopTransactionDetail>>> GetCheckShopTransactionDetails()
        {
            return await _context.CheckShopTransactionDetails.ToListAsync();
        }

        // GET: api/CheckShopTransactionDetails/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CheckShopTransactionDetail>> GetCheckShopTransactionDetail(int id)
        {
            var checkShopTransactionDetail = await _context.CheckShopTransactionDetails.FindAsync(id);

            if (checkShopTransactionDetail == null)
            {
                return NotFound();
            }

            return checkShopTransactionDetail;
        }

        // PUT: api/CheckShopTransactionDetails/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCheckShopTransactionDetail(int id, CheckShopTransactionDetail checkShopTransactionDetail)
        {
            if (id != checkShopTransactionDetail.id)
            {
                return BadRequest();
            }

            _context.Entry(checkShopTransactionDetail).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CheckShopTransactionDetailExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CheckShopTransactionDetails
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<CheckShopTransactionDetail>> PostCheckShopTransactionDetail(CheckShopTransactionDetail checkShopTransactionDetail)
        {
            _context.CheckShopTransactionDetails.Add(checkShopTransactionDetail);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCheckShopTransactionDetail", new { id = checkShopTransactionDetail.id }, checkShopTransactionDetail);
        }

        // DELETE: api/CheckShopTransactionDetails/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCheckShopTransactionDetail(int id)
        {
            var checkShopTransactionDetail = await _context.CheckShopTransactionDetails.FindAsync(id);
            if (checkShopTransactionDetail == null)
            {
                return NotFound();
            }

            _context.CheckShopTransactionDetails.Remove(checkShopTransactionDetail);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool CheckShopTransactionDetailExists(int id)
        {
            return _context.CheckShopTransactionDetails.Any(e => e.id == id);
        }
    }
}
