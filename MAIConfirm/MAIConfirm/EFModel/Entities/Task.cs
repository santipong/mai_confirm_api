﻿using System;
using System.Collections.Generic;

#nullable disable

namespace MAIConfirm.EFModel.Entities
{
    public partial class Task
    {
        public int TaskId { get; set; }
        public int PeriodId { get; set; }
        public int ShopId { get; set; }
        public string TaskName { get; set; }
        public DateTime PlanDate { get; set; }
        public DateTime? CheckDate { get; set; }
        public byte[] CreatedAt { get; set; }
        public DateTime? AccountDate { get; set; }
    }
}
