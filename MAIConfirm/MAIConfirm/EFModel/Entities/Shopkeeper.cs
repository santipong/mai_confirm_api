﻿using System;
using System.Collections.Generic;

#nullable disable

namespace MAIConfirm.EFModel.Entities
{
    public partial class Shopkeeper
    {
        public int Id { get; set; }
        public int ShopId { get; set; }
        public int EmpId { get; set; }
        public byte[] CreatedAt { get; set; }
        public bool? IsNew { get; set; }
    }
}
