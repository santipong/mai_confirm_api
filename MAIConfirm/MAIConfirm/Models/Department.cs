﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MAIConfirm.Models
{
    public class Department
    {
		public int departmentId { get; set; }
		public string departmentName { get; set; }
		public string region { get; set; }
		public string compCode { get; set; }
		public string area { get; set; }
		public string center { get; set; }
		public string province { get; set; }
		public string channel { get; set; }
	}

	public class DepartmentResponse
	{
		public string status { get; set; }
		public string message { get; set; }
		public List<Department> departments { get; set; }
	}
}
