﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MAIConfirm.Models;

namespace MAIConfirm.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("api/[controller]")]
    [ApiController]
    public class AssetTasksController : ControllerBase
    {
        private readonly MAIConfirmAssetDbContext _context;

        public AssetTasksController(MAIConfirmAssetDbContext context)
        {
            _context = context;
        }

        // GET: api/AssetTasks
        [HttpGet]
        public async Task<ActionResult<IEnumerable<AssetTask>>> GetTasks()
        {
            return await _context.Tasks.ToListAsync();
        }

        // GET: api/AssetTasks/5
        [HttpGet("{id}")]
        public async Task<ActionResult<AssetTask>> GetAssetTask(int id)
        {
            var assetTask = await _context.Tasks.FindAsync(id);

            if (assetTask == null)
            {
                return NotFound();
            }

            return assetTask;
        }

        // PUT: api/AssetTasks/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAssetTask(int id, AssetTask assetTask)
        {
            if (id != assetTask.taskID)
            {
                return BadRequest();
            }

            _context.Entry(assetTask).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AssetTaskExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/AssetTasks
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<AssetTask>> PostAssetTask(AssetTask assetTask)
        {
            _context.Tasks.Add(assetTask);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAssetTask", new { id = assetTask.taskID }, assetTask);
        }

        // DELETE: api/AssetTasks/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAssetTask(int id)
        {
            var assetTask = await _context.Tasks.FindAsync(id);
            if (assetTask == null)
            {
                return NotFound();
            }

            _context.Tasks.Remove(assetTask);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool AssetTaskExists(int id)
        {
            return _context.Tasks.Any(e => e.taskID == id);
        }
    }
}
