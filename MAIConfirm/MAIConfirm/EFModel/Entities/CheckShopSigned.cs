﻿using System;
using System.Collections.Generic;

#nullable disable

namespace MAIConfirm.EFModel.Entities
{
    public partial class CheckShopSigned
    {
        public int Id { get; set; }
        public int CheckShopId { get; set; }
        public string ConfirmStatus { get; set; }
        public string CompCode { get; set; }
        public int AssetTypeId { get; set; }
        public string CustomerSigned { get; set; }
        public string EmployeeSigned { get; set; }
        public string Remark { get; set; }
        public byte[] CreatedAt { get; set; }
        public string CheckerId { get; set; }
    }
}
