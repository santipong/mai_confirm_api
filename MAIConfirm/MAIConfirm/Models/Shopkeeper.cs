﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MAIConfirm.Models
{
    public class Shopkeeper
    {
        public int id { get; set; }
        public int shopId { get; set; }
        public int empId { get; set; }
        public bool isNew { get; set; }
    }

    public class ShopKeeperDetail
    {
        public int empId { get; set; }
        public String empName { get; set; }
        public Department department { get; set; }
        public Company company { get; set; }
        public bool isNew { get; set; }
    }

    public class CheckShopKeeper
    {
        public int id { get; set; }
        public int checkShopId { get; set; }
        public string empId { get; set; }
        public string empName { get; set; }
        public string compCode { get; set; }
        public bool isActive { get; set; }
    }

    public class CheckShopKeepersResponse
    {
        public string status { get; set; }
        public string message { get; set; }
        public List<CheckShopKeeper> checkShopKeeper { get; set; }
    }

    public class CheckShopKeeperRequest 
    {
        public int checkShopId { get; set; }
        public List<KeeperDetail> keeperDetails { get; set; }
    }

    public class KeeperDetail
    {
        public int checkShopKeeperId { get; set; }
        public string empId { get; set; }
        public string empName { get; set; }
        public string compCode { get; set; }
        public bool isNew { get; set; }
        public bool isChange { get; set; }
        public string oldEmpId { get; set; }
    }

    public class CurrentShopKeepersResponse
    {
        public string status { get; set; }
        public string message { get; set; }
        public List<CheckShopKeeper> keeperDetails { get; set; }
    }
}
