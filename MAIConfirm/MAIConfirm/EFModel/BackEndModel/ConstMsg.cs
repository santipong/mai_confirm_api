﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MAIConfirm.EFModel.BackEndModel
{
    public static class ConstMsg
    {
        public const string STATUS_SUCCESS = "success";
        public const string MAICONFIRMDBCONNECTIONSTRING = "MAIConfirmDbConnectionString";
        public const string STATUS_ERROR = "error";
        public const string NOT_FOUND_MSG = "Not found data.";
        public const string INVALID_PARAMETER = "Invalid parameter.";
        public const string FIND_REPORT_AUDIT_SHOP_NOT_FOUND = "ไม่พบข้อมูลที่เลือก";
        public const string CONFIRMCORRECT = "ถูกต้อง";
        public const string CONFIRMINCORRECT = "ไม่ถูกต้อง";
        public const string CONFIRMUNRECOMMEND = "ไม่ประสงค์ยืนยันยอด";
    }
}
