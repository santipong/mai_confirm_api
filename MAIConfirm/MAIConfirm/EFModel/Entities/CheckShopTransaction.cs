﻿using System;
using System.Collections.Generic;

#nullable disable

namespace MAIConfirm.EFModel.Entities
{
    public partial class CheckShopTransaction
    {
        public int CheckShopTransactionId { get; set; }
        public int CheckShopId { get; set; }
        public int AssetId { get; set; }
        public string CompCode { get; set; }
        public int AssetTypeId { get; set; }
        public int? AccountBalance { get; set; }
        public int? Avaliable { get; set; }
        public int? Damaged { get; set; }
        public byte[] CreatedAt { get; set; }
    }
}
