﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MAIConfirm.Models;

namespace MAIConfirm.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("api/[controller]")]
    [ApiController]
    public class CheckShopDetailsController : ControllerBase
    {
        private readonly MAIConfirmAssetDbContext _context;

        public CheckShopDetailsController(MAIConfirmAssetDbContext context)
        {
            _context = context;
        }

        // GET: api/CheckShopDetails
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CheckShopDetail>>> GetCheckShopDetails()
        {
            return await _context.CheckShopDetails.ToListAsync();
        }

        // GET: api/CheckShopDetails/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CheckShopDetail>> GetCheckShopDetail(int id)
        {
            var checkShopDetail = await _context.CheckShopDetails.FindAsync(id);

            if (checkShopDetail == null)
            {
                return NotFound();
            }

            return checkShopDetail;
        }

        // PUT: api/CheckShopDetails/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCheckShopDetail(int id, CheckShopDetail checkShopDetail)
        {
            if (id != checkShopDetail.id)
            {
                return BadRequest();
            }

            _context.Entry(checkShopDetail).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CheckShopDetailExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CheckShopDetails
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<CheckShopDetail>> PostCheckShopDetail(CheckShopDetail checkShopDetail)
        {
            _context.CheckShopDetails.Add(checkShopDetail);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCheckShopDetail", new { id = checkShopDetail.id }, checkShopDetail);
        }

        // DELETE: api/CheckShopDetails/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCheckShopDetail(int id)
        {
            var checkShopDetail = await _context.CheckShopDetails.FindAsync(id);
            if (checkShopDetail == null)
            {
                return NotFound();
            }

            _context.CheckShopDetails.Remove(checkShopDetail);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool CheckShopDetailExists(int id)
        {
            return _context.CheckShopDetails.Any(e => e.id == id);
        }
    }
}
